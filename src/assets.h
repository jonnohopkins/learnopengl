#pragma once

#include <vector>
#include <string>

#include "model.h"
#include "texture.h"
#include "scene.h"

constexpr bool gamma_enabled = true;

// assets:
extern std::vector<std::pair<scene_id_t, Scene>> scenes; // sorted by scene id
extern std::vector<scene_id_t> active_scenes;
extern std::vector<scene_id_t> prefabs; // sorted
extern std::vector<Texture> textures; // sorted by texture id
extern std::vector<Cubemap> cubemaps; // sorted by texture id
extern std::vector<std::pair<mesh_id_t, Mesh>> meshes; // sorted by mesh id
extern std::vector<std::pair<model_id_t, Model>> models; // sorted by model id

// note: doesn't actually add to 'textures', should fix that.
void add_textures(std::vector<Texture>::const_iterator begin, std::vector<Texture>::const_iterator end);
void remove_texture(tex_id_t tex_id);
// note: doesn't actually add to 'cubemaps', should fix that.
void add_cubemap(const Cubemap& cubemap);
void remove_cubemap(tex_id_t tex_id);
void add_models(const std::vector<std::string>& paths); // returns the number of models successfully loaded.
void remove_models(const std::vector<model_id_t>& models_to_remove);

void remove_mesh(mesh_id_t mesh_id);

// executed on the main thread
// update the texture ids if the texture has been loaded.
void update_textures_if_pending();

void init_assets();
void terminate_assets();
