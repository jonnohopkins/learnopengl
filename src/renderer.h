#pragma once

#include <vector>
#include "shader.h"
#include "camera.h"
#include "scene.h"

class Model;

enum class ShaderView {
    Unlit,
    Unlit_Instanced,
    Lit_Phong,
    Lit_Blinn_Phong,
    Lit_Blinn_Phong_Shadow,
    Deferred_Shading,
    Debug_Depth, // visualise depth, from the camera
    Debug_Normal_Local, // visualise fragment normals (in local space)
    Debug_Normal_World, // visualise fragment normals (in world space)
    Debug_Wireframe, // replace all rendered objects with wireframes
    Reflect,
    Refract,
    Override_Vertex_Normals, // add normal vectors to all vertices
};

enum class BloomOption {
    None,
    RepeatedGaussian,
    MultiSizeGaussian,
};

class Renderer {
public:
    ShaderView shader_view{ ShaderView::Lit_Blinn_Phong_Shadow };
    int width{ 0 }; // update with resize()
    int height{ 0 }; // update with resize()
    float exposure{ 1.0f };
    size_t msaa_samples{ 4 }; // update with set_msaa_samples()
    bool vsync_enabled{ true }; // update with set_vsync()
    bool use_postprocess{ true };
    bool use_normals_rendering{ false }; // render normal vectors
    bool use_skybox{ false };

    bool use_game_view{ false };

    // bloom settings
    static constexpr size_t bloom_max_num_blurs = 5;

    BloomOption bloom_blur{ BloomOption::MultiSizeGaussian };
    float bloom_intensity_factor{ 0.4f };
    std::vector<float> bloom_powers{ 1, 0.75f, 0.5f, 0.25 };
    float bloom_brightness_cutoff{ 1.0f };
    float bloom_brightness_full{ 2.5f };
    float gaussian_blur_std_dev{ 0.9f };
    size_t gaussian_blur_kernel_size{ 7 };
    std::vector<float> gaussian_blur_weights;

    Camera camera{ glm::vec3(0.0f, 0.0f, 3.0f) };
    float delta_time{ 0.0f };

    Renderer();
    Renderer(const Renderer&) = delete;
    Renderer(Renderer&&) = delete;
    Renderer& operator =(const Renderer&) = delete;
    Renderer& operator =(Renderer&&) = delete;
    ~Renderer() = default;

    void init();
    void terminate();

    inline bool is_view_lit() const {
        return shader_view == ShaderView::Lit_Phong ||
            shader_view == ShaderView::Lit_Blinn_Phong ||
            shader_view == ShaderView::Lit_Blinn_Phong_Shadow ||
            shader_view == ShaderView::Deferred_Shading;
    }

    inline bool does_view_have_shadows() const {
        return shader_view == ShaderView::Lit_Blinn_Phong_Shadow ||
            shader_view == ShaderView::Deferred_Shading;
    }

    inline bool is_view_debug() const {
        return shader_view == ShaderView::Debug_Depth ||
            shader_view == ShaderView::Debug_Normal_Local ||
            shader_view == ShaderView::Debug_Normal_World ||
            shader_view == ShaderView::Debug_Wireframe;
    }

    void post_scene_setup();
    void render_frame();
    void update(bool mesh_buffer_requires_update, bool light_shadow_info_requires_update);

    void resize(int w, int h);
    void create_gaussian_blur_weights();
    void add_instance_for_mesh(mesh_id_t id, const Mesh& mesh); // needs to be called for all meshes
    void remove_instances_for_mesh(const std::vector<mesh_id_t>& meshes_to_remove);

    void set_msaa_samples(size_t samples);
    void set_vsync(bool enabled);

    inline GLuint game_view_color_texture() const { return tonemapped_color_texture; }

private:
    // shaders
    Shader shader_model;
    Shader shader_model_instanced;
    Shader shader_phong;
    Shader shader_blinn_phong;
    Shader shader_blinn_phong_shadow;
    Shader shader_unlit;
    Shader shader_reflect;
    Shader shader_refract;
    Shader shader_depth;
    Shader shader_normals;
    Shader shader_skybox;
    Shader shader_shadow_direction;
    Shader shader_shadow_point;
    Shader shader_debug_normal_local;
    Shader shader_debug_normal_world;
    Shader shader_debug_wireframe;
    Shader shader_bloom_downsample;
    Shader shader_bloom_bright_only;
    Shader shader_bloom_gaussian_blur;
    Shader shader_bloom_combine_textures;
    Shader shader_deferred_g_buffer;
    Shader shader_deferred_shading;
    Shader shader_screen;

    size_t skybox_cubemap_idx;

    //                                               /->  g_buffer -> deferred_fb  --\
    // [shadow_direction_fb, shadow_point_fb -> ] --|                                 |-->  [ bloom_fbs -> ] post_process_fb [ -> tonemapped_fb ]
    //                                               \->  ms_fb                    --/

    GLuint screen_vbo, screen_vao;
    GLuint cube_vbo, cube_ebo, light_vao;
    GLuint skybox_ebo, skybox_vao;
    GLuint shadow_direction_fb, shadow_depth_texture, shadow_point_fb, shadow_depth_cubemap;
    GLuint ms_fb, ms_color_texture, ms_rbo;
    GLuint g_buffer, g_position, g_normal, g_color_spec, gbuffer_depth_rbo;
    GLuint deferred_fb, deferred_color_texture, deferred_rbo;
    GLuint bloom_fbs[2 * bloom_max_num_blurs];
    GLuint bloom_color_textures[2 * bloom_max_num_blurs];
    GLuint post_process_fb, post_process_color_texture;
    GLuint tonemapped_fb, tonemapped_color_texture;

    std::pair<scene_id_t, entity_id_t> direction_light;
    std::pair<scene_id_t, entity_id_t> point_light;

    float last_frame{ 0.0f };

    struct MeshInstanceInfo {
        size_t count; // the number of entities with this mesh
        GLuint gl_buffer; // buffer of #count matrices that represent all entities' transforms in the scene with this mesh

        inline MeshInstanceInfo() noexcept
            : count(0) {
            glGenBuffers(1, &gl_buffer);
        }

        MeshInstanceInfo(const MeshInstanceInfo&) = delete;
        inline MeshInstanceInfo(MeshInstanceInfo&& rhs) noexcept {
            count = rhs.count;
            gl_buffer = rhs.gl_buffer;
            rhs.gl_buffer = 0;
        }
        MeshInstanceInfo& operator=(const MeshInstanceInfo&) = delete;
        inline MeshInstanceInfo& operator=(MeshInstanceInfo&& rhs) noexcept {
            glDeleteBuffers(1, &gl_buffer);
            count = rhs.count;
            gl_buffer = rhs.gl_buffer;
            rhs.gl_buffer = 0;

            return *this;
        }

        inline ~MeshInstanceInfo() {
            glDeleteBuffers(1, &gl_buffer);
        }
    };

    std::vector<std::pair<mesh_id_t, MeshInstanceInfo>> instance_infos; // sorted by id;

    void create_skybox_cubemap();
    void create_gl_skybox();
    void render_skybox(const glm::mat4& view, const glm::mat4& projection);

    void create_framebuffers(bool is_update);
    void create_gbuffer(bool is_update);
    void render_lights(const std::vector<std::pair<scene_id_t, const Scene*>>& active_scene_ptrs, const glm::mat4& view, const glm::mat4& projection);
    glm::mat4 camera_projection();
    glm::mat4 shadow_directional_projection();
    glm::mat4 shadow_point_projection();
    void render_shadows_non_instanced(const Scene& scene, const glm::mat4& parent_model, entity_id_t entity_id, glm::mat4 direction_light_space_matrix, glm::vec3 light_world_pos);
    void bind_lighting_info_to_shader(const std::vector<std::pair<scene_id_t, const Scene*>>& active_scene_ptrs, Shader& shader);
    void bind_shadow_info_to_shader(Shader& shader, glm::mat4 direction_light_space_matrix);
    void render_entity_non_instanced(const std::vector<std::pair<scene_id_t, const Scene*>>& active_scene_ptrs, const Scene& scene, const glm::mat4& parent_model, const glm::mat4& view, const glm::mat4& projection, entity_id_t entity_id, ShaderView shader_view, glm::mat4 direction_light_space_matrix);
    void create_gl_cube_and_light();
    void create_gl_screen();
    void create_shadow_framebuffer_with_tex2d();
    void create_shadow_framebuffer_with_cubemap();
    void setup_shaders(float time);
    void render_bloom(GLuint source_framebuffer, GLuint dest_framebuffer);
    void update_instance_buffer(); // needs to be called whenever the entities, their transforms or meshes are updated.
    void update_shadow_light_info();
};

constexpr float camera_near = 0.1f;
constexpr float camera_far = 250.0f;
constexpr float camera_fovy = 45.0f;
constexpr unsigned int shadow_size = 4 * 1024;
constexpr float shadow_near_plane = 0.1f;
constexpr float shadow_far_plane = 100.0f;

extern Renderer renderer;
