#pragma once

#include <vector>
#include <algorithm>

#include "entity.h"
#include "model.h"
#include "transform.h"
#include "light.h"
#include "physics_2d.h"

template<typename IdType>
struct IdComparator {
    template<typename Pair>
    inline bool operator ()(const Pair& lhs, IdType rhs) { return lhs.first < rhs; }
    template<typename Pair>
    inline bool operator ()(IdType lhs, const Pair& rhs) { return lhs < rhs.first; }
};

template<typename Vector, typename IdType>
inline typename Vector::const_iterator lower_bound(const Vector& vector, IdType id) {
    return std::lower_bound(vector.begin(), vector.end(), id, IdComparator<IdType>{});
}

template<typename Vector, typename IdType>
inline typename Vector::iterator lower_bound(Vector& vector, IdType id) {
    return std::lower_bound(vector.begin(), vector.end(), id, IdComparator<IdType>{});
}

template<typename Vector, typename IdType>
inline std::pair<typename Vector::const_iterator, typename Vector::const_iterator> equal_range(const Vector& vector, IdType id) {
    return std::equal_range(vector.begin(), vector.end(), id, IdComparator<IdType>{});
}

template<typename Vector, typename IdType>
inline std::pair<typename Vector::iterator, typename Vector::iterator> equal_range(Vector& vector, IdType id) {
    return std::equal_range(vector.begin(), vector.end(), id, IdComparator<IdType>{});
}

template<typename Vector, typename IdType>
inline typename Vector::iterator emplace(Vector& vector, IdType id, typename Vector::value_type::second_type&& value) {
    return vector.emplace(lower_bound(vector, id), id, std::move(value));
}

template<typename Vector, typename IdType>
inline typename Vector::iterator emplace(Vector& vector, IdType id, typename const Vector::value_type::second_type& value) {
    return vector.emplace(lower_bound(vector, id), id, value);
}

class Scene {
public:
    std::vector<std::pair<entity_id_t, Entity>> entities; // sorted
    std::vector<entity_id_t> root_entities; // NOT sorted

    // components:
    // all components are sorted by entity id
    std::vector<std::pair<entity_id_t, Transform>> transforms; // must be unique per entity
    std::vector<std::pair<entity_id_t, mesh_id_t>> entity_meshes; // must be unique per entity
    std::vector<std::pair<entity_id_t, Light>> lights; // must be unique per entity
    std::vector<std::pair<entity_id_t, Rigidbody2d>> rigidbody_2ds; // must be unique per entity
    std::vector<std::pair<entity_id_t, PhysicsShape2dBox>> physics_shape_2ds_box;
    std::vector<std::pair<entity_id_t, PhysicsShape2dCircle>> physics_shape_2ds_circle;

    entity_id_t new_entity_id();
    std::pair<entity_id_t, Entity>& create_entity();
    std::pair<entity_id_t, Entity>& create_entity_with_parent(entity_id_t parent_id = 0);
    void delete_entity(entity_id_t id);

    entity_id_t last_entity_id{ 0 };
};

scene_id_t new_scene_id();

std::pair<entity_id_t, Entity>& copy_entity(const Scene& source_scene, entity_id_t source_id, Scene& dest_scene, entity_id_t dest_parent_id = 0);

void reparent_entity(Scene& source_scene, entity_id_t entity_id, Scene& dest_scene, entity_id_t new_parent_id);

void reparent_entity(Scene& scene, entity_id_t entity_id, entity_id_t new_parent_id);

bool is_active(const Scene& scene, entity_id_t entity_id);

void set_active(scene_id_t scene_id, entity_id_t entity_id, Entity& entity, bool is_active);

// source_transform is in the local space of the source entity.
// return a transform relative to the local space of the destination entity.
Transform to_local_space(const Scene& source_scene, entity_id_t source_id, Transform source_transform, const Scene& dest_scene, entity_id_t dest_id);

inline Transform to_local_space(const Scene& scene, entity_id_t source_id, Transform source_transform, entity_id_t dest_id) {
    return to_local_space(scene, source_id, source_transform, scene, dest_id);
}

// returns true if the entity is active
bool world_space_matrix(const Scene& scene, entity_id_t entity_id, glm::mat4& model_mat, bool only_calc_matrix_when_active = true);
