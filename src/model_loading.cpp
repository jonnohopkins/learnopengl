#include "model_loading.h"
#include "globals.h"
#include "scene.h"
#include "assets.h"

#include <iostream>
#include <filesystem>
#include <functional>

#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>

inline glm::vec3 ai_to_glm(aiVector3D source) {
    glm::vec3 result;
    result.x = source.x;
    result.y = source.y;
    result.z = source.z;
    return result;
}

void queue_load_for_material_textures(Model& model, std::vector<MeshTexture>& mesh_textures, aiMaterial* mat, aiTextureType type, TextureType tex_type, size_t default_idx) {
    aiString str;
    size_t textures_processed = 0;

    mesh_textures.reserve(mesh_textures.size() + mat->GetTextureCount(type));
    auto count_for_type = mat->GetTextureCount(type);

    if (count_for_type == 0) {
        mesh_textures.emplace_back(textures[default_idx]);
    }
    else if (count_for_type > 1) {
        ASSERT(false, "Multiple texture for the same type found.");
    }

    for (unsigned int i = 0; i < count_for_type; ++i) {
        mat->GetTexture(type, i, &str);

        std::string texture_path = str.C_Str();
        if (!std::filesystem::exists(texture_path)) {
            texture_path = std::filesystem::path(model.path).parent_path().string() + '/' + texture_path;
        }

        auto it_texture = std::find_if(textures.begin(), textures.end(), [&texture_path](const Texture& candidate) {
            return std::strcmp(candidate.path.c_str(), texture_path.c_str()) == 0;
            });

        if (it_texture != textures.end()) {
            mesh_textures.push_back(*it_texture);
        }
        else {
            // texture hasn't been loaded already, queue it to be loaded
            Texture& texture = textures.emplace_back(new_texture_id(), false, textures[default_idx].gl_id, tex_type, std::move(texture_path));
            mesh_textures.emplace_back(texture);
            model.owned_textures.push_back(texture.tex_id);
        }
    }
}

Mesh process_mesh(Model& model, aiMesh* mesh, const aiScene* scene) {
    Mesh converted_mesh;

    converted_mesh.vertices.reserve(converted_mesh.vertices.size() + mesh->mNumVertices);
    for (unsigned int i = 0; i < mesh->mNumVertices; i++) {
        Vertex& vertex = converted_mesh.vertices.emplace_back();
        vertex.Position = ai_to_glm(mesh->mVertices[i]);

        if (mesh->HasNormals()) {
            vertex.Normal = ai_to_glm(mesh->mNormals[i]);
        }

        if (mesh->mTextureCoords[0]) // does the mesh contain texture coordinates?
        {
            vertex.TexCoords.x = mesh->mTextureCoords[0][i].x;
            vertex.TexCoords.y = mesh->mTextureCoords[0][i].y;
            vertex.Tangent = ai_to_glm(mesh->mTangents[i]);
            //vertex.Bitangent = ai_to_glm(mesh->mBitangents[i]);
        }
    }

    // needs *at least* mesh->mNumFaces * 3 extra indices:
    converted_mesh.indices.reserve(converted_mesh.indices.size() + mesh->mNumFaces * 3);
    for (unsigned int i = 0; i < mesh->mNumFaces; i++) {
        const aiFace& face = mesh->mFaces[i];
        for (unsigned int j = 0; j < face.mNumIndices; j++)
            converted_mesh.indices.push_back(face.mIndices[j]);
    }

        aiMaterial* material = scene->mMaterials[mesh->mMaterialIndex];
        queue_load_for_material_textures(model, converted_mesh.textures, material, aiTextureType_DIFFUSE, TextureType::Diffuse, 0);
        queue_load_for_material_textures(model, converted_mesh.textures, material, aiTextureType_SPECULAR, TextureType::Specular, 1);
        queue_load_for_material_textures(model, converted_mesh.textures, material, aiTextureType_HEIGHT, TextureType::Normal, 2);
        queue_load_for_material_textures(model, converted_mesh.textures, material, aiTextureType_AMBIENT, TextureType::Height, 3);

    converted_mesh.setup();

    return converted_mesh;
}

void create_model_from_file(const char* path) {
    auto& [prefab_scene_id, prefab_scene] = *emplace(scenes, new_scene_id(), Scene{});
    prefabs.insert(std::lower_bound(prefabs.begin(), prefabs.end(), prefab_scene_id), prefab_scene_id);

    Model model;
    model.scene_id = prefab_scene_id;
    model.path = path;

    Assimp::Importer import;
    const aiScene* scene = import.ReadFile(path, aiProcess_Triangulate | aiProcess_GenSmoothNormals | aiProcess_FlipUVs | aiProcess_CalcTangentSpace);

    if (!scene || scene->mFlags & AI_SCENE_FLAGS_INCOMPLETE || !scene->mRootNode) {
        throw std::runtime_error{ import.GetErrorString() };
    }

    std::function<std::vector<std::pair<entity_id_t, Entity>>::iterator(aiNode*)> create_entity_for_node = [&prefab_scene, &model, &create_entity_for_node, scene](aiNode* node) -> std::vector<std::pair<entity_id_t, Entity>>::iterator {
        Entity entity;
        entity.name = node->mName.C_Str();
        entity_id_t entity_id = prefab_scene.new_entity_id();

        glm::mat4 matrix{
            node->mTransformation.a1, node->mTransformation.b1, node->mTransformation.c1, node->mTransformation.d1,
            node->mTransformation.a2, node->mTransformation.b2, node->mTransformation.c2, node->mTransformation.d2,
            node->mTransformation.a3, node->mTransformation.b3, node->mTransformation.c3, node->mTransformation.d3,
            node->mTransformation.a4, node->mTransformation.b4, node->mTransformation.c4, node->mTransformation.d4
        };

        emplace(prefab_scene.transforms, entity_id, Transform{ matrix });

        model.owned_meshes.reserve(model.owned_meshes.size() + node->mNumMeshes);
        meshes.reserve(meshes.size() + node->mNumMeshes);
        prefab_scene.entity_meshes.reserve(prefab_scene.entity_meshes.size() + node->mNumMeshes);
        for (unsigned int i = 0; i < node->mNumMeshes; i++) {
            mesh_id_t mesh_id = new_mesh_id();
            aiMesh* mesh = scene->mMeshes[node->mMeshes[i]];

            model.owned_meshes.push_back(mesh_id);
            emplace(meshes, mesh_id, process_mesh(model, mesh, scene));
            emplace(prefab_scene.entity_meshes, entity_id, mesh_id);
        }

        entity.children.reserve(node->mNumChildren);
        prefab_scene.transforms.reserve(prefab_scene.transforms.size() + node->mNumChildren);
        prefab_scene.entities.reserve(prefab_scene.entities.size() + node->mNumChildren);

        for (unsigned int i = 0; i < node->mNumChildren; ++i) {
            auto& [ child_id, child_entity ] = *create_entity_for_node(node->mChildren[i]);
            child_entity.parent = entity_id;
            entity.children.push_back(child_id);
        }

        return emplace(prefab_scene.entities, entity_id, std::move(entity));
    };

    auto& [ root_id, root_entity ] = *create_entity_for_node(scene->mRootNode);
    prefab_scene.root_entities.push_back(root_id);
    root_entity.name = std::filesystem::path(path).stem().string();

    emplace(models, new_model_id(), std::move(model));
}
