#pragma once

// refer to imconfig.h

#include <glm/vec2.hpp>
#include <glm/vec4.hpp>

#define IM_VEC2_CLASS_EXTRA                                                 \
        ImVec2(const glm::vec2& value) { x = value.x; y = value.y; }        \
        operator glm::vec2() const { return glm::vec2(x, y); }

#define IM_VEC4_CLASS_EXTRA                                                                         \
        ImVec4(const glm::vec4& value) { x = value.x; y = value.y; z = value.z; w = value.w; }      \
        operator glm::vec4() const { return glm::vec4(x, y, z, w); }
