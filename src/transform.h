#pragma once

#include <glm/glm.hpp>
#include <glm/gtc/quaternion.hpp>

const glm::vec3 world_up{ 0.0f, 1.0f, 0.0f };
const glm::vec3 world_forward{ 0.0f, 0.0f, 1.0f };
const glm::vec3 world_right{ -1.0f, 0.0f, 0.0f };

inline glm::vec3 position_from_affine_transformation(glm::mat4 mat) {
    return glm::vec3{ mat[3][0], mat[3][1], mat[3][2] };
}

inline glm::vec3 scale_from_affine_transformation(glm::mat4 mat) {
    return glm::vec3{
        glm::length(glm::vec3(mat[0][0], mat[0][1], mat[0][2])),
        glm::length(glm::vec3(mat[1][0], mat[1][1], mat[1][2])),
        glm::length(glm::vec3(mat[2][0], mat[2][1], mat[2][2]))
    };
}

inline void perspective_divide(glm::vec4& value) {
    value.x = value.x / value.w;
    value.y = value.y / value.w;
    value.z = value.z / value.w;
    value.w = 1.0f;
}

// equivalent to remap_2d_matrix(0.0f, width, height, 0.0f, -1.0f, 1.0f, -1.0f, 1.0f);
inline glm::mat4 viewport_matrix(float width, float height) {
    return glm::mat4{
        glm::vec4(width / 2.0f, 0.0f, 0.0f, 0.0f),
        glm::vec4(0.0f, -height / 2.0f, 0.0f, 0.0f),
        glm::vec4(0.0f, 0.0f, 1.0f, 0.0f),
        glm::vec4(width / 2.0f, height / 2.0f, 0.0f, 1.0f)
    };
}

inline glm::mat4 remap_2d_matrix(float old_x_min, float old_x_max, float old_y_min, float old_y_max, float new_x_min, float new_x_max, float new_y_min, float new_y_max) {
    float x_constant = new_x_min - old_x_min * (new_x_max - new_x_min) / (old_x_max - old_x_min);
    float x_factor = (new_x_max - new_x_min) / (old_x_max - old_x_min);
    float y_constant = new_y_min - old_y_min * (new_y_max - new_y_min) / (old_y_max - old_y_min);
    float y_factor = (new_y_max - new_y_min) / (old_y_max - old_y_min);

    return glm::mat4{
        glm::vec4(x_factor, 0.0f, 0.0f, 0.0f),
        glm::vec4(0.0f, y_factor, 0.0f, 0.0f),
        glm::vec4(0.0f, 0.0f, 1.0f, 0.0f),
        glm::vec4(x_constant, y_constant, 0.0f, 1.0f)
    };
}

// todo: rotating around the X axis is the reverse of rotation direction around the Y and Z axis. make consistent!
class Transform {
public:
    glm::vec3 Position;
    glm::vec3 EulerAngles;
    glm::quat Rotation;
    glm::vec3 Scale;

    inline Transform(glm::vec3 p = glm::vec3{}) noexcept
        : Position{ p }, EulerAngles{}, Rotation{}, Scale{ 1.0f, 1.0f, 1.0f } {
    }

    inline Transform(glm::vec3 p, glm::quat r, glm::vec3 s = glm::vec3{ 1.0f, 1.0f, 1.0f }) noexcept
        : Position{ p }, Rotation{ r }, Scale{ s } {
        UpdateEulerAngles();
    }

    inline Transform(glm::vec3 p, glm::vec3 a, glm::vec3 s = glm::vec3{ 1.0f, 1.0f, 1.0f }) noexcept
        : Position{ p }, EulerAngles{ a }, Scale{ s } {
        UpdateRotation();
    }

    // todo: shouldn't really be doing this ...
    // repeatedly converting TRS matrices to and from Transforms will result in numerical instability
    explicit inline Transform(glm::mat4 mat) noexcept
        : Position(position_from_affine_transformation(mat)),
        Scale(scale_from_affine_transformation(mat)) {
        mat[0][0] /= Scale.x;
        mat[0][1] /= Scale.x;
        mat[0][2] /= Scale.x;
        mat[1][0] /= Scale.y;
        mat[1][1] /= Scale.y;
        mat[1][2] /= Scale.y;
        mat[2][0] /= Scale.z;
        mat[2][1] /= Scale.z;
        mat[2][2] /= Scale.z;
        Rotation = glm::quat_cast(mat);

        UpdateEulerAngles();
    }

    Transform(const Transform&) = default;
    Transform(Transform&&) = default;

    Transform& operator =(const Transform& rhs) = default;
    Transform& operator =(Transform&& rhs) = default;

    ~Transform() = default;

    inline void UpdateEulerAngles() {
        EulerAngles = glm::degrees(glm::eulerAngles(Rotation));
    }

    inline void UpdateRotation() {
        Rotation = glm::quat(glm::radians(EulerAngles));
    }

    inline glm::mat4 GetMatrix() const {
        glm::mat4 result = glm::mat4(1.0f);
        result = glm::translate(result, Position);
        result *= glm::mat4(Rotation);
        result = glm::scale(result, Scale);
        return result;
    }

    inline glm::vec3 Forward() const {
        return Rotation * world_forward;
    }

    inline glm::vec3 Up() const {
        return Rotation * world_up;
    }

    inline glm::vec3 Right() const {
        return Rotation * world_right;
    }

    inline glm::mat4 GetViewMatrix() const {
        return glm::lookAt(Position, Position + Forward(), Up());
    }
};
