#include "physics_2d.h"

#include <vector>
#include <functional>

#include "entity.h"
#include "assets.h"
#include "globals.h"
#include "renderer.h"

#include <imgui.h>

#include <box2d/b2_world.h>
#include <box2d/b2_body.h>
#include <box2d/b2_polygon_shape.h>
#include <box2d/b2_fixture.h>

#include "box2d/draw.h"

#include <glm/vec2.hpp>
#include <glm/vec3.hpp>

inline glm::vec2 b2_to_glm(b2Vec2 source) {
    glm::vec2 result;
    result.x = source.x;
    result.y = source.y;
    return result;
}

Physics2dGlobal physics_2d_settings;

b2World* world = nullptr;

std::vector<std::pair<std::pair<scene_id_t, entity_id_t>, b2Body*>> b2bodies;

b2PolygonShape to_box2d_shape(const PhysicsShape2dBox& shape2dBox, glm::mat4 parent_mat) {
    Transform shape_transform{ glm::vec3{ shape2dBox.center, 0.0f }, glm::vec3{ 0.0f, 0.0f, shape2dBox.rotation_degrees }, glm::vec3{shape2dBox.size.x / 2.0f, shape2dBox.size.y / 2.0f, 1.0f} };
    Transform relative_transform{ parent_mat * shape_transform.GetMatrix() };

    b2PolygonShape box;
    box.SetAsBox(
        relative_transform.Scale.x, relative_transform.Scale.y,
        b2Vec2{ relative_transform.Position.x, relative_transform.Position.y },
        glm::radians(relative_transform.EulerAngles.z)
    );

    return box;
}

b2CircleShape to_box2d_shape(const PhysicsShape2dCircle& shape2dCircle, glm::mat4 parent_mat) {
    Transform shape_transform{ glm::vec3{ shape2dCircle.center, 0.0f } };
    Transform relative_transform{ parent_mat * shape_transform.GetMatrix() };

    b2CircleShape circle;
    circle.m_p = b2Vec2{ relative_transform.Position.x, relative_transform.Position.y };
    circle.m_radius = std::max(relative_transform.Scale.x, relative_transform.Scale.y) * shape2dCircle.radius;

    return circle;
}

void create_physics_world() {
    uint32 flags = 0;
    flags += physics_2d_settings.debug_draw_shapes * b2Draw::e_shapeBit;
    flags += physics_2d_settings.debug_draw_joints * b2Draw::e_jointBit;
    flags += physics_2d_settings.debug_draw_aabb * b2Draw::e_aabbBit;
    flags += physics_2d_settings.debug_draw_center_of_mass * b2Draw::e_centerOfMassBit;
    g_debugDraw.SetFlags(flags);

    if (world)
        delete world;

    world = new b2World{ b2Vec2{ physics_2d_settings.gravity.x, physics_2d_settings.gravity.y } };
    world->SetAllowSleeping(physics_2d_settings.allow_sleep);
    b2bodies.clear();

    for (scene_id_t scene_id : active_scenes) {
        auto [it_scene_lower, it_scene_upper] = equal_range(scenes, scene_id);
        ASSERT(std::distance(it_scene_lower, it_scene_upper) == 1, "scene expected.");
        const Scene& scene = it_scene_lower->second;

        for (const auto& [entity_id, rigidbody2d] : scene.rigidbody_2ds) {
            glm::mat4 model_mat;
            bool is_active = world_space_matrix(scene, entity_id, model_mat, false);

            Transform world_transform{ model_mat };

            b2BodyDef bodyDef;
            bodyDef.type = rigidbody2d.type == Rigidbody2dType::Static ? b2_staticBody
                : rigidbody2d.type == Rigidbody2dType::Kinematic ? b2_kinematicBody
                : b2_dynamicBody;
            bodyDef.position.Set(world_transform.Position.x, world_transform.Position.y);
            bodyDef.angle = glm::radians(world_transform.EulerAngles.z);
            bodyDef.enabled = is_active;
            bodyDef.userData.entity_id = entity_id;
            bodyDef.userData.scene_id = scene_id;
            bodyDef.linearDamping = rigidbody2d.linear_damping;
            bodyDef.angularDamping = rigidbody2d.angular_damping;
            bodyDef.allowSleep = rigidbody2d.allow_sleep;
            bodyDef.awake = rigidbody2d.start_awake;
            bodyDef.fixedRotation = rigidbody2d.fixed_rotation;
            bodyDef.bullet = rigidbody2d.is_continuous;
            bodyDef.gravityScale = rigidbody2d.gravity_scale;

            b2Body* body = world->CreateBody(&bodyDef);

            bool shapes_found = false;

            std::function<void(entity_id_t, glm::mat4)> add_shapes_for_entity = [&add_shapes_for_entity, &scene, scene_id, &rigidbody2d, body, &shapes_found](entity_id_t entity_id, glm::mat4 parent_mat) {
                auto [it_entity_lower, it_entity_upper] = equal_range(scene.entities, entity_id);
                ASSERT(std::distance(it_entity_lower, it_entity_upper) == 1, "entity expected.");
                const Entity& entity = it_entity_lower->second;

                auto create_fixture_for_shape = [&](const auto& physics_shape_2ds) {
                    auto [it_physics_shape_2d_lower, it_physics_shape_2d_upper] = equal_range(physics_shape_2ds, entity_id);

                for (auto it_shape = it_physics_shape_2d_lower; it_shape != it_physics_shape_2d_upper; ++it_shape) {
                        const auto& shape2d = it_shape->second;
                        auto b2shape = to_box2d_shape(shape2d, parent_mat);

                    b2FixtureDef fixtureDef;
                        fixtureDef.shape = &b2shape;
                    fixtureDef.filter.categoryBits = 1 << shape2d.collision_idx;
                    fixtureDef.filter.maskBits = physics_2d_settings.collision_masks[shape2d.collision_idx];
                    fixtureDef.isSensor = shape2d.is_sensor;
                    fixtureDef.userData.scene_id = scene_id;
                    fixtureDef.userData.entity_id = entity_id;

                    fixtureDef.friction = shape2d.friction;
                    fixtureDef.restitution = shape2d.restitution;
                    fixtureDef.density = shape2d.density;

                    b2Fixture* fixture = body->CreateFixture(&fixtureDef);
                }

                if (it_physics_shape_2d_lower != it_physics_shape_2d_upper)
                    shapes_found = true;
                };

                create_fixture_for_shape(scene.physics_shape_2ds_box);
                create_fixture_for_shape(scene.physics_shape_2ds_circle);

                for (entity_id_t child_id : entity.children) {
                    auto [it_transform_lower, it_transform_upper] = equal_range(scene.transforms, child_id);
                    glm::mat4 child_mat = (it_transform_lower != it_transform_upper) ? it_transform_lower->second.GetMatrix() : glm::mat4(1.0f);

                    add_shapes_for_entity(child_id, parent_mat * child_mat);
                }
            };

            add_shapes_for_entity(entity_id, glm::mat4(1.0f));

            if (!rigidbody2d.auto_calculate_mass) {
                b2MassData mass_data;
                body->GetMassData(&mass_data);
                mass_data.mass = rigidbody2d.mass;
                body->SetMassData(&mass_data);
            }

            if (shapes_found)
                emplace(b2bodies, std::make_pair(scene_id, entity_id), body);
            else
                world->DestroyBody(body);
        }
    }

    world->SetDebugDraw(&g_debugDraw);
}

void init_physics() {
    g_debugDraw.Create();

    create_physics_world();
}

void terminate_physics() {
    g_debugDraw.Destroy();

    if (world)
        delete world;

    b2bodies.clear();
}

void physics_entity_set_activation(scene_id_t scene_id, entity_id_t entity_id, bool is_active) {
    auto [it_body_lower, it_body_upper] = equal_range(b2bodies, std::make_pair(scene_id, entity_id));
    if (it_body_lower != it_body_upper) {
        b2Body* body = it_body_lower->second;
        body->SetEnabled(is_active);
    }
}

// may return the current entity if it has a b2bodies entry.
bool ancestor_with_physics_body(scene_id_t scene_id, const Scene& scene, entity_id_t* entity_id, const Entity** entity, size_t* b2bodies_idx) {
    scene_id_t current_scene_id = scene_id;
    const Scene* current_scene = &scene;

    while (*entity_id != 0) {
        auto [it_entity_lower, it_entity_upper] = equal_range(current_scene->entities, *entity_id);
        ASSERT(std::distance(it_entity_lower, it_entity_upper) == 1, "entity expected.");

        *entity = &it_entity_lower->second;

        auto [it_body_lower, it_body_upper] = equal_range(b2bodies, std::make_pair(current_scene_id, *entity_id));
        if (it_body_lower != it_body_upper) {
            *b2bodies_idx = std::distance(b2bodies.begin(), it_body_lower);
            return true;
        }

        *entity_id = it_entity_lower->second.parent;
    }

    return false;
}

void update_physics_bodies() {
    std::vector<bool> rigidbody2ds_processed;
    rigidbody2ds_processed.resize(b2bodies.size());

    auto update_body = [](Scene& scene, entity_id_t entity_id, const Entity& entity, const b2Body* body) {
        if (body->IsAwake() && body->GetType() != b2_staticBody && body->IsEnabled()) {
            auto [it_transform_lower, it_transform_upper] = equal_range(scene.transforms, entity_id);
            ASSERT(std::distance(it_transform_lower, it_transform_upper) == 1, "transform expected.");
            Transform& local_transform = it_transform_lower->second;

            glm::mat4 parent_model_mat;
            bool is_active = entity.is_active && world_space_matrix(scene, entity.parent, parent_model_mat);

            ASSERT(is_active, "body->IsEnabled() implies the entity is active.");

            Transform world_transform{ parent_model_mat * local_transform.GetMatrix() };

            world_transform.Position = glm::vec3(b2_to_glm(body->GetPosition()), 0.0f);
            // don't modify the Position.z
            world_transform.EulerAngles.z = glm::degrees(body->GetAngle());
            // don't modify the EulerAngles.x or .y
            world_transform.UpdateRotation();

            local_transform = Transform{ glm::inverse(parent_model_mat) * world_transform.GetMatrix() };
        }
    };

    size_t processed_idx = 0;

    struct AncestorPhysicsInfo {
        entity_id_t entity_id;
        size_t b2bodies_idx;
        const Entity* entity;
    };

    std::vector<AncestorPhysicsInfo> ancestor_info;

    for (auto [ids, body] : b2bodies) {
        auto [it_scene_lower, it_scene_upper] = equal_range(scenes, ids.first);
        ASSERT(std::distance(it_scene_lower, it_scene_upper) == 1, "scene expected.");
        Scene& scene = it_scene_lower->second;

        auto [it_entity_lower, it_entity_upper] = equal_range(scene.entities, ids.second);
        ASSERT(std::distance(it_entity_lower, it_entity_upper) == 1, "entity expected.");
        const Entity& entity = it_entity_lower->second;

        ancestor_info.clear();

        AncestorPhysicsInfo info;
        info.entity_id = entity.parent;

        while (ancestor_with_physics_body(ids.first, scene, &info.entity_id, &info.entity, &info.b2bodies_idx)) {
            ancestor_info.push_back(info);
            info.entity_id = info.entity->parent;
        }

        for (auto it = ancestor_info.rbegin(); it != ancestor_info.rend(); ++it) {
            if (!rigidbody2ds_processed[it->b2bodies_idx]) {
                update_body(scene, it->entity_id, *it->entity, b2bodies[it->b2bodies_idx].second);
                rigidbody2ds_processed[it->b2bodies_idx] = true;
            }
        }

        if (!rigidbody2ds_processed[processed_idx]) {
            update_body(scene, ids.second, entity, body);
            rigidbody2ds_processed[processed_idx] = true;
        }

        ++processed_idx;
    }
}

bool raycast_closest(const b2World& world, glm::vec2 from, glm::vec2 to, b2Fixture** fixture, glm::vec2* point, glm::vec2* normal, uint16_t maskBits) {
    b2Vec2 b2_from{ from.x, from.y };
    b2Vec2 b2_to{ to.x, to.y };

    class RayCastClosestCallback : public b2RayCastCallback {
    public:
        float ReportFixture(b2Fixture* fixture, const b2Vec2& point, const b2Vec2& normal, float fraction) override {
            bool is_included = maskBits & fixture->GetFilterData().categoryBits;

            if (!is_included) {
                return -1;
            }

            this->fixture = fixture;
            this->point = point;
            this->normal = normal;

            return fraction;
        }

        // input
        uint16_t maskBits{ 0xFFFF };

        // output
        bool is_hit() const { return fixture; }
        b2Fixture* fixture{ nullptr };
        b2Vec2 point;
        b2Vec2 normal;
    };

    RayCastClosestCallback callback;
    callback.maskBits = maskBits;
    world.RayCast(&callback, b2_from, b2_to);

    *fixture = callback.fixture;
    point->x = callback.point.x;
    point->y = callback.point.y;
    normal->x = callback.normal.x;
    normal->y = callback.normal.y;

    if (physics_2d_settings.debug_draw) {
        if (callback.is_hit()) {
            g_debugDraw.DrawPoint(callback.point, 5.0f, b2Color(0.4f, 0.9f, 0.4f));
            g_debugDraw.DrawSegment(b2_from, callback.point, b2Color(0.8f, 0.8f, 0.8f));
            b2Vec2 head = callback.point + 0.5f * callback.normal;
            g_debugDraw.DrawSegment(callback.point, head, b2Color(0.9f, 0.9f, 0.4f));

            auto [it_scene_lower, it_scene_upper] = equal_range(scenes, callback.fixture->GetUserData().scene_id);
            ASSERT(std::distance(it_scene_lower, it_scene_upper) == 1, "scene expected.");
            const Scene& scene = it_scene_lower->second;

            auto [it_entity_lower, it_entity_upper] = equal_range(scene.entities, callback.fixture->GetUserData().entity_id);
            ASSERT(std::distance(it_entity_lower, it_entity_upper) == 1, "entity expected.");
            const Entity& entity = it_entity_lower->second;

            g_debugDraw.DrawString(b2Vec3{ callback.point.x, callback.point.y, 0.0f }, entity.name.c_str());
        }
        else {
            g_debugDraw.DrawSegment(b2_from, b2_to, b2Color(0.8f, 0.8f, 0.8f));
        }
    }

    return callback.is_hit();
}

void update_physics() {
    if (physics_2d_settings.reset) {
        physics_2d_settings.reset = false;

        create_physics_world();
    }

    if (physics_2d_settings.playing || physics_2d_settings.advance) {
        float physics_time_step = 1.0f / (float) physics_2d_settings.steps_per_second;
        world->Step(physics_time_step, physics_2d_settings.velocity_iterations, physics_2d_settings.position_iterations);

        update_physics_bodies();

        physics_2d_settings.advance = false;
    }

    if (physics_2d_settings.debug_draw) {
        ImGui::SetNextWindowPos(ImVec2(0.0f, 0.0f));
        ImGui::SetNextWindowSize(ImVec2(float(renderer.width), float(renderer.height)));
        ImGui::SetNextWindowBgAlpha(0.0f);
        ImGui::Begin("Overlay", nullptr, ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoInputs | ImGuiWindowFlags_AlwaysAutoResize | ImGuiWindowFlags_NoScrollbar);
        ImGui::End();

        world->DebugDraw();
    }
    g_debugDraw.Flush();
}
