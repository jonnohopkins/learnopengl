#pragma once

#include <rapidjson/encodings.h>
#include <array>
#include <tuple>
#include <memory>

// C++20's std::remove_cvref
template<typename T>
struct remove_cvref { typedef std::remove_cv_t<std::remove_reference_t<T>> type; };

template<typename T>
using remove_cvref_t = typename remove_cvref<T>::type;

// pointer_to_member<MemberType ClassType::*>::type = MemberType
template<typename T>
struct pointer_to_member;

template<typename Type, typename MemberType>
struct pointer_to_member<MemberType Type::*> {
    using type = MemberType;
};

template<typename Type, typename MemberType>
struct pointer_to_member<MemberType Type::* const> {
    using type = MemberType;
};

template<typename T>
using pointer_to_member_t = typename pointer_to_member<T>::type;

template<typename T>
struct serialization;

template<typename T, typename = int>
struct has_serialization_member_data : std::false_type {};

template<typename T>
struct has_serialization_member_data<T, decltype((void) serialization<T>::member_data, 0)> : std::true_type {};

template<typename T>
inline constexpr bool has_serialization_member_data_v = has_serialization_member_data<T>::value;

template<typename From, typename To, typename Enable = int>
struct is_value_deserializable : std::false_type {};

template<typename From, typename To>
struct is_value_deserializable<From, To, decltype((void) deserialize_value(std::declval<To&>(), std::declval<From>()), 0)> : std::true_type {};

template<typename From, typename To>
inline constexpr bool is_value_deserializable_v = is_value_deserializable<From, To>::value;

template<typename T>
inline static constexpr bool is_any_value_deserializable_v =
    is_value_deserializable_v<std::nullptr_t, T> ||
    is_value_deserializable_v<bool, T> ||
    is_value_deserializable_v<int, T> ||
    is_value_deserializable_v<unsigned int, T> ||
    is_value_deserializable_v<int64_t, T> ||
    is_value_deserializable_v<uint64_t, T> ||
    is_value_deserializable_v<double, T> ||
    is_value_deserializable_v<const char*, T>;

template<typename Type, typename Enable = void>
struct DeserializerForObject {};

template<typename T, typename = void>
struct has_deserializer_for_object : std::false_type {};

template<typename T>
struct has_deserializer_for_object<T, std::enable_if_t<!std::is_same_v<void, typename DeserializerForObject<T>::type>>> : std::true_type {};

template<typename T>
inline constexpr bool has_deserializer_for_object_v = has_deserializer_for_object<T>::value;

template<typename Type, typename Enable = void>
struct DeserializerForArray {};

template<typename T, typename = void>
struct has_deserializer_for_array : std::false_type {};

template<typename T>
struct has_deserializer_for_array<T, std::enable_if_t<!std::is_same_v<void, typename DeserializerForArray<T>::type>>> : std::true_type {};

template<typename T>
inline constexpr bool has_deserializer_for_array_v = has_deserializer_for_array<T>::value;

template<typename T>
inline constexpr bool is_deserializable_v = has_deserializer_for_array_v<T> || has_deserializer_for_object_v<T> || is_any_value_deserializable_v<T>;

template<typename T>
struct is_vector : std::false_type {};

template<typename T>
struct is_vector<std::vector<T>> : std::true_type {};

template<typename T>
inline constexpr bool is_vector_v = is_vector<T>::value;

template<typename T>
struct is_tuple : std::false_type {};

template<typename ... Ts>
struct is_tuple<std::tuple<Ts ...>> : std::true_type {};

template<typename T1, typename T2>
struct is_tuple<std::pair<T1, T2>> : std::true_type {};

template<typename T, size_t S>
struct is_tuple<std::array<T, S>> : std::true_type {};

template<typename T>
inline constexpr bool is_tuple_v = is_tuple<T>::value;

namespace detail {
    template<typename Tuple, typename Callable, size_t I>
    std::enable_if_t < I < std::tuple_size_v<remove_cvref_t<Tuple>>> call_over_tuple(Callable&& callable, Tuple&& tuple) {
        callable(std::get<I>(tuple));
        call_over_tuple<Tuple, Callable, I + 1>(std::forward<Callable>(callable), std::forward<Tuple>(tuple));
    }

    template<typename Tuple, typename Callable, size_t I>
    std::enable_if_t<I == std::tuple_size_v<remove_cvref_t<Tuple>>> call_over_tuple(Callable&& callable, Tuple&& tuple) {
    }
}

// Calls Callable::operator()(TupleElem element) for each tuple element.
template <typename Tuple, typename Callable>
void call_over_tuple(Callable&& callable, Tuple&& tuple) {
    detail::call_over_tuple<Tuple, Callable, 0>(std::forward<Callable>(callable), std::forward<Tuple>(tuple));
}

template<typename Encoding = rapidjson::UTF8<>>
struct VirtualHandler {
    typedef typename Encoding::Ch Ch;

    virtual ~VirtualHandler() = default;

    virtual bool Null() = 0;
    virtual bool Bool(bool) = 0;
    virtual bool Int(int) = 0;
    virtual bool Uint(unsigned) = 0;
    virtual bool Int64(int64_t) = 0;
    virtual bool Uint64(uint64_t) = 0;
    virtual bool Double(double) = 0;
    virtual bool String(const Ch*, rapidjson::SizeType, bool) = 0;
    virtual bool Key(const Ch* str, rapidjson::SizeType size, bool copy) = 0;

    virtual std::unique_ptr<VirtualHandler<rapidjson::UTF8<>>> deserializer_for_object() = 0;
    virtual std::unique_ptr<VirtualHandler<rapidjson::UTF8<>>> deserializer_for_array() = 0;
    virtual bool complete() = 0;
};

struct Deserializer {
    typedef rapidjson::UTF8<>::Ch Ch;

    enum deserializer_type {
        neither = 0,
        is_object = 1,
        is_array = 2
    };

    Deserializer(deserializer_type type, std::unique_ptr<VirtualHandler<rapidjson::UTF8<>>> deserializer) {
        deserializers.emplace_back(type, std::move(deserializer));
    }

    bool Null() { return deserializers.back().second->Null(); }
    bool Bool(bool value) { return deserializers.back().second->Bool(value); }
    bool Int(int value) { return deserializers.back().second->Int(value); }
    bool Uint(unsigned value) { return deserializers.back().second->Uint(value); }
    bool Int64(int64_t value) { return deserializers.back().second->Int64(value); }
    bool Uint64(uint64_t value) { return deserializers.back().second->Uint64(value); }
    bool Double(double value) { return deserializers.back().second->Double(value); }
    /// enabled via kParseNumbersAsStringsFlag, string is not null-terminated (use length)
    bool RawNumber(const Ch* str, rapidjson::SizeType size, bool copy) { return false; }
    bool String(const Ch* str, rapidjson::SizeType size, bool copy) { return deserializers.back().second->String(str, size, copy); }

    bool Key(const Ch* str, rapidjson::SizeType size, bool copy) {
        return deserializers.back().second->Key(str, size, copy);
    }

    bool StartObject() {
        auto new_deserializer = deserializers.back().second->deserializer_for_object();
        if (new_deserializer) {
            deserializers.emplace_back(deserializer_type::is_object, std::move(new_deserializer));
            return true;
        }
        else {
            return false;
        }
    }

    bool EndObject(rapidjson::SizeType size) {
        if (deserializers.back().first & deserializer_type::is_object) {
            if (!deserializers.back().second->complete())
                return false;
            deserializers.pop_back();
            return true;
        }
        else {
            return false;
        }
    }

    bool StartArray() {
        auto new_deserializer = deserializers.back().second->deserializer_for_array();
        if (new_deserializer) {
            deserializers.emplace_back(deserializer_type::is_array, std::move(new_deserializer));
            return true;
        }
        else {
            return false;
        }
    }

    bool EndArray(rapidjson::SizeType size) {
        if (deserializers.back().first & deserializer_type::is_array) {
            if (!deserializers.back().second->complete())
                return false;
            deserializers.pop_back();
            return true;
        }
        else {
            return false;
        }
    }

private:
    std::vector<std::pair<deserializer_type, std::unique_ptr<VirtualHandler<rapidjson::UTF8<>>>>> deserializers;
};

template<typename Type>
std::enable_if_t<has_deserializer_for_object_v<Type>, std::unique_ptr<VirtualHandler<rapidjson::UTF8<>>>> deserializer_for_object_or_null(Type* target) {
    return std::make_unique<typename DeserializerForObject<Type>::type>(target);
}

template<typename Type>
std::enable_if_t<!has_deserializer_for_object_v<Type>, std::unique_ptr<VirtualHandler<rapidjson::UTF8<>>>> deserializer_for_object_or_null(Type* target) {
    return nullptr;
}

template<typename Type>
std::enable_if_t<has_deserializer_for_array_v<Type>, std::unique_ptr<VirtualHandler<rapidjson::UTF8<>>>> deserializer_for_array_or_null(Type* target) {
    return std::make_unique<typename DeserializerForArray<Type>::type>(target);
}

template<typename Type>
std::enable_if_t<!has_deserializer_for_array_v<Type>, std::unique_ptr<VirtualHandler<rapidjson::UTF8<>>>> deserializer_for_array_or_null(Type* target) {
    return nullptr;
}

template<typename Type>
struct VectorDeserializer : public VirtualHandler<rapidjson::UTF8<>> {
    using target_type = Type; // the vector type to deserialize
    using value_type = typename target_type::value_type; // the vector's value_type

    target_type* target;

    explicit VectorDeserializer(target_type* t = nullptr) noexcept
        : target(t) {
    }

    ~VectorDeserializer() override = default;

private:
    template<typename ValueType>
    std::enable_if_t<has_deserializer_for_object_v<ValueType>, std::unique_ptr<VirtualHandler<rapidjson::UTF8<>>>> push_object_deserializer_or_null() {
        target->emplace_back();
        return deserializer_for_object_or_null(&target->back());
    }

    template<typename ValueType>
    std::enable_if_t<!has_deserializer_for_object_v<ValueType>, std::unique_ptr<VirtualHandler<rapidjson::UTF8<>>>> push_object_deserializer_or_null() {
        return nullptr;
    }

    template<typename ValueType>
    std::enable_if_t<has_deserializer_for_array_v<ValueType>, std::unique_ptr<VirtualHandler<rapidjson::UTF8<>>>> push_vector_deserializer_or_null() {
        target->emplace_back();
        return deserializer_for_array_or_null(&target->back());
    }

    template<typename ValueType>
    std::enable_if_t<!has_deserializer_for_array_v<ValueType>, std::unique_ptr<VirtualHandler<rapidjson::UTF8<>>>> push_vector_deserializer_or_null() {
        return nullptr;
    }

public:
    std::unique_ptr<VirtualHandler<rapidjson::UTF8<>>> deserializer_for_object() override {
        return push_object_deserializer_or_null<value_type>();
    }

    std::unique_ptr<VirtualHandler<rapidjson::UTF8<>>> deserializer_for_array() override {
        return push_vector_deserializer_or_null<value_type>();
    }

    bool complete() override {
        return true;
    }

private:
    template<typename DeserializedType>
    std::enable_if_t<is_value_deserializable_v<DeserializedType, value_type>, bool> push_value(DeserializedType deserialized_value) {
        target->emplace_back();
        deserialize_value(target->back(), deserialized_value);
        return true;
    }

    template<typename DeserializedType>
    std::enable_if_t<!is_value_deserializable_v<DeserializedType, value_type>, bool> push_value(DeserializedType deserialized_value) {
        return false;
    }

public:
    bool Null() override { return push_value(nullptr); }
    bool Bool(bool deserialized_value) override { return push_value(deserialized_value); }
    bool Int(int deserialized_value) override { return push_value(deserialized_value); }
    bool Uint(unsigned int deserialized_value) override { return push_value(deserialized_value); }
    bool Int64(int64_t deserialized_value) override { return push_value(deserialized_value); }
    bool Uint64(uint64_t deserialized_value) override { return push_value(deserialized_value); }
    bool Double(double deserialized_value) override { return push_value(deserialized_value); }
    bool String(const char* str, rapidjson::SizeType size, bool copy) override { return push_value(str); }

    bool Key(const char* str, rapidjson::SizeType size, bool copy) override {
        return false;
    }
};

template<typename Type>
struct DeserializerForArray<Type, std::enable_if_t<is_vector_v<Type>&& is_deserializable_v<typename Type::value_type>>> {
    using type = VectorDeserializer<Type>;
};

template<typename Type>
struct ObjectDeserializer : public VirtualHandler<rapidjson::UTF8<>> {
    using target_type = Type; // the type to deserialize
    using member_data_tuple_type = decltype(serialization<target_type>::member_data); // a tuple of { (const char*) member name, (member type target_type::*) member pointer }

private:
    static constexpr size_t state_expect_key = 0; // expecting Key()
    // I > 0 represents expecting value for member[I - 1]
    size_t state{ state_expect_key };

public:
    target_type* target;

    explicit ObjectDeserializer(target_type* t = nullptr) noexcept
        : target(t) {
    }

    ~ObjectDeserializer() override = default;

private:
    template<size_t I>
    std::enable_if_t<I < std::tuple_size_v<member_data_tuple_type>, std::unique_ptr<VirtualHandler<rapidjson::UTF8<>>>> object_deserializer_for_state() {
        if (state == (I + 1)) {
            state = state_expect_key;
            return deserializer_for_object_or_null(&(target->*std::get<1>(std::get<I>(serialization<target_type>::member_data))));
        }
        else {
            return object_deserializer_for_state<I + 1>();
        }
    }

    template<size_t I>
    std::enable_if_t<I == std::tuple_size_v<member_data_tuple_type>, std::unique_ptr<VirtualHandler<rapidjson::UTF8<>>>> object_deserializer_for_state() {
        return nullptr;
    }

    template<size_t I>
    std::enable_if_t<I < std::tuple_size_v<member_data_tuple_type>, std::unique_ptr<VirtualHandler<rapidjson::UTF8<>>>> vector_deserializer_for_state() {
        if (state == (I + 1)) {
            state = state_expect_key;
            return deserializer_for_array_or_null(&(target->*std::get<1>(std::get<I>(serialization<target_type>::member_data))));
        }
        else {
            return vector_deserializer_for_state<I + 1>();
        }
    }

    template<size_t I>
    std::enable_if_t<I == std::tuple_size_v<member_data_tuple_type>, std::unique_ptr<VirtualHandler<rapidjson::UTF8<>>>> vector_deserializer_for_state() {
        return nullptr;
    }

public:
    std::unique_ptr<VirtualHandler<rapidjson::UTF8<>>> deserializer_for_object() override {
        return object_deserializer_for_state<0>();
    }

    std::unique_ptr<VirtualHandler<rapidjson::UTF8<>>> deserializer_for_array() override {
        return vector_deserializer_for_state<0>();
    }

    bool complete() override {
        return true;
    }

private:
    template<size_t I, typename DeserializedType>
    std::enable_if_t<is_value_deserializable_v<remove_cvref_t<DeserializedType>, pointer_to_member_t<std::tuple_element_t<1, std::tuple_element_t<I, member_data_tuple_type>>>>, bool> deserialize_member(DeserializedType&& deserialized_value) {
        deserialize_value(target->*std::get<1>(std::get<I>(serialization<target_type>::member_data)), std::forward<DeserializedType>(deserialized_value));
        state = state_expect_key;
        return true;
    }

    template<size_t I, typename DeserializedType>
    std::enable_if_t<!is_value_deserializable_v<remove_cvref_t<DeserializedType>, pointer_to_member_t<std::tuple_element_t<1, std::tuple_element_t<I, member_data_tuple_type>>>>, bool> deserialize_member(DeserializedType&& deserialized_value) {
        return false;
    }

    template<size_t I, typename DeserializedType>
    std::enable_if_t<I < std::tuple_size_v<member_data_tuple_type>, bool> set_member_for_state(DeserializedType&& deserialized_value) {
        if (state == (I + 1)) {
            return deserialize_member<I>(std::forward<DeserializedType>(deserialized_value));
        }
        else {
            return set_member_for_state<I + 1>(std::forward<DeserializedType>(deserialized_value));
        }
    }

    template<size_t I, typename DeserializedType>
    std::enable_if_t<I == std::tuple_size_v<member_data_tuple_type>, bool> set_member_for_state(DeserializedType&& deserialized_value) {
        return false;
    }

public:
    bool Null() override { return set_member_for_state<0>(nullptr); }
    bool Bool(bool deserialized_value) override { return set_member_for_state<0>(deserialized_value); }
    bool Int(int deserialized_value) override { return set_member_for_state<0>(deserialized_value); }
    bool Uint(unsigned int deserialized_value) override { return set_member_for_state<0>(deserialized_value); }
    bool Int64(int64_t deserialized_value) override { return set_member_for_state<0>(deserialized_value); }
    bool Uint64(uint64_t deserialized_value) override { return set_member_for_state<0>(deserialized_value); }
    bool Double(double deserialized_value) override { return set_member_for_state<0>(deserialized_value); }
    bool String(const char* str, rapidjson::SizeType size, bool copy) override { return set_member_for_state<0>(str); }

private:
    template<size_t I>
    std::enable_if_t<I < std::tuple_size_v<member_data_tuple_type>, bool> set_expect_member_value(const char* str) {
        const auto& ith_field_name = std::get<0>(std::get<I>(serialization<target_type>::member_data));
        // todo: case insensitive search
        if (std::strcmp(ith_field_name, str) == 0) {
            state = I + 1;
            return true;
        }
        else {
            return set_expect_member_value<I + 1>(str);
        }
    }

    template<size_t I>
    std::enable_if_t<I == std::tuple_size_v<member_data_tuple_type>, bool> set_expect_member_value(const char* str) {
        return false;
    }

public:
    bool Key(const char* str, rapidjson::SizeType size, bool copy) override {
        if (state == state_expect_key) {
            return set_expect_member_value<0>(str);
        }
        else {
            return false;
        }
    }
};

template<typename Type>
struct DeserializerForObject<Type, std::enable_if_t<has_serialization_member_data_v<Type>>> {
    using type = ObjectDeserializer<Type>;
};

template<typename Type>
struct ExpectDeserializer final : public VirtualHandler<rapidjson::UTF8<>> {
    using target_type = Type; // the type to deserialize

    target_type* target;

    explicit ExpectDeserializer(target_type* t = nullptr) noexcept
        : target(t) {
    }

    ~ExpectDeserializer() override = default;

    std::unique_ptr<VirtualHandler<rapidjson::UTF8<>>> deserializer_for_object() final {
        return deserializer_for_object_or_null(target);
    }

    std::unique_ptr<VirtualHandler<rapidjson::UTF8<>>> deserializer_for_array() final {
        return deserializer_for_array_or_null(target);
    }

    bool complete() override {
        return true;
    }

private:
    template<typename DeserializedType>
    std::enable_if_t<is_value_deserializable_v<remove_cvref_t<DeserializedType>, target_type>, bool> deserialize_expected(DeserializedType&& deserialized_value) {
        deserialize_value(*target, std::forward<DeserializedType>(deserialized_value));
        return true;
    }

    template<typename DeserializedType>
    std::enable_if_t<!is_value_deserializable_v<remove_cvref_t<DeserializedType>, target_type>, bool> deserialize_expected(DeserializedType&& deserialized_value) {
        return false;
    }

public:
    bool Null() final { return deserialize_expected(nullptr); }
    bool Bool(bool deserialized_value) final { return deserialize_expected(deserialized_value); }
    bool Int(int deserialized_value) final { return deserialize_expected(deserialized_value); }
    bool Uint(unsigned int deserialized_value) final { return deserialize_expected(deserialized_value); }
    bool Int64(int64_t deserialized_value) final { return deserialize_expected(deserialized_value); }
    bool Uint64(uint64_t deserialized_value) final { return deserialize_expected(deserialized_value); }
    bool Double(double deserialized_value) final { return deserialize_expected(deserialized_value); }
    bool String(const char* str, rapidjson::SizeType size, bool copy) final { return deserialize_expected(str); }

    bool Key(const char* str, rapidjson::SizeType size, bool copy) final {
        return false;
    }
};

template<typename Type>
struct TupleDeserializer : public VirtualHandler<rapidjson::UTF8<>> {
    using target_type = Type; // the vector type to deserialize
    inline static constexpr size_t tuple_size = std::tuple_size_v<Type>;

private:
    // I represents expecting value for member[I]
    size_t state{ 0 };

public:
    target_type* target;

    explicit TupleDeserializer(target_type* t = nullptr) noexcept
        : target(t) {
    }

    ~TupleDeserializer() override = default;
    
private:
    template<size_t I>
    std::enable_if_t<I < tuple_size, std::unique_ptr<VirtualHandler<rapidjson::UTF8<>>>> object_deserializer_for_state() {
        if (state == I) {
            ++state;
            return deserializer_for_object_or_null(&std::get<I>(*target));
        }
        else {
            return object_deserializer_for_state<I + 1>();
        }
    }

    template<size_t I>
    std::enable_if_t<I == tuple_size, std::unique_ptr<VirtualHandler<rapidjson::UTF8<>>>> object_deserializer_for_state() {
        return nullptr;
    }

    template<size_t I>
    std::enable_if_t<I < tuple_size, std::unique_ptr<VirtualHandler<rapidjson::UTF8<>>>> vector_deserializer_for_state() {
        if (state == I) {
            ++state;
            return deserializer_for_array_or_null(&std::get<I>(*target));
        }
        else {
            return vector_deserializer_for_state<I + 1>();
        }
    }

    template<size_t I>
    std::enable_if_t<I == tuple_size, std::unique_ptr<VirtualHandler<rapidjson::UTF8<>>>> vector_deserializer_for_state() {
        return nullptr;
    }

public:
    std::unique_ptr<VirtualHandler<rapidjson::UTF8<>>> deserializer_for_object() override {
        return object_deserializer_for_state<0>();
    }

    std::unique_ptr<VirtualHandler<rapidjson::UTF8<>>> deserializer_for_array() override {
        return vector_deserializer_for_state<0>();
    }

    bool complete() override {
        return true;
    }

private:
    template<size_t I, typename DeserializedType>
    std::enable_if_t<is_value_deserializable_v<remove_cvref_t<DeserializedType>, std::tuple_element_t<I, target_type>>, bool> deserialize_member(DeserializedType&& deserialized_value) {
        deserialize_value(std::get<I>(*target), std::forward<DeserializedType>(deserialized_value));
        ++state;
        return true;
    }

    template<size_t I, typename DeserializedType>
    std::enable_if_t<!is_value_deserializable_v<remove_cvref_t<DeserializedType>, std::tuple_element_t<I, target_type>>, bool> deserialize_member(DeserializedType&& deserialized_value) {
        return false;
    }

    template<size_t I, typename DeserializedType>
    std::enable_if_t<I < tuple_size, bool> set_member_for_state(DeserializedType&& deserialized_value) {
        if (state == I) {
            return deserialize_member<I>(std::forward<DeserializedType>(deserialized_value));
        }
        else {
            return set_member_for_state<I + 1>(std::forward<DeserializedType>(deserialized_value));
        }
    }

    template<size_t I, typename DeserializedType>
    std::enable_if_t<I == tuple_size, bool> set_member_for_state(DeserializedType&& deserialized_value) {
        return false;
    }

public:
    bool Null() override { return set_member_for_state<0>(nullptr); }
    bool Bool(bool deserialized_value) override { return set_member_for_state<0>(deserialized_value); }
    bool Int(int deserialized_value) override { return set_member_for_state<0>(deserialized_value); }
    bool Uint(unsigned int deserialized_value) override { return set_member_for_state<0>(deserialized_value); }
    bool Int64(int64_t deserialized_value) override { return set_member_for_state<0>(deserialized_value); }
    bool Uint64(uint64_t deserialized_value) override { return set_member_for_state<0>(deserialized_value); }
    bool Double(double deserialized_value) override { return set_member_for_state<0>(deserialized_value); }
    bool String(const char* str, rapidjson::SizeType size, bool copy) override { return set_member_for_state<0>(str); }

    bool Key(const char* str, rapidjson::SizeType size, bool copy) override {
        return false;
    }
};

template<typename Type>
struct DeserializerForArray<Type, std::enable_if_t<is_tuple_v<Type>>> {
    using type = TupleDeserializer<Type>;
};

// T::operator =(T&&) is always valid
template<typename To, typename From>
std::enable_if_t<std::is_same_v<remove_cvref_t<To>, remove_cvref_t<From>>> deserialize_value(To& member, From&& deserialized_value) {
    member = std::forward<From>(deserialized_value);
}

// enum from integral types
template<typename To, typename From>
std::enable_if_t<std::is_enum_v<remove_cvref_t<To>> && std::is_integral_v<remove_cvref_t<From>>> deserialize_value(To& member, From&& deserialized_value) {
    member = To(deserialized_value);
}

void deserialize_value(float& member, double deserialized_value) { member = deserialized_value; }
void deserialize_value(long double& member, double deserialized_value) { member = deserialized_value; }
void deserialize_value(short int& member, int deserialized_value) { member = deserialized_value; }
void deserialize_value(short int& member, long deserialized_value) { member = deserialized_value; }
void deserialize_value(short int& member, long long deserialized_value) { member = deserialized_value; }
void deserialize_value(int& member, short int deserialized_value) { member = deserialized_value; }
void deserialize_value(int& member, long deserialized_value) { member = deserialized_value; }
void deserialize_value(int& member, long long deserialized_value) { member = deserialized_value; }
void deserialize_value(long& member, short int deserialized_value) { member = deserialized_value; }
void deserialize_value(long& member, int deserialized_value) { member = deserialized_value; }
void deserialize_value(long& member, long long deserialized_value) { member = deserialized_value; }
void deserialize_value(long long& member, short int deserialized_value) { member = deserialized_value; }
void deserialize_value(long long& member, int deserialized_value) { member = deserialized_value; }
void deserialize_value(long long& member, long deserialized_value) { member = deserialized_value; }
void deserialize_value(unsigned short int& member, unsigned int deserialized_value) { member = deserialized_value; }
void deserialize_value(unsigned short int& member, unsigned long deserialized_value) { member = deserialized_value; }
void deserialize_value(unsigned short int& member, unsigned long long deserialized_value) { member = deserialized_value; }
void deserialize_value(unsigned int& member, unsigned short int deserialized_value) { member = deserialized_value; }
void deserialize_value(unsigned int& member, unsigned long deserialized_value) { member = deserialized_value; }
void deserialize_value(unsigned int& member, unsigned long long deserialized_value) { member = deserialized_value; }
void deserialize_value(unsigned long& member, unsigned short int deserialized_value) { member = deserialized_value; }
void deserialize_value(unsigned long& member, unsigned int deserialized_value) { member = deserialized_value; }
void deserialize_value(unsigned long& member, unsigned long long deserialized_value) { member = deserialized_value; }
void deserialize_value(unsigned long long& member, unsigned short int deserialized_value) { member = deserialized_value; }
void deserialize_value(unsigned long long& member, unsigned int deserialized_value) { member = deserialized_value; }
void deserialize_value(unsigned long long& member, unsigned long deserialized_value) { member = deserialized_value; }
void deserialize_value(std::string& member, const char* str) { member = str; }

template<typename Writer>
void serialize(Writer& writer, const std::string& value) {
    writer.String(value.c_str(), value.size(), true);
}

template<typename Writer>
void serialize(Writer& writer, bool value) {
    writer.Bool(value);
}

template<typename Writer>
void serialize(Writer& writer, short int value) {
    writer.Int(value);
}

template<typename Writer>
void serialize(Writer& writer, unsigned short int value) {
    writer.Uint(value);
}

template<typename Writer>
void serialize(Writer& writer, int value) {
    writer.Int(value);
}

template<typename Writer>
void serialize(Writer& writer, unsigned int value) {
    writer.Uint(value);
}

template<typename Writer>
void serialize(Writer& writer, long value) {
    writer.Int64(value);
}

template<typename Writer>
void serialize(Writer& writer, unsigned long value) {
    writer.Uint64(value);
}

template<typename Writer>
void serialize(Writer& writer, long long value) {
    writer.Int64(value);
}

template<typename Writer>
void serialize(Writer& writer, unsigned long long value) {
    writer.Uint64(value);
}

template<typename Writer>
void serialize(Writer& writer, float value) {
    writer.Double(value);
}

template<typename Writer>
void serialize(Writer& writer, double value) {
    writer.Double(value);
}

template<typename Writer>
void serialize(Writer& writer, long double value) {
    writer.Double(value);
}

template<typename Writer, typename ValueType>
void serialize(Writer& writer, const std::vector<ValueType>& value) {
    writer.StartArray();
    for (auto&& element : value) {
        serialize(writer, element);
    }
    writer.EndArray();
}

template<typename Writer, typename TupleType>
std::enable_if_t<is_tuple_v<TupleType>> serialize(Writer& writer, const TupleType& value) {
    writer.StartArray();
    call_over_tuple([&writer](const auto& element) {
            serialize(writer, element);
        }, value);
    writer.EndArray();
}

template<typename Writer, typename SourceType>
std::enable_if_t<has_serialization_member_data_v<remove_cvref_t<SourceType>>> serialize(Writer& writer, SourceType&& source) {
    writer.StartObject();
    call_over_tuple([&writer, &source](const auto& member_data_pair) {
            writer.Key(std::get<0>(member_data_pair));
            serialize(writer, source.*std::get<1>(member_data_pair));
        },
        serialization<remove_cvref_t<SourceType>>::member_data);
    writer.EndObject();
}

template<typename Writer, typename EnumType>
std::enable_if_t<std::is_enum_v<remove_cvref_t<EnumType>>> serialize(Writer& writer, EnumType&& source) {
    serialize(writer, std::underlying_type_t<remove_cvref_t<EnumType>>( source ));
}
