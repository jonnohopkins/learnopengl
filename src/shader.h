#pragma once

#include <glad/glad.h>
#include <glm/glm.hpp>

class Shader {
public:
    GLuint ID;

    Shader() noexcept :
        ID(0) {
    }

    Shader(const Shader&) = delete;

    Shader(Shader&& rhs) noexcept
        : ID(rhs.ID) {
        rhs.ID = 0;
    }

    Shader& operator=(const Shader&) = delete;

    Shader& operator=(Shader&& rhs) noexcept {
        glDeleteProgram(ID);
        ID = rhs.ID;
        rhs.ID = 0;
        return *this;
    }

    // provides the strong exception guarantee
    void create_regular(const char* vertexPath, const char* fragmentPath, const char* geometryPath = nullptr);

    // provides the strong exception guarantee
    void create_compute(const char* computePath);

    ~Shader() {
        if(ID != 0)
            glDeleteProgram(ID);
    }

    void use() {
        glUseProgram(ID);
    }

    void setBool(const char* name, bool value) {         
        glUniform1i(glGetUniformLocation(ID, name), (int)value); 
    }
    void setInt(const char* name, int value) { 
        glUniform1i(glGetUniformLocation(ID, name), value); 
    }
    void setUint(const char* name, unsigned int value) {
        glUniform1ui(glGetUniformLocation(ID, name), value);
    }
    void setFloat(const char* name, float value) { 
        glUniform1f(glGetUniformLocation(ID, name), value); 
    }
    void setVec2(const char* name, const glm::vec2 &value) { 
        glUniform2fv(glGetUniformLocation(ID, name), 1, &value[0]); 
    }
    void setVec2(const char* name, float x, float y) { 
        glUniform2f(glGetUniformLocation(ID, name), x, y); 
    }
    void setVec3(const char* name, const glm::vec3 &value) { 
        glUniform3fv(glGetUniformLocation(ID, name), 1, &value[0]); 
    }
    void setVec3(const char* name, float x, float y, float z) { 
        glUniform3f(glGetUniformLocation(ID, name), x, y, z); 
    }
    void setVec4(const char* name, const glm::vec4 &value) { 
        glUniform4fv(glGetUniformLocation(ID, name), 1, &value[0]); 
    }
    void setVec4(const char* name, float x, float y, float z, float w) { 
        glUniform4f(glGetUniformLocation(ID, name), x, y, z, w); 
    }
    void setMat2(const char* name, const glm::mat2 &mat) {
        glUniformMatrix2fv(glGetUniformLocation(ID, name), 1, GL_FALSE, &mat[0][0]);
    }
    void setMat3(const char* name, const glm::mat3 &mat) {
        glUniformMatrix3fv(glGetUniformLocation(ID, name), 1, GL_FALSE, &mat[0][0]);
    }
    void setMat4(const char* name, const glm::mat4 &mat) {
        glUniformMatrix4fv(glGetUniformLocation(ID, name), 1, GL_FALSE, &mat[0][0]);
    }
};
