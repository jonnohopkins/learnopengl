#include "editor.h"

#include <functional>
#include <algorithm>

#include "entity.h"
#include "renderer.h"
#include "scene.h"
#include "assets.h"
#include "globals.h"
#include "asset_save_load.h"
#include <native_ui.h>

#include <imgui.h>
#include <imgui_impl_glfw.h>
#include <imgui_impl_opengl3.h>
#include <misc/cpp/imgui_stdlib.h>

Editor editor;

float bound_degrees(float angle) {
    if (angle < -180.0f)
        return angle + 360.0f;
    else if (angle > 180.0f)
        return angle - 360.0f;
    else
        return angle;
}

// Option must be equality comparable and assignable (an enum fulfills this).
// Collection is a collection of { const char* label, Option value }
template<typename Option, typename Collection>
bool radio_buttons(Option& option, const Collection& radio_options) {
    bool value_changed = false;
    for (auto [label, value] : radio_options) {
        static_assert(std::is_same_v<Option, decltype(value)>, "types should be the same.");
        if (ImGui::RadioButton(label, option == value)) {
            option = value;
            value_changed = true;
        }
    }

    return value_changed;
}

template<typename Collection, typename Iterator, typename FuncIteratorToName>
bool combo_selector(const char* label, const Collection& collection, Iterator* it_selected, FuncIteratorToName iter_to_name) {
    bool value_changed = false;

    if (ImGui::BeginCombo(label, iter_to_name(*it_selected))) {
        for (auto it = collection.cbegin(); it != collection.cend(); ++it) {
            const bool is_selected = (it == *it_selected);
            if (ImGui::Selectable(iter_to_name(it), is_selected)) {
                *it_selected = it;
                value_changed = true;
            }

            if (is_selected)
                ImGui::SetItemDefaultFocus();
        }

        ImGui::EndCombo();
    }

    return value_changed;
}

bool TableRowXY(const char* row_id, const char* row_label, glm::vec2& value, float v_speed) {
    bool value_changed;

    ImGui::PushID(row_id);
    ImGui::TableNextRow();
    ImGui::TableNextColumn();
    ImGui::Text(row_label);

    ImGui::TableNextColumn();
    ImGui::Text("X");
    ImGui::SameLine();
    value_changed = ImGui::DragFloat("##RowX", &value.x, v_speed);

    ImGui::TableNextColumn();
    ImGui::Text("Y");
    ImGui::SameLine();
    value_changed = ImGui::DragFloat("##RowY", &value.y, v_speed) || value_changed;
    ImGui::PopID();

    return value_changed;
}

bool TableRowXYZ(const char* row_id, const char* row_label, glm::vec3& value, float v_speed) {
    bool value_changed;

    ImGui::PushID(row_id);
    ImGui::TableNextRow();
    ImGui::TableNextColumn();
    ImGui::Text(row_label);

    ImGui::TableNextColumn();
    ImGui::Text("X");
    ImGui::SameLine();
    value_changed = ImGui::DragFloat("##RowX", &value.x, v_speed);

    ImGui::TableNextColumn();
    ImGui::Text("Y");
    ImGui::SameLine();
    value_changed = ImGui::DragFloat("##RowY", &value.y, v_speed) || value_changed;

    ImGui::TableNextColumn();
    ImGui::Text("Z");
    ImGui::SameLine();
    value_changed = ImGui::DragFloat("##RowZ", &value.z, v_speed) || value_changed;
    ImGui::PopID();

    return value_changed;
}

void Editor::manipulators() {
    const ImGuiWindowFlags window_flags = ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoCollapse;

    bool show = true;
    if (ImGui::Begin("Manipulator Controls", &show, window_flags)) {
        constexpr std::array<std::pair<const char*, ImGuizmo::OPERATION>, 3> operations{
            std::pair<const char*, ImGuizmo::OPERATION>{ "Translate", ImGuizmo::OPERATION::TRANSLATE },
            std::pair<const char*, ImGuizmo::OPERATION>{ "Rotate", ImGuizmo::OPERATION::ROTATE },
            std::pair<const char*, ImGuizmo::OPERATION>{ "Scale", ImGuizmo::OPERATION::SCALE }
        };

        radio_buttons(manipulator_operation, operations);

        ImGui::Separator();

        constexpr std::array<std::pair<const char*, ImGuizmo::MODE>, 2> modes{
            std::pair<const char*, ImGuizmo::MODE>{ "World", ImGuizmo::MODE::WORLD },
            std::pair<const char*, ImGuizmo::MODE>{ "Local", ImGuizmo::MODE::LOCAL }
        };

        radio_buttons(manipulator_mode, modes);

        ImGui::End();
    }

    glm::mat4 view = renderer.camera.transform.GetViewMatrix();
    glm::mat4 proj = glm::perspective(glm::radians(camera_fovy), (float) renderer.width / (float) renderer.height, camera_near, camera_far);

    for (auto [selected_scene_id, selected_entity_id] : entities_selected) {
        auto [it_scene_lower, it_scene_upper] = equal_range(scenes, selected_scene_id);
        ASSERT(std::distance(it_scene_lower, it_scene_upper) == 1, "scene expected.");
        Scene& selected_scene = it_scene_lower->second;

        auto [it_entity_lower, it_entity_upper] = equal_range(selected_scene.entities, selected_entity_id);
        auto [it_transform_lower, it_transform_upper] = equal_range(selected_scene.transforms, selected_entity_id);

        if (it_entity_lower != it_entity_upper && it_transform_lower != it_transform_upper) {
            const Entity& selected_entity = it_entity_lower->second;

            glm::mat4 parent_mat;
            world_space_matrix(selected_scene, selected_entity.parent, parent_mat, false);

            glm::mat4 model = parent_mat * it_transform_lower->second.GetMatrix();

            ImGuizmo::SetID(selected_scene_id ^ selected_entity_id);
            bool manipulated = ImGuizmo::Manipulate(&view[0][0], &proj[0][0], manipulator_operation, manipulator_mode, &model[0][0]);
            if (manipulated) {
                it_transform_lower->second = Transform{ glm::inverse(parent_mat) * model };
            }
        }
    }
}

void Editor::display(bool& mesh_buffer_requires_update, bool& light_shadow_info_requires_update) {
    mesh_buffer_requires_update = false;
    light_shadow_info_requires_update = false;

    if (interacting_with_ui) {
        show_main_menu();

        if (show_bloom_window)
            bloom_window();

        if (show_demo_window)
            ImGui::ShowDemoWindow(&show_demo_window);

        if (show_scene_window) {
            bool scene_updated = scene_window();
            mesh_buffer_requires_update = scene_updated || mesh_buffer_requires_update;
            light_shadow_info_requires_update = scene_updated || light_shadow_info_requires_update;
        }

        if (show_entity_window) {
            bool entity_update, transform_update, mesh_updated, light_updated;
            entity_window(entity_update, transform_update, mesh_updated, light_updated);
            mesh_buffer_requires_update = mesh_buffer_requires_update || entity_update || transform_update || mesh_updated;
            light_shadow_info_requires_update = light_shadow_info_requires_update || entity_update || transform_update || light_updated;
        }

        if (show_prefab_window) {
            prefabs_window();
        }

        if (show_model_window) {
            mesh_buffer_requires_update = models_window() || mesh_buffer_requires_update;
        }

        if (show_physics_2d_window) {
            physics_2d_window();
        }

        manipulators();
    }
}

void Editor::show_main_menu() {
    if (ImGui::BeginMainMenuBar()) {
        ImGui::MenuItem("Scene", nullptr, &show_scene_window);
        ImGui::MenuItem("Entity", nullptr, &show_entity_window);
        if (ImGui::BeginMenu("View")) {
            if (ImGui::BeginMenu("Shader")) {
                constexpr std::array<std::pair<const char*, ShaderView>, 8> shader_views_regular{
                    std::pair<const char*, ShaderView>{ "Unlit", ShaderView::Unlit },
                    std::pair<const char*, ShaderView>{ "Unlit (Instanced)", ShaderView::Unlit_Instanced },
                    std::pair<const char*, ShaderView>{ "Phong", ShaderView::Lit_Phong },
                    std::pair<const char*, ShaderView>{ "Blinn-Phong", ShaderView::Lit_Blinn_Phong },
                    std::pair<const char*, ShaderView>{ "Blinn-Phong Shadow", ShaderView::Lit_Blinn_Phong_Shadow },
                    std::pair<const char*, ShaderView>{ "Deferred Shading", ShaderView::Deferred_Shading },
                    std::pair<const char*, ShaderView>{ "Reflect", ShaderView::Reflect },
                    std::pair<const char*, ShaderView>{ "Refract", ShaderView::Refract }
                };
                radio_buttons(renderer.shader_view, shader_views_regular);

                if (ImGui::BeginMenu("Debug")) {
                    constexpr std::array<std::pair<const char*, ShaderView>, 4> shader_views_debug{
                        std::pair<const char*, ShaderView>{ "Depth", ShaderView::Debug_Depth },
                        std::pair<const char*, ShaderView>{ "Normal - Local Space", ShaderView::Debug_Normal_Local },
                        std::pair<const char*, ShaderView>{ "Normal - World Space", ShaderView::Debug_Normal_World },
                        std::pair<const char*, ShaderView>{ "Wireframe", ShaderView::Debug_Wireframe }
                    };

                    radio_buttons(renderer.shader_view, shader_views_debug);
                    ImGui::EndMenu();
                }
                ImGui::EndMenu();
            }
            ImGui::DragFloat("Exposure", &renderer.exposure, 0.05f, 0.0f, std::numeric_limits<float>::max());
            ImGui::Checkbox("Skybox", &renderer.use_skybox);
            ImGui::Checkbox("Normals", &renderer.use_normals_rendering);
            ImGui::Checkbox("Post Process", &renderer.use_postprocess);
            ImGui::MenuItem("Bloom Controls", nullptr, &show_bloom_window);
            if (ImGui::BeginMenu("Anti-Aliasing")) {
                constexpr std::array<std::pair<const char*, size_t>, 4> msaa_samples{
                    std::pair<const char*, size_t>{ "None", 1 },
                    std::pair<const char*, size_t>{ "2x MSAA", 2 },
                    std::pair<const char*, size_t>{ "4x MSAA", 4 },
                    std::pair<const char*, size_t>{ "8x MSAA", 8 }
                };

                for (auto [label, value] : msaa_samples) {
                    if (ImGui::RadioButton(label, renderer.msaa_samples == value))
                        renderer.set_msaa_samples(value);
                }
                ImGui::EndMenu();
            }
            bool vsync = renderer.vsync_enabled;
            if (ImGui::Checkbox("V-Sync", &vsync))
                renderer.set_vsync(vsync);
            ImGui::EndMenu();
        }
        ImGui::MenuItem("Prefabs", nullptr, &show_prefab_window);
        ImGui::MenuItem("Physics 2D", nullptr, &show_physics_2d_window);
        ImGui::MenuItem("Models", nullptr, &show_model_window);
        ImGui::MenuItem("ImGui Demo", nullptr, &show_demo_window);
        int fps = 1.0f / renderer.delta_time;
        ImGui::Text("FPS: %i", fps);
        ImGui::EndMainMenuBar();
    }
}

const char* instantiate_prefab_drag_drop_name = "INSTANTIATE_PREFAB_ENTITY";

bool Editor::scene_window() {
    bool value_changed = false;
    const ImGuiWindowFlags window_flags = ImGuiWindowFlags_NoCollapse;
    ImGui::SetNextWindowSizeConstraints(ImVec2(200, 100), ImVec2(FLT_MAX, FLT_MAX));
    if (ImGui::Begin("Scene", &show_scene_window, window_flags)) {
        if (ImGui::BeginPopupContextItem()) // <-- use last item id as popup id
        {
            if (ImGui::Button("+Scene")) {
                ImGui::CloseCurrentPopup();

                auto& [scene_id, scene] = *emplace(scenes, new_scene_id(), Scene{});
                active_scenes.push_back(scene_id);

                value_changed = true;
            }

            if (ImGui::Button("Load")) {
                ImGui::CloseCurrentPopup();

                static constexpr FileFilterSpec file_types[] =
                {
                    {L"JSON",       L"*.json"}
                };

                bool user_selected_file;
                std::vector<std::string> paths;
                if (file_open_dialog(paths, user_selected_file, file_types, std::size(file_types), false) && user_selected_file) {
                    load_scene_file(paths[0].c_str(), &value_changed);
                }
            }

            ImGui::EndPopup();
        }

        bool add_command = false;
        scene_id_t add_scene_id;
        entity_id_t add_parent_entity_id;
        bool remove_command = false;
        scene_id_t remove_scene_id;
        entity_id_t remove_entity_id;

        auto EntityContextItem = [&add_command, &add_scene_id, &add_parent_entity_id, &remove_command, &remove_scene_id, &remove_entity_id](scene_id_t scene_id, entity_id_t entity_id) {
            if (ImGui::BeginPopupContextItem()) // <-- use last item id as popup id
            {
                if (ImGui::Button("+")) {
                    ImGui::CloseCurrentPopup();

                    add_command = true;
                    add_scene_id = scene_id;
                    add_parent_entity_id = entity_id;
                }

                ImGui::SameLine();

                if (ImGui::Button("-")) {
                    ImGui::CloseCurrentPopup();

                    remove_command = true;
                    remove_scene_id = scene_id;
                    remove_entity_id = entity_id;
                }

                ImGui::EndPopup();
            }
        };
        
        auto SceneContextItem = [&value_changed, &add_command, &add_scene_id, &add_parent_entity_id, &remove_command, &remove_scene_id, &remove_entity_id](scene_id_t scene_id) {
            if (ImGui::BeginPopupContextItem()) // <-- use last item id as popup id
            {
                if (ImGui::Button("+")) {
                    ImGui::CloseCurrentPopup();

                    add_command = true;
                    add_scene_id = scene_id;
                    add_parent_entity_id = 0;
                }

                ImGui::SameLine();

                if (ImGui::Button("-")) {
                    ImGui::CloseCurrentPopup();

                    remove_command = true;
                    remove_scene_id = scene_id;
                    remove_entity_id = 0;
                }

                ImGui::SameLine();

                if (ImGui::Button("Save")) {
                    ImGui::CloseCurrentPopup();

                    static constexpr FileFilterSpec file_types[] =
                    {
                        {L"JSON",       L"*.json"}
                    };

                    bool user_saved_file;
                    std::string path;
                    if (file_save_dialog(path, user_saved_file, file_types, std::size(file_types), L"json") && user_saved_file) {
                        auto [it_scene_lower, it_scene_upper] = equal_range(scenes, scene_id);
                        ASSERT(std::distance(it_scene_lower, it_scene_upper) == 1, "scene expected.");
                        Scene& scene = it_scene_lower->second;

                        save_scene_file(path.c_str(), scene);
                    }
                }

                ImGui::EndPopup();
            }
        };

        std::function<void(scene_id_t, Scene&, entity_id_t, Entity*)> display_entity =
            [this, &value_changed, &display_entity, &EntityContextItem, &SceneContextItem](scene_id_t scene_id, Scene& scene, entity_id_t entity_id, Entity* entity) {
            ImGui::TableNextRow();

            std::vector<entity_id_t>& children = entity ? entity->children : scene.root_entities;
            std::string name = entity ? entity->name : ("Scene " + std::to_string(scene_id));

            auto scene_entity_pair = std::make_pair(scene_id, entity_id);
            auto [it_selected_lower, it_selected_upper] = std::equal_range(entities_selected.begin(), entities_selected.end(), scene_entity_pair);
            bool is_selected = (it_selected_lower != it_selected_upper);

            ImGui::PushID((void*) (intptr_t) entity_id);

            ImGui::TableNextColumn();

            ImGuiTreeNodeFlags node_flags = ImGuiTreeNodeFlags_OpenOnArrow | ImGuiTreeNodeFlags_OpenOnDoubleClick | ImGuiTreeNodeFlags_SpanAvailWidth | ImGuiTreeNodeFlags_FramePadding | ImGuiTreeNodeFlags_SpanFullWidth;
            if (children.empty())
                node_flags |= ImGuiTreeNodeFlags_Bullet;
            if (is_selected)
                node_flags |= ImGuiTreeNodeFlags_Selected;

            bool is_tree_open = ImGui::TreeNodeEx("tree node", node_flags, name.c_str());

            const char* move_entity_drag_drop_name = "MOVE_ENTITY";
            if (entity_id != 0 && ImGui::BeginDragDropSource(ImGuiDragDropFlags_None)) {
                auto data = std::make_pair(scene_id, entity_id);
                ImGui::SetDragDropPayload(move_entity_drag_drop_name, &data, sizeof(data));
                ImGui::Text("Move %s", name.c_str());
                ImGui::EndDragDropSource();
            }

            if (ImGui::BeginDragDropTarget()) {
                if (const ImGuiPayload* payload = ImGui::AcceptDragDropPayload(move_entity_drag_drop_name)) {
                    std::pair<scene_id_t, entity_id_t> source;
                    ASSERT(payload->DataSize == sizeof(source), "unexpected size");
                    source = *(const decltype(source)*) payload->Data;

                    auto [it_scene_lower, it_scene_upper] = equal_range(scenes, source.first);
                    ASSERT(std::distance(it_scene_lower, it_scene_upper) == 1, "scene expected.");

                    reparent_entity(it_scene_lower->second, source.second, scene, entity_id);

                    value_changed = true;
                }

                ImGui::EndDragDropTarget();
            }

            if (ImGui::BeginDragDropTarget()) {
                if (const ImGuiPayload* payload = ImGui::AcceptDragDropPayload(instantiate_prefab_drag_drop_name)) {
                    ASSERT(payload->DataSize == sizeof(scene_id_t), "unexpected size");
                    scene_id_t prefab_scene_id = *(const scene_id_t*) payload->Data;

                    auto [it_scene_lower, it_scene_upper] = equal_range(scenes, prefab_scene_id);
                    ASSERT(std::distance(it_scene_lower, it_scene_upper) == 1, "scene expected.");
                    Scene& prefab_scene = it_scene_lower->second;

                    copy_entity(prefab_scene, prefab_scene.root_entities[0], scene, entity_id);

                    value_changed = true;
                }

                ImGui::EndDragDropTarget();
            }

            if (ImGui::IsItemClicked()) {
                if (!ImGui::GetIO().KeyCtrl) {
                    entities_selected.clear();
                    entities_selected.push_back(std::move(scene_entity_pair));
                }
                else {
                    if (is_selected) {
                        entities_selected.erase(it_selected_lower);
                    }
                    else {
                        entities_selected.emplace(std::lower_bound(entities_selected.begin(), entities_selected.end(), scene_entity_pair), scene_entity_pair);
                    }
                }
            }

            if (entity_id == 0)
                SceneContextItem(scene_id);
            else
                EntityContextItem(scene_id, entity_id);

            ImGui::TableNextColumn();

            if (entity) {
                bool is_active = entity->is_active;
                if (ImGui::Checkbox("##Active", &is_active)) {
                    set_active(scene_id, entity_id, *entity, is_active);
                    value_changed = true;
                }
            }
                
            ImGui::PopID();

            if (is_tree_open) {
                for (auto child_id : children) {
                    auto [it_child_lower, it_child_upper] = equal_range(scene.entities, child_id);
                    ASSERT(std::distance(it_child_lower, it_child_upper) == 1, "entity expected.");
                    display_entity(scene_id, scene, it_child_lower->first, &it_child_lower->second);
                }
                ImGui::TreePop();
            }
        };

        ImGuiTableFlags flags = ImGuiTableFlags_NoSavedSettings | ImGuiTableFlags_RowBg | ImGuiTableFlags_NoBordersInBody; // | ImGuiTableFlags_ScrollY
        if (ImGui::BeginTable("SceneTable", 2, flags)) {
            ImGui::TableSetupColumn("Name", ImGuiTableColumnFlags_NoHide);
            ImGui::TableSetupColumn("Active", ImGuiTableColumnFlags_WidthFixed, ImGui::GetFrameHeight());            

            for (scene_id_t scene_id : active_scenes) {
                auto [it_scene_lower, it_scene_upper] = equal_range(scenes, scene_id);
                ASSERT(std::distance(it_scene_lower, it_scene_upper) == 1, "scene expected.");
                Scene& scene = it_scene_lower->second;

                ImGui::PushID(scene_id);
                display_entity(scene_id, scene, 0, nullptr);
                ImGui::PopID();
            }

            ImGui::EndTable();
        }

        if (add_command) {
            auto [it_scene_lower, it_scene_upper] = equal_range(scenes, add_scene_id);
            ASSERT(std::distance(it_scene_lower, it_scene_upper) == 1, "scene expected.");
            Scene& add_to_scene = it_scene_lower->second;

            auto& [child_id, child_entity] = add_to_scene.create_entity_with_parent(add_parent_entity_id);
            child_entity.name = "entity";

            emplace(add_to_scene.transforms, child_id, Transform{});

            value_changed = true;
        }

        if (remove_command) {
            if (std::find(entities_selected.begin(), entities_selected.end(), std::make_pair(remove_scene_id, remove_entity_id)) != entities_selected.end()) {
                for (auto [ scene_id, entity_id ] : entities_selected) {
                    auto [it_scene_lower, it_scene_upper] = equal_range(scenes, scene_id);
                    ASSERT(std::distance(it_scene_lower, it_scene_upper) == 1, "scene expected.");
                    Scene& remove_from_scene = it_scene_lower->second;

                    if (entity_id == 0) {
                        auto [ it_active_scene_lower, it_active_scene_upper ] = std::equal_range(active_scenes.begin(), active_scenes.end(), scene_id);
                        ASSERT(std::distance(it_active_scene_lower, it_active_scene_upper) == 1, "scene expected.");
                        active_scenes.erase(it_active_scene_lower);
                    }
                    else {
                        remove_from_scene.delete_entity(entity_id);
                    }
                }
            }
            else {
                auto [it_scene_lower, it_scene_upper] = equal_range(scenes, remove_scene_id);
                ASSERT(std::distance(it_scene_lower, it_scene_upper) == 1, "scene expected.");
                Scene& remove_from_scene = it_scene_lower->second;

                if (remove_entity_id == 0) {
                    auto [it_active_scene_lower, it_active_scene_upper] = std::equal_range(active_scenes.begin(), active_scenes.end(), remove_scene_id);
                    ASSERT(std::distance(it_active_scene_lower, it_active_scene_upper) == 1, "scene expected.");
                    active_scenes.erase(it_active_scene_lower);
                }
                else {
                    remove_from_scene.delete_entity(remove_entity_id);
                }
            }

            entities_selected.clear();

            value_changed = true;
        }

        ImGui::End();
    }

    return value_changed;
}

bool transform_controls(Transform& transform) {
    bool value_changed = false;

    if (ImGui::BeginTable("TransformTable", 4)) {

        value_changed = TableRowXYZ("Position", "Position:\t", transform.Position, 0.05f) || value_changed;

        bool is_rotation_dirty = TableRowXYZ("Rotation", "Rotation:\t", transform.EulerAngles, 1.0f);
        if (is_rotation_dirty) {
            transform.EulerAngles.x = bound_degrees(transform.EulerAngles.x);
            transform.EulerAngles.y = bound_degrees(transform.EulerAngles.y);
            transform.EulerAngles.z = bound_degrees(transform.EulerAngles.z);
            transform.UpdateRotation();
            value_changed = true;
        }

        value_changed = TableRowXYZ("Scale", "Scale:\t", transform.Scale, 0.1f) || value_changed;

        ImGui::EndTable();
    }

    return value_changed;
}

bool mesh_controls(mesh_id_t& selected_mesh_id) {
    auto [it_mesh_lower, it_mesh_upper] = equal_range(meshes, selected_mesh_id);
    ASSERT(std::distance(it_mesh_lower, it_mesh_upper) <= 1, "mesh id was not unique.");

    bool value_changed = false;

    auto combo_element = [&selected_mesh_id, &value_changed](bool is_selected, const char* label, mesh_id_t select_id) {
        if (ImGui::Selectable(label, is_selected)) {
            selected_mesh_id = select_id;
            value_changed = true;
        }

        if (is_selected)
            ImGui::SetItemDefaultFocus();
    };

    char str_buffer[8];
    sprintf(str_buffer, "%i", (int) selected_mesh_id);
    const char* none_label = "None";
    const bool none_selected = (it_mesh_lower == it_mesh_upper);
    const char* preview = none_selected ? none_label : str_buffer;

    if (ImGui::BeginCombo("Mesh##Path", preview)) {
        combo_element(none_selected, none_label, 0);

        for (const auto& [ id, mesh ] : meshes) {
            const bool is_selected = (id == selected_mesh_id);
            sprintf(str_buffer, "%i", (int) id);
            combo_element(is_selected, str_buffer, id);
        }

        ImGui::EndCombo();
    }

    return value_changed;
}

bool light_controls(Light& light) {
    bool value_changed = false;

    std::array<const char*, 3> light_type_names = {
        "Directional",
        "Point",
        "Spot"
    };

    auto it_selected = light_type_names.cbegin() + (size_t) light.type;
    if (combo_selector("Type", light_type_names, &it_selected, [](auto it) { return *it; })) {
        light.type = (LightType) std::distance(light_type_names.cbegin(), it_selected);
        value_changed = true;
    }

    if (light.type == LightType::Point) {
        ImGui::SetNextItemWidth(50);
        value_changed = ImGui::DragFloat("Constant", &light.constant, 0.02f) || value_changed;
        ImGui::SameLine();
        ImGui::SetNextItemWidth(50);
        value_changed = ImGui::DragFloat("Linear", &light.linear, 0.02f) || value_changed;
        ImGui::SameLine();
        ImGui::SetNextItemWidth(50);
        value_changed = ImGui::DragFloat("Quadratic", &light.quadratic, 0.02f) || value_changed;
    }
    else if (light.type == LightType::Spot) {
        ImGui::SetNextItemWidth(50);
        value_changed = ImGui::DragFloat("Cut Off", &light.cutoff, 1.0f) || value_changed;
        ImGui::SameLine();
        ImGui::SetNextItemWidth(50);
        value_changed = ImGui::DragFloat("Outer Cut Off", &light.outer_cutoff, 1.0f) || value_changed;
    }

    if (ImGui::BeginTable("LightTable", 2, ImGuiTableFlags_SizingFixedFit)) {
        ImGui::TableNextRow();
        ImGui::TableNextColumn();
        ImGui::SetNextItemWidth(150);
        ImGui::ColorEdit3("Diffuse", &light.diffuse.r);
        ImGui::TableNextColumn();
        ImGui::SetNextItemWidth(50);
        value_changed = ImGui::DragFloat("Intensity##Diffuse", &light.diffuse_intensity, 0.1f) || value_changed;

        ImGui::TableNextRow();
        ImGui::TableNextColumn();
        ImGui::SetNextItemWidth(150);
        ImGui::ColorEdit3("Specular", &light.specular.r);
        ImGui::TableNextColumn();
        ImGui::SetNextItemWidth(50);
        value_changed = ImGui::DragFloat("Intensity##Specular", &light.specular_intensity, 0.1f) || value_changed;

        ImGui::TableNextRow();
        ImGui::TableNextColumn();
        ImGui::SetNextItemWidth(150);
        ImGui::ColorEdit3("Ambient", &light.ambient.r);
        ImGui::TableNextColumn();
        ImGui::SetNextItemWidth(50);
        value_changed = ImGui::DragFloat("Intensity##Ambient", &light.ambient_intensity, 0.1f) || value_changed;

        ImGui::EndTable();
    }

    return value_changed;
}

bool rigidbody_2d_controls(Rigidbody2d& rigidbody_2d) {
    bool value_changed = false;

    constexpr std::array<const char*, 3> rigidbody_2d_types{
        "Static",
        "Kinematic",
        "Dynamic"
    };

    auto it_selected = rigidbody_2d_types.cbegin() + (size_t) rigidbody_2d.type;
    if (combo_selector("Type", rigidbody_2d_types, &it_selected, [](auto it) { return *it; })) {
        rigidbody_2d.type = (Rigidbody2dType) std::distance(rigidbody_2d_types.cbegin(), it_selected);
        value_changed = true;
    }

    value_changed = ImGui::Checkbox("Is Continuous", &rigidbody_2d.is_continuous) || value_changed;
    value_changed = ImGui::Checkbox("Auto Calculate Mass", &rigidbody_2d.auto_calculate_mass) || value_changed;
    if (rigidbody_2d.auto_calculate_mass)
        ImGui::BeginDisabled();
    value_changed = ImGui::DragFloat("Mass", &rigidbody_2d.mass, 0.2f, 0.0f) || value_changed;
    if (rigidbody_2d.auto_calculate_mass)
        ImGui::EndDisabled();
    value_changed = ImGui::DragFloat("Linear Damping", &rigidbody_2d.linear_damping, 0.01f, 0.0f, 1.0f) || value_changed;
    value_changed = ImGui::DragFloat("Angular Damping", &rigidbody_2d.angular_damping, 0.01f, 0.0f, 1.0f) || value_changed;
    value_changed = ImGui::Checkbox("Allow Sleep", &rigidbody_2d.allow_sleep) || value_changed;
    if (!rigidbody_2d.allow_sleep)
        ImGui::BeginDisabled();
    value_changed = ImGui::Checkbox("Start Awake", &rigidbody_2d.start_awake) || value_changed;
    if (!rigidbody_2d.allow_sleep)
        ImGui::EndDisabled();
    value_changed = ImGui::Checkbox("Fixed Rotation", &rigidbody_2d.fixed_rotation) || value_changed;
    value_changed = ImGui::DragFloat("Gravity Scale", &rigidbody_2d.gravity_scale, 0.02f) || value_changed;

    return value_changed;
}

bool physics_shape_2d_controls(PhysicsShape2d& physics_shape_2d) {
    bool value_changed = false;

    value_changed = ImGui::Checkbox("Is Sensor", &physics_shape_2d.is_sensor) || value_changed;
    value_changed = ImGui::DragFloat("Friction", &physics_shape_2d.friction, 0.05f, 0.0f) || value_changed;
    value_changed = ImGui::DragFloat("Restitution", &physics_shape_2d.restitution, 0.05f, 0.0f) || value_changed;
    value_changed = ImGui::DragFloat("Density", &physics_shape_2d.density, 0.1f, 0.0f) || value_changed;

    auto it_selected = physics_2d_settings.collision_layer_names.cbegin() + physics_shape_2d.collision_idx;
    if (combo_selector("Collision Layer", physics_2d_settings.collision_layer_names, &it_selected, [](auto it) { return it->c_str(); })) {
        physics_shape_2d.collision_idx = std::distance(physics_2d_settings.collision_layer_names.cbegin(), it_selected);
        value_changed = true;
    }

    return value_changed;
}

bool physics_shape_2d_box_controls(PhysicsShape2dBox& physics_shape_box) {
    bool value_changed = physics_shape_2d_controls(physics_shape_box);

    if (ImGui::BeginTable("PhysicsShape2dTable", 3)) {
        value_changed = TableRowXY("Size", "Size:\t", physics_shape_box.size, 0.05f) || value_changed;
        value_changed = TableRowXY("Center", "Center:\t", physics_shape_box.center, 0.05f) || value_changed;

        ImGui::EndTable();
    }

    if (ImGui::DragFloat("Rotation", &physics_shape_box.rotation_degrees, 1.0f)) {
        physics_shape_box.rotation_degrees = bound_degrees(physics_shape_box.rotation_degrees);
        value_changed = true;
    }

    return value_changed;
}

bool physics_shape_2d_circle_controls(PhysicsShape2dCircle& physics_shape_circle) {
    bool value_changed = physics_shape_2d_controls(physics_shape_circle);

    if (ImGui::BeginTable("PhysicsShape2dTable", 3)) {
        value_changed = TableRowXY("Center", "Center:\t", physics_shape_circle.center, 0.05f) || value_changed;

        ImGui::EndTable();
    }

    value_changed = ImGui::DragFloat("Radius", &physics_shape_circle.radius, 0.05f, 0.0f) || value_changed;

    return value_changed;
}

void Editor::entity_window(bool& entity_update, bool& transform_update, bool& mesh_updated, bool& light_updated) {
    entity_update = false;
    transform_update = false;
    mesh_updated = false;
    light_updated = false;
    bool rigidbody_2d_updated = false;
    bool physics_shape_2d_updated = false;

    const ImGuiWindowFlags window_flags = ImGuiWindowFlags_NoCollapse;
    ImGui::SetNextWindowSizeConstraints(ImVec2(350, 150), ImVec2(FLT_MAX, FLT_MAX));

    if (ImGui::Begin("Entity", &show_entity_window, window_flags)) {
        if (entities_selected.size() == 1) {
            auto [ selected_scene_id, selected_entity_id ] = entities_selected.front();

            auto [it_scene_lower, it_scene_upper] = equal_range(scenes, selected_scene_id);
            ASSERT(std::distance(it_scene_lower, it_scene_upper) == 1, "scene expected.");
            Scene& selected_scene = it_scene_lower->second;

            auto [it_entity_lower, it_entity_upper] = equal_range(selected_scene.entities, selected_entity_id);
            auto [it_transform_lower, it_transform_upper] = equal_range(selected_scene.transforms, selected_entity_id);
            auto [it_entity_mesh_lower, it_entity_mesh_upper] = equal_range(selected_scene.entity_meshes, selected_entity_id);
            auto [it_light_lower, it_light_upper] = equal_range(selected_scene.lights, selected_entity_id);
            auto [it_rigidbody_2d_lower, it_rigidbody_2d_upper] = equal_range(selected_scene.rigidbody_2ds, selected_entity_id);
            auto [it_physics_shape_2d_box_lower, it_physics_shape_2d_box_upper] = equal_range(selected_scene.physics_shape_2ds_box, selected_entity_id);
            auto [it_physics_shape_2d_circle_lower, it_physics_shape_2d_circle_upper] = equal_range(selected_scene.physics_shape_2ds_circle, selected_entity_id);

            if (ImGui::BeginPopupContextItem()) // <-- use last item id as popup id
            {
                auto add_component = [selected_entity_id](auto& components, auto& it_lower, auto& it_upper, auto&& component) {
                    size_t lower_idx = std::distance(components.begin(), it_lower);
                    it_upper = components.emplace(it_upper, selected_entity_id, std::move(component)) + 1;
                    it_lower = components.begin() + lower_idx;
                };

                if (it_entity_mesh_lower == it_entity_mesh_upper && ImGui::Button("+Mesh")) {
                    add_component(selected_scene.entity_meshes, it_entity_mesh_lower, it_entity_mesh_upper, 0);
                    mesh_updated = true;
                    ImGui::CloseCurrentPopup();
                }
                if (it_light_lower == it_light_upper && ImGui::Button("+Light")) {
                    add_component(selected_scene.lights, it_light_lower, it_light_upper, Light{});
                    light_updated = true;
                    ImGui::CloseCurrentPopup();
                }

                if (ImGui::BeginMenu("Physics 2d")) {
                    if (it_rigidbody_2d_lower == it_rigidbody_2d_upper && ImGui::Button("+Rigidbody 2D")) {
                        add_component(selected_scene.rigidbody_2ds, it_rigidbody_2d_lower, it_rigidbody_2d_upper, Rigidbody2d{});
                        rigidbody_2d_updated = true;
                        ImGui::CloseCurrentPopup();
                    }
                    if (ImGui::Button("+Shape - Box")) {
                        add_component(selected_scene.physics_shape_2ds_box, it_physics_shape_2d_box_lower, it_physics_shape_2d_box_upper, PhysicsShape2dBox{});
                        physics_shape_2d_updated = true;
                        ImGui::CloseCurrentPopup();
                    }
                    if (ImGui::Button("+Shape - Circle")) {
                        add_component(selected_scene.physics_shape_2ds_circle, it_physics_shape_2d_circle_lower, it_physics_shape_2d_circle_upper, PhysicsShape2dCircle{});
                        physics_shape_2d_updated = true;
                        ImGui::CloseCurrentPopup();
                    }

                    ImGui::EndMenu();
                }

                ImGui::EndPopup();
            }

            if (it_entity_lower != it_entity_upper && ImGui::CollapsingHeader("Entity", ImGuiTreeNodeFlags_DefaultOpen)) {
                if (ImGui::InputText("Name", &it_entity_lower->second.name)) {
                    if (it_entity_lower->second.name.empty())
                        it_entity_lower->second.name = "entity";
                    entity_update = true;
                }
                ImGui::SameLine();
                bool is_active = it_entity_lower->second.is_active;
                if (ImGui::Checkbox("Active", &is_active)) {
                    set_active(selected_scene_id, selected_entity_id, it_entity_lower->second, is_active);
                    entity_update = true;
                }
            }

            if (it_transform_lower != it_transform_upper && ImGui::CollapsingHeader("Transform", ImGuiTreeNodeFlags_DefaultOpen)) {
                transform_update = transform_controls(it_transform_lower->second);
            }

            auto display_components = [](const char* header_label, auto& components, auto it_component_lower, auto it_component_upper, auto controls_func) {
                bool value_changed = false;

                for (auto it_component = it_component_lower; it_component != it_component_upper;) {
                    ImGui::PushID((const void*) &*it_component);

                    bool was_removed = false;

                    auto context_remove = [&]() {
                        if (ImGui::BeginPopupContextItem()) {
                            if (ImGui::Button("-")) {
                                it_component = components.erase(it_component);
                                was_removed = true;
                                value_changed = true;
                                ImGui::CloseCurrentPopup();
                            }
                            ImGui::EndPopup();
                        }
                    };

                    if (ImGui::CollapsingHeader(header_label, ImGuiTreeNodeFlags_DefaultOpen)) {
                        context_remove();

                        if (!was_removed)
                            value_changed = controls_func(it_component->second) || value_changed;
                    }
                    else {
                        context_remove();
                    }

                    if (!was_removed)
                        ++it_component;

                    ImGui::PopID();
                }

                return value_changed;
            };

            mesh_updated = display_components("Mesh", selected_scene.entity_meshes, it_entity_mesh_lower, it_entity_mesh_upper, mesh_controls) || mesh_updated;
            light_updated = display_components("Light", selected_scene.lights, it_light_lower, it_light_upper, light_controls) || light_updated;
            rigidbody_2d_updated = display_components("Rigidbody 2D", selected_scene.rigidbody_2ds, it_rigidbody_2d_lower, it_rigidbody_2d_upper, rigidbody_2d_controls) || rigidbody_2d_updated;
            physics_shape_2d_updated = display_components("Shape 2D Box", selected_scene.physics_shape_2ds_box, it_physics_shape_2d_box_lower, it_physics_shape_2d_box_upper, physics_shape_2d_box_controls) || physics_shape_2d_updated;
            physics_shape_2d_updated = display_components("Shape 2D Circle", selected_scene.physics_shape_2ds_circle, it_physics_shape_2d_circle_lower, it_physics_shape_2d_circle_upper, physics_shape_2d_circle_controls) || physics_shape_2d_updated;
        }

        ImGui::End();
    }
}

void Editor::prefabs_window() {
    const ImGuiWindowFlags window_flags = ImGuiWindowFlags_NoCollapse;
    ImGui::SetNextWindowSizeConstraints(ImVec2(350, 150), ImVec2(FLT_MAX, FLT_MAX));

    if (ImGui::Begin("Prefabs", &show_prefab_window, window_flags)) {
        if (ImGui::BeginListBox("Prefabs")) {
            for (scene_id_t prefab_scene_id : prefabs) {
                auto [it_scene_lower, it_scene_upper] = equal_range(scenes, prefab_scene_id);
                ASSERT(std::distance(it_scene_lower, it_scene_upper) == 1, "scene expected.");
                Scene& prefab_scene = it_scene_lower->second;

                auto [it_entity_lower, it_entity_upper] = equal_range(prefab_scene.entities, prefab_scene.root_entities[0]);
                ASSERT(std::distance(it_entity_lower, it_entity_upper) == 1, "entity expected.");
                const Entity& prefab_entity = it_entity_lower->second;

                bool is_selected = false;
                if (ImGui::Selectable(prefab_entity.name.c_str(), is_selected)) {
                    // todo: implement ...
                }

                if (ImGui::BeginDragDropSource(ImGuiDragDropFlags_None)) {
                    ImGui::SetDragDropPayload(instantiate_prefab_drag_drop_name, &prefab_scene_id, sizeof(entity_id_t));
                    ImGui::Text("Instantiate %s", prefab_entity.name.c_str());
                    ImGui::EndDragDropSource();
                }

                if (is_selected)
                    ImGui::SetItemDefaultFocus();
            }
            ImGui::EndListBox();
        }

        ImGui::End();
    }
}

bool Editor::models_window() {
    const ImGuiWindowFlags window_flags = ImGuiWindowFlags_NoCollapse;
    ImGui::SetNextWindowSizeConstraints(ImVec2(350, 150), ImVec2(FLT_MAX, FLT_MAX));

    bool value_changed = false;

    if (ImGui::Begin("Models", &show_model_window, window_flags)) {
        if (ImGui::BeginListBox("Models")) {
            for (const auto& [ model_id, model ] : models) {
                auto [ it_selected_lower, it_selected_upper ] = equal_range(models_selected.begin(), models_selected.end(), model_id);
                bool is_selected = (it_selected_lower != it_selected_upper);
                if (ImGui::Selectable(model.path.c_str(), is_selected)) {
                    if (!ImGui::GetIO().KeyCtrl) {
                        models_selected.clear();
                        models_selected.emplace_back(model_id);
                    }
                    else {
                        if (is_selected) {
                            models_selected.erase(it_selected_lower);
                        }
                        else {
                            models_selected.emplace(std::lower_bound(models_selected.begin(), models_selected.end(), model_id), model_id);
                        }
                    }
                }

                if (is_selected)
                    ImGui::SetItemDefaultFocus();
            }
            ImGui::EndListBox();
        }

        if (ImGui::Button("Add")) {
            static constexpr FileFilterSpec model_file_types[] =
            {
                {L"Model files",                    L"*.obj;*.fbx;*.blend;*.gltf;*.glb"},
                {L"Wavefront",                      L"*.obj"},
                {L"Autodesk",                       L"*.fbx"},
                {L"Blender",                        L"*.blend"},
                {L"glTF",                           L"*.gltf;*.glb"},
                {L"All Documents",                  L"*.*"}
            };

            bool user_selected_file;
            std::vector<std::string> paths;
            if (file_open_dialog(paths, user_selected_file, model_file_types, std::size(model_file_types), true) && user_selected_file) {
                add_models(paths);
                value_changed = true;
            }
        }

        ImGui::SameLine();
        if (ImGui::Button("Remove")) {
            remove_models(models_selected);
            value_changed = value_changed || !models_selected.empty();
            models_selected.clear();
        }

        ImGui::End();
    }

    return value_changed;
}

// ensures that the sum of the bloom powers is always 1.0f.
void reallocate_bloom_power(size_t idx_changed, float previous_power) {
    std::vector<std::pair<float, size_t>> power_idx;
    power_idx.reserve(renderer.bloom_powers.size() - 1);
    auto j = 0;
    for (; j < idx_changed; ++j)
        power_idx.emplace_back(renderer.bloom_powers[j], j);
    for (++j; j < renderer.bloom_powers.size(); ++j)
        power_idx.emplace_back(renderer.bloom_powers[j], j);
    std::sort(power_idx.begin(), power_idx.end(), [](auto lhs, auto rhs) { return lhs.first < rhs.first; });

    float alloc = previous_power - renderer.bloom_powers[idx_changed];
    for (j = 0; j < power_idx.size(); ++j) {
        float to_alloc = alloc / (power_idx.size() - j);
        if (0.0f <= power_idx[j].first + to_alloc && power_idx[j].first + to_alloc <= 1.0f) {
            power_idx[j].first += to_alloc;
            alloc -= to_alloc;
        }
        else {
            alloc -= power_idx[j].first;
            power_idx[j].first = 0.0f;
        }
    }

    for (const auto& pair : power_idx)
        renderer.bloom_powers[pair.second] = pair.first;
}

void Editor::bloom_window() {
    const ImGuiWindowFlags window_flags = ImGuiWindowFlags_NoCollapse;
    //ImGui::SetNextWindowSizeConstraints(ImVec2(200, 100), ImVec2(FLT_MAX, FLT_MAX));
    if (ImGui::Begin("Bloom", &show_bloom_window, window_flags)) {
        const char* bloom_option_names[] = {
            "None",
            "Repeated Gaussian",
            "Multi-size Gaussian",
        };

        int combo_idx = (int) renderer.bloom_blur;
        if (ImGui::Combo("Type", &combo_idx, bloom_option_names, 3)) {
            renderer.bloom_blur = (BloomOption) combo_idx;
        }
        if (ImGui::IsItemHovered())
            ImGui::SetTooltip("Repeated Gaussian - a simple bloom approximation.\n"
                "Multi-size Gaussian - applies gaussian blurs at different scales. Typically produces better quality results as well as being faster.");

        ImGui::DragFloat("Intensity", &renderer.bloom_intensity_factor, 0.05f, 0.0f, std::numeric_limits<float>::max());
        if (ImGui::IsItemHovered())
            ImGui::SetTooltip("Scales the whole bloom effect.");
        if (ImGui::DragFloat("Brightness Cut-off", &renderer.bloom_brightness_cutoff, 0.1f, 0.0f, std::numeric_limits<float>::max()))
            renderer.bloom_brightness_full = std::max(renderer.bloom_brightness_full, renderer.bloom_brightness_cutoff);
        if (ImGui::IsItemHovered())
            ImGui::SetTooltip("Brightness levels above this value will be included in the bloom. Brightness will be interpolated if less than Brightness Full.");
        if (ImGui::DragFloat("Brightness Full", &renderer.bloom_brightness_full, 0.1f, 0.0f, std::numeric_limits<float>::max()))
            renderer.bloom_brightness_cutoff = std::min(renderer.bloom_brightness_cutoff, renderer.bloom_brightness_full);
        if (ImGui::IsItemHovered())
            ImGui::SetTooltip("Brightness levels above this value will be at full intensity in the bloom.");
        int new_num_bloom_powers = renderer.bloom_powers.size();
        if (ImGui::DragInt("Number of Blurs", &new_num_bloom_powers, 0.1f, 1, renderer.bloom_max_num_blurs)) {
            //if (new_num_bloom_powers < renderer.bloom_powers.size()) {
            //    for (size_t i = renderer.bloom_powers.size(); i > new_num_bloom_powers; --i) {
            //        float previous_power = renderer.bloom_powers[i - 1];
            //        renderer.bloom_powers[i - 1] = 0.0f;
            //        reallocate_bloom_power(i - 1, previous_power);
            //    }
            //}
            renderer.bloom_powers.resize(new_num_bloom_powers);
        }
        if (ImGui::IsItemHovered())
            ImGui::SetTooltip("The number of gaussian blurs applied. Each blur adds a performance penalty (lower values = faster).");

        ImGui::Separator();

        bool gaussian_changed = ImGui::DragFloat("Standard Deviation", &renderer.gaussian_blur_std_dev, 0.05f, 0.05f, 25.0f);
        if (ImGui::IsItemHovered())
            ImGui::SetTooltip("Larger standard deviations increase the weights of the lower weights relative to the median. Aim for the lowest weight to be close to zero.");
        int new_gaussian_blur_kernel_size = renderer.gaussian_blur_kernel_size;
        if (ImGui::DragInt("Kernel Size", &new_gaussian_blur_kernel_size, 0.1f, 3, 21)) {
            gaussian_changed = true;
            renderer.gaussian_blur_kernel_size = (new_gaussian_blur_kernel_size > renderer.gaussian_blur_kernel_size && (new_gaussian_blur_kernel_size % 2) == 0) ? new_gaussian_blur_kernel_size + 1
                : (new_gaussian_blur_kernel_size < renderer.gaussian_blur_kernel_size && (new_gaussian_blur_kernel_size % 2) == 0) ? new_gaussian_blur_kernel_size - 1
                : new_gaussian_blur_kernel_size;
        }
        if (ImGui::IsItemHovered())
            ImGui::SetTooltip("The number of weights in the gaussian distribution.");

        if (gaussian_changed)
            renderer.create_gaussian_blur_weights();

        ImGui::PlotHistogram("Gaussian Weights", renderer.gaussian_blur_weights.data(), renderer.gaussian_blur_weights.size(), 0, NULL, 0.0f, renderer.gaussian_blur_weights[renderer.gaussian_blur_kernel_size / 2], ImVec2(0, 80.0f));

        ImGui::Separator();

        if (renderer.bloom_blur != BloomOption::MultiSizeGaussian)
            ImGui::BeginDisabled();

        char str_buffer[16];
        size_t idx_power = 0;
        for (; idx_power < renderer.bloom_powers.size(); ++idx_power) {
            sprintf(str_buffer, "Power[%i]", (int) idx_power);
            ImGui::DragFloat(str_buffer, &renderer.bloom_powers[idx_power], 0.01f, 0.0f, 1.0f);
            if (ImGui::IsItemHovered())
                ImGui::SetTooltip("The intensity of each resolution blur, relative to all others.");
        }

        ImGui::BeginDisabled();
        for (; idx_power < renderer.bloom_max_num_blurs; ++idx_power) {
            sprintf(str_buffer, "Power[%i]", (int) idx_power);
            float zero = 0.0f;
            ImGui::DragFloat(str_buffer, &zero, 0.01f, 0.0f, 1.0f);
            if (ImGui::IsItemHovered())
                ImGui::SetTooltip("The intensity of each resolution blur, relative to all others.");
        }
        ImGui::EndDisabled();

        if (renderer.bloom_blur != BloomOption::MultiSizeGaussian)
            ImGui::EndDisabled();

        ImGui::End();
    }
}

void Editor::physics_2d_window() {
    const ImGuiWindowFlags window_flags = ImGuiWindowFlags_NoCollapse;
    //ImGui::SetNextWindowSizeConstraints(ImVec2(200, 100), ImVec2(FLT_MAX, FLT_MAX));
    if (ImGui::Begin("Physics 2D", &show_physics_2d_window, window_flags)) {
        ImGui::DragFloat2("Gravity", &physics_2d_settings.gravity[0], 0.1f);
        ImGui::DragInt("Velocity Iterations", &physics_2d_settings.velocity_iterations, 0.1f, 0);
        ImGui::DragInt("Position Iterations", &physics_2d_settings.position_iterations, 0.1f, 0);
        ImGui::DragInt("Steps Per Second", &physics_2d_settings.steps_per_second, 0.1f, 1);

        if (ImGui::CollapsingHeader("Collision Layers")) {
            if (ImGui::BeginTable("Collision Masks", physics_2d_settings.collision_masks.size() + 1, ImGuiTableFlags_NoSavedSettings | ImGuiTableFlags_RowBg | ImGuiTableFlags_Borders)) {
                //ImGui::TableSetupColumn("", ImGuiTableColumnFlags_NoHide);
                //for (const std::string& layer_name : physics_2d_settings.collision_layer_names) {
                //    ImGui::TableSetupColumn(layer_name.c_str(), ImGuiTableColumnFlags_NoHide);
                //}

                //ImGui::TableHeadersRow();

                ImGui::TableNextRow(ImGuiTableRowFlags_Headers);
                for (int column = 1; column < physics_2d_settings.collision_masks.size() + 1; column++) {
                    ImGui::TableSetColumnIndex(column);
                    std::string& layer_name = physics_2d_settings.collision_layer_names[column - 1];
                    ImGui::PushID(column);
                    ImGui::PushItemWidth(-1);
                    ImGui::InputText("##Name", &layer_name);
                    ImGui::PopItemWidth();
                    ImGui::PopID();
                }

                for (size_t from_category_idx = 0; from_category_idx < physics_2d_settings.collision_masks.size(); ++from_category_idx) {
                    ImGui::PushID(from_category_idx);
                    ImGui::TableNextRow();

                    ImGui::TableNextColumn();
                    const std::string& layer_name = physics_2d_settings.collision_layer_names[from_category_idx];
                    ImGui::Text(layer_name.c_str(), ImGuiTableColumnFlags_NoHide);

                    size_t to_category_idx = 0;
                    for (; to_category_idx < from_category_idx; ++to_category_idx) {
                        ImGui::TableNextColumn();
                    }

                    for (; to_category_idx < physics_2d_settings.collision_masks.size(); ++to_category_idx) {
                        ImGui::TableNextColumn();
                        ImGui::PushID(to_category_idx);
                        bool flag = physics_2d_settings.collision_masks[from_category_idx] & (1 << to_category_idx);
                        if (ImGui::Checkbox("##checkbox", &flag)) {
                            physics_2d_settings.collision_masks[from_category_idx] ^= (1 << to_category_idx);
                        }
                        ImGui::PopID();
                    }
                    ImGui::PopID();
                }

                ImGui::EndTable();
            }
        }

        if (ImGui::CollapsingHeader("Debug")) {
            ImGui::Checkbox("Debug Draw", &physics_2d_settings.debug_draw);
            ImGui::Checkbox("Draw Shapes", &physics_2d_settings.debug_draw_shapes);
            ImGui::Checkbox("Draw Joins", &physics_2d_settings.debug_draw_joints);
            ImGui::Checkbox("Draw AABB", &physics_2d_settings.debug_draw_aabb);
            ImGui::Checkbox("Draw Center of Mass", &physics_2d_settings.debug_draw_center_of_mass);
        }

        if (physics_2d_settings.playing && ImGui::Button("Pause"))
            physics_2d_settings.playing = false;
        else if (!physics_2d_settings.playing && ImGui::Button("Play"))
            physics_2d_settings.playing = true;
        ImGui::SameLine();
        if (ImGui::Button("Advance"))
            physics_2d_settings.advance = true;
        ImGui::SameLine();
        physics_2d_settings.reset = ImGui::Button("Reset");

        ImGui::End();
    }
}

Editor::Editor() {
    FATAL_ASSERT(native_ui_startup(), "native UI failed to initialize.")
}
