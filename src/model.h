#pragma once

#include <glad/glad.h>
#include <glm/glm.hpp>

#include <vector>
#include <string>

#include "texture.h"
#include "entity.h"

class Shader;

typedef size_t mesh_id_t;

mesh_id_t new_mesh_id();

typedef size_t model_id_t;

model_id_t new_model_id();

//static constexpr size_t max_bone_influence = 4;

struct Vertex {
    glm::vec3 Position;
    glm::vec3 Normal;
    glm::vec2 TexCoords;
    glm::vec3 Tangent;
    //glm::vec3 Bitangent;
    // int BoneIds[max_bone_influence];
    // float Weights[max_bone_influence];

    Vertex() = default;

    inline Vertex(glm::vec3 p, glm::vec3 n, glm::vec2 tc, glm::vec3 t = glm::vec3{}) noexcept
        : Position(p), Normal(n), TexCoords(tc), Tangent(t) {
    }
};

struct MeshTexture {
    tex_id_t tex_id;
    GLuint gl_id; // no ownership
    TextureType type;

    /* implicit */ inline MeshTexture(const Texture& value) noexcept
        : gl_id(value.gl_id),
        type(value.type),
        tex_id(value.tex_id) {
    }

    MeshTexture(MeshTexture&& rhs) = default;
    MeshTexture(const MeshTexture&) = default;
    MeshTexture& operator =(MeshTexture&& rhs) = default;
    MeshTexture& operator =(const MeshTexture&) = default;
};

class Mesh {
public:
    std::vector<Vertex>       vertices;
    std::vector<unsigned int> indices;
    std::vector<MeshTexture>  textures; // todo: change to an array of one per TextureType?

    GLuint VAO, VBO, EBO; // has ownership

    void setup(); // call after vertices and indices are setup

    Mesh() = default;
    Mesh(Mesh&& rhs) noexcept;
    Mesh(const Mesh&) = delete;
    Mesh& operator =(Mesh&& rhs) noexcept;
    Mesh& operator =(const Mesh&) = delete;
    ~Mesh();
};

class Model {
public:
    std::string path;
    std::vector<tex_id_t> owned_textures;
    std::vector<mesh_id_t> owned_meshes;
    scene_id_t scene_id;

    Model() = default;
    Model(Model&& rhs) = default;
    Model(const Model&) = delete;
    Model& operator =(Model&& rhs) = default;
    Model& operator =(const Model&) = delete;
};

void gl_draw(const Mesh& mesh, Shader& shader, size_t instance_count);

void setup_uniform_attributes(GLuint instance_buffer, const Mesh& mesh);
