#pragma once

#include <array>
#include <string>

#include <glad/glad.h>

typedef size_t tex_id_t;

tex_id_t new_texture_id();

enum class TextureType {
    Diffuse,
    Specular,
    Normal,
    Height
};

struct Texture {
    bool is_loaded;
    tex_id_t tex_id;
    GLuint gl_id; // has ownership, if is_loaded
    TextureType type;
    std::string path;

    inline Texture(tex_id_t id, bool l, GLuint i, TextureType t, std::string&& p) noexcept
        : tex_id(id), is_loaded(l), gl_id(i), type(t), path(std::move(p)) {
    }
};

struct Cubemap {
    tex_id_t tex_id;
    GLuint gl_id; // has ownership
    std::array<std::string, 6> face_paths;
    std::array<bool, 6> is_loaded; // currently only used for validation.

    std::string& right_face_path() { return face_paths[0]; }
    std::string& left_face_path() { return face_paths[1]; }
    std::string& top_face_path() { return face_paths[2]; }
    std::string& bottom_face_path() { return face_paths[3]; }
    std::string& front_face_path() { return face_paths[4]; }
    std::string& back_face_path() { return face_paths[5]; }
};
