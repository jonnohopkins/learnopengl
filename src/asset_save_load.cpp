#include "asset_save_load.h"

#include "assets.h"
#include "json_serialization.h"

#include <rapidjson/reader.h>
#include <rapidjson/filereadstream.h>
#include <rapidjson/filewritestream.h>
#include <rapidjson/prettywriter.h>
#include <rapidjson/error/en.h>

#include <iostream>

template<>
struct serialization<glm::vec2> {
    inline static constexpr auto member_data = std::make_tuple(
        std::make_pair("x", &glm::vec2::x),
        std::make_pair("y", &glm::vec2::y)
    );
};

template<>
struct serialization<glm::vec3> {
    inline static constexpr auto member_data = std::make_tuple(
        std::make_pair("x", &glm::vec3::x),
        std::make_pair("y", &glm::vec3::y),
        std::make_pair("z", &glm::vec3::z)
    );
};

template<>
struct serialization<glm::vec4> {
    inline static constexpr auto member_data = std::make_tuple(
        std::make_pair("x", &glm::vec4::x),
        std::make_pair("y", &glm::vec4::y),
        std::make_pair("z", &glm::vec4::z),
        std::make_pair("w", &glm::vec4::w)
    );
};

template<>
struct serialization<glm::quat> {
    inline static constexpr auto member_data = std::make_tuple(
        std::make_pair("x", &glm::quat::x),
        std::make_pair("y", &glm::quat::y),
        std::make_pair("z", &glm::quat::z),
        std::make_pair("w", &glm::quat::w)
    );
};

template<>
struct serialization<Entity> {
    inline static constexpr auto member_data = std::make_tuple(
        std::make_pair("is_active", &Entity::is_active),
        std::make_pair("name", &Entity::name),
        std::make_pair("parent", &Entity::parent),
        std::make_pair("children", &Entity::children)
    );
};

template<>
struct serialization<Transform> {
    inline static constexpr auto member_data = std::make_tuple(
        std::make_pair("Position", &Transform::Position),
        std::make_pair("EulerAngles", &Transform::EulerAngles),
        std::make_pair("Scale", &Transform::Scale)
    );
};

struct TransformDeserializer : public ObjectDeserializer<Transform> {
    explicit TransformDeserializer(target_type* target) noexcept
        : ObjectDeserializer<Transform>(target) {
    }

    bool complete() override {
        target->UpdateRotation();
        return true;
    }
};

template<>
struct DeserializerForObject<Transform> {
    using type = TransformDeserializer;
};

template<>
struct serialization<Light> {
    inline static constexpr auto member_data = std::make_tuple(
        std::make_pair("type", &Light::type),
        std::make_pair("constant", &Light::constant),
        std::make_pair("linear", &Light::linear),
        std::make_pair("quadratic", &Light::quadratic),
        std::make_pair("cutoff", &Light::cutoff),
        std::make_pair("outer_cutoff", &Light::outer_cutoff),
        std::make_pair("ambient", &Light::ambient),
        std::make_pair("ambient_intensity", &Light::ambient_intensity),
        std::make_pair("diffuse", &Light::diffuse),
        std::make_pair("diffuse_intensity", &Light::diffuse_intensity),
        std::make_pair("specular", &Light::specular),
        std::make_pair("specular_intensity", &Light::specular_intensity)
    );
};

template<>
struct serialization<Rigidbody2d> {
    inline static constexpr auto member_data = std::make_tuple(
        std::make_pair("type", &Rigidbody2d::type),
        std::make_pair("is_continuous", &Rigidbody2d::is_continuous),
        std::make_pair("auto_calculate_mass", &Rigidbody2d::auto_calculate_mass),
        std::make_pair("mass", &Rigidbody2d::mass),
        std::make_pair("linear_damping", &Rigidbody2d::linear_damping),
        std::make_pair("angular_damping", &Rigidbody2d::angular_damping),
        std::make_pair("allow_sleep", &Rigidbody2d::allow_sleep),
        std::make_pair("start_awake", &Rigidbody2d::start_awake),
        std::make_pair("fixed_rotation", &Rigidbody2d::fixed_rotation),
        std::make_pair("gravity_scale", &Rigidbody2d::gravity_scale)
    );
};

template<>
struct serialization<PhysicsShape2dBox> {
    inline static constexpr auto member_data = std::make_tuple(
        std::make_pair("collision_idx", &PhysicsShape2d::collision_idx),
        std::make_pair("is_sensor", &PhysicsShape2d::is_sensor),
        std::make_pair("friction", &PhysicsShape2d::friction),
        std::make_pair("restitution", &PhysicsShape2d::restitution),
        std::make_pair("density", &PhysicsShape2d::density),
        std::make_pair("center", &PhysicsShape2dBox::center),
        std::make_pair("size", &PhysicsShape2dBox::size),
        std::make_pair("rotation_degrees", &PhysicsShape2dBox::rotation_degrees)
    );
};

template<>
struct serialization<PhysicsShape2dCircle> {
    inline static constexpr auto member_data = std::make_tuple(
        std::make_pair("collision_idx", &PhysicsShape2d::collision_idx),
        std::make_pair("is_sensor", &PhysicsShape2d::is_sensor),
        std::make_pair("friction", &PhysicsShape2d::friction),
        std::make_pair("restitution", &PhysicsShape2d::restitution),
        std::make_pair("density", &PhysicsShape2d::density),
        std::make_pair("center", &PhysicsShape2dCircle::center),
        std::make_pair("radius", &PhysicsShape2dCircle::radius)
    );
};

template<>
struct serialization<Scene> {
    inline static constexpr auto member_data = std::make_tuple(
        std::make_pair("entities", &Scene::entities),
        std::make_pair("root_entities", &Scene::root_entities),
        std::make_pair("transforms", &Scene::transforms),
        std::make_pair("entity_meshes", &Scene::entity_meshes),
        std::make_pair("lights", &Scene::lights),
        std::make_pair("rigidbody_2ds", &Scene::rigidbody_2ds),
        std::make_pair("physics_shape_2ds_box", &Scene::physics_shape_2ds_box),
        std::make_pair("physics_shape_2ds_circle", &Scene::physics_shape_2ds_circle)
    );
};

struct SceneDeserializer : public ObjectDeserializer<Scene> {
    explicit SceneDeserializer(target_type* target) noexcept
        : ObjectDeserializer<Scene>(target) {
    }

    bool complete() override {
        std::sort(target->entities.begin(), target->entities.end(), [](const std::pair<entity_id_t, Entity>& lhs, const std::pair<entity_id_t, Entity>& rhs) { return lhs.first < rhs.first; });
        if (target->entities.empty())
            target->last_entity_id = 0;
        else
            target->last_entity_id = target->entities.back().first;
        return true;
    }
};

template<>
struct DeserializerForObject<Scene> {
    using type = SceneDeserializer;
};

void load_scene_file(const char* path, bool* value_changed) {
    FILE* fp = fopen(path, "rb"); // non-Windows use "r"

    if (fp) {
        Scene scene;

        rapidjson::Reader reader;
        char read_buffer[1 << 16];
        rapidjson::FileReadStream is(fp, read_buffer, sizeof(read_buffer));
        Deserializer deserialize_handler{ Deserializer::deserializer_type::neither, std::make_unique<ExpectDeserializer<Scene>>(&scene) };

        if (!reader.Parse(is, deserialize_handler)) {
            rapidjson::ParseErrorCode error = reader.GetParseErrorCode();
            size_t offset = reader.GetErrorOffset();
            size_t line_num = 1, char_num = 1, last_line_char = 0;
            for (size_t i = 0; i < offset; ++i) {
                if (read_buffer[i] == '\n') {
                    ++line_num;
                    char_num = 1;
                    last_line_char = i + 1;
                }
                else {
                    ++char_num;
                }
            }

            size_t next_line_char = offset;
            while (next_line_char < sizeof(read_buffer) && read_buffer[next_line_char] != '\n' && read_buffer[next_line_char] != '\0')
                ++next_line_char;

            std::cerr << "Error parsing at line " << line_num << ", column " << char_num << ":\n" << std::string_view(read_buffer + last_line_char, next_line_char - last_line_char) << std::endl;
            std::cerr << "RapidJSON error: " << rapidjson::GetParseError_En(error) << std::endl;
        }
        else {
            scenes.emplace_back(new_scene_id(), std::move(scene));
            active_scenes.push_back(scenes.back().first);

            *value_changed = true;
        }

        fclose(fp);
    }
    else {
        std::cerr << "could not open file for reading: " << path << std::endl;
    }
}

void save_scene_file(const char* path, const Scene& scene) {
    FILE* fp = fopen(path, "wb"); // non-Windows use "r"

    if (fp) {
        char write_buffer[1 << 16];
        rapidjson::FileWriteStream os(fp, write_buffer, sizeof(write_buffer));
        rapidjson::PrettyWriter<rapidjson::FileWriteStream> writer{ os };
        serialize(writer, scene);

        fclose(fp);
    }
    else {
        std::cerr << "Could not open file for writing: " << path << std::endl;
    }
}