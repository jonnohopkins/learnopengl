#include <iostream>
#include <utility>
#include <filesystem>
#include <thread>
#include <numeric>
#include <iostream>

#include <glad/glad.h>
#define GLEW_STATIC
#include <GLFW/glfw3.h>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include <imgui.h>
#include <imgui_impl_glfw.h>
#include <imgui_impl_opengl3.h>
#include <misc/cpp/imgui_stdlib.h>

#define STB_IMAGE_IMPLEMENTATION
#include <stb_image.h>

#include "globals.h"
#include "shader.h"
#include "camera.h"
#include "model.h"
#include "entity.h"
#include "editor.h"
#include "embedded_scenes.h"
#include "renderer.h"
#include "assets.h"
#include "simple_log_timer.h"

#include <native_ui.h>

const std::string font_filename = monospace_font_filename();

ImGuiIO* io = nullptr;

bool first_mouse = true;
float last_x = renderer.width / 2.0;
float last_y = renderer.height / 2.0;

bool is_camera_fast = false;

float content_scale = 1.0f;

void framebuffer_size_callback(GLFWwindow* window, int width, int height) {
    renderer.resize(width, height);
    ImGuizmo::SetRect(0, 0, renderer.width, renderer.height);
}

// after adding fonts the font textures need to be rebuilt.
// on startup this will be done automatically by imgui.
// when the window content scale is changed this is done manually.
void add_content_scaled_fonts() {
    float size_pixels = std::floor(13.0f * content_scale);
    if (!io->Fonts->AddFontFromFileTTF(font_filename.c_str(), size_pixels)) {
        // fallback to default font.
        io->Fonts->AddFontDefault();
    }
}

void window_content_scale_callback(GLFWwindow* window, float xscale, float yscale) {
    if (content_scale != xscale) {
        content_scale = xscale;

        io->Fonts->Clear();

        add_content_scaled_fonts();

        ImGui_ImplOpenGL3_DestroyFontsTexture();
        ImGui_ImplOpenGL3_CreateFontsTexture();
    }
}

void set_input_mode(GLFWwindow* window) {
    glfwSetInputMode(window, GLFW_CURSOR, editor.interacting_with_ui ? GLFW_CURSOR_NORMAL : GLFW_CURSOR_DISABLED);
}

void process_input(GLFWwindow* window) {
    if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
        glfwSetWindowShouldClose(window, true);

    static int key_d_last_press = GLFW_RELEASE;
    int key_d_press = glfwGetKey(window, GLFW_KEY_D);
    if ((glfwGetKey(window, GLFW_KEY_LEFT_ALT) == GLFW_PRESS || glfwGetKey(window, GLFW_KEY_RIGHT_ALT) == GLFW_PRESS) && key_d_press == GLFW_PRESS && key_d_last_press == GLFW_RELEASE) {
        editor.interacting_with_ui = !editor.interacting_with_ui;
        set_input_mode(window);
    }
    key_d_last_press = key_d_press;

    if (!editor.interacting_with_ui) {
        glm::vec3 direction = glm::vec3(0.0f, 0.0f, 0.0f);

        if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS)
            direction.z = -1.0f;
        if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS)
            direction.z = 1.0f;
        if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS)
            direction.x = -1.0f;
        if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS)
            direction.x = 1.0f;
        if (glfwGetKey(window, GLFW_KEY_LEFT_SHIFT) == GLFW_PRESS)
            direction.y = -1.0f;
        if (glfwGetKey(window, GLFW_KEY_SPACE) == GLFW_PRESS)
            direction.y = 1.0f;

        if (direction != glm::vec3{})
            direction = glm::normalize(direction);

        static int key_left_control_last_press = GLFW_RELEASE;
        int key_left_control_press = glfwGetKey(window, GLFW_KEY_LEFT_CONTROL);
        if (key_left_control_press == GLFW_PRESS && key_left_control_last_press == GLFW_RELEASE) {
            is_camera_fast = !is_camera_fast;
        }
        key_left_control_last_press = key_left_control_press;

        if (is_camera_fast) {
            direction.y *= 3.0f;
            direction.x *= 6.0f;
            direction.z *= 6.0f;
        }

        renderer.camera.ProcessKeyboard(direction, renderer.delta_time);
    }
}

void mouse_callback(GLFWwindow* window, double x_pos, double y_pos) {
    if (first_mouse) {
        last_x = x_pos;
        last_y = y_pos;
        first_mouse = false;
    }

    float xoffset = x_pos - last_x;
    float yoffset = last_y - y_pos; // reversed since y-coordinates go from bottom to top
    last_x = x_pos;
    last_y = y_pos;

    if (!editor.interacting_with_ui)
        renderer.camera.ProcessMouseMovement(xoffset, yoffset);
}

void glfw_error_callback(int error, const char* description) {
    fprintf(stderr, "GLFW error %d: %s\n", error, description);
}

#ifdef DEBUG
void GLAPIENTRY gl_debug_message(GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei length, const GLchar* message, const void* user_param) {
    fprintf(stderr, "GL error: source = %s, type = **%s**, severity = [%s], message = %s\n",
        (source == GL_DEBUG_SOURCE_API ? "API"
            : source == GL_DEBUG_SOURCE_WINDOW_SYSTEM ? "WINDOW_SYSTEM"
            : source == GL_DEBUG_SOURCE_SHADER_COMPILER ? "SHADER_COMPILER"
            : source == GL_DEBUG_SOURCE_THIRD_PARTY ? "THIRD_PARTY"
            : source == GL_DEBUG_SOURCE_APPLICATION ? "APPLICATION"
            : source == GL_DEBUG_SOURCE_OTHER ? "OTHER"
            : "UNKNOWN"),
        (type == GL_DEBUG_TYPE_ERROR ? "ERROR"
            : type == GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR ? "DEPRECATED"
            : type == GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR ? "UNDEFINED"
            : type == GL_DEBUG_TYPE_PORTABILITY ? "PORTABILITY"
            : type == GL_DEBUG_TYPE_PERFORMANCE ? "PERFORMANCE"
            : type == GL_DEBUG_TYPE_MARKER ? "MARKER"
            : type == GL_DEBUG_TYPE_PUSH_GROUP ? "PUSH_GROUP"
            : type == GL_DEBUG_TYPE_POP_GROUP ? "POP_GROUP"
            : type == GL_DEBUG_TYPE_OTHER ? "OTHER"
            : "UNKNOWN"),
        (severity == GL_DEBUG_SEVERITY_HIGH ? "HIGH"
            : severity == GL_DEBUG_SEVERITY_MEDIUM ? "MEDIUM"
            : severity == GL_DEBUG_SEVERITY_LOW ? "LOW"
            : severity == GL_DEBUG_SEVERITY_NOTIFICATION ? "NOTIFICATION"
            : "UNKNOWN"),
        message);

    if(type != GL_DEBUG_TYPE_PERFORMANCE)
        BREAKPOINT;
}
#endif

int main() {
    simple_log_timer app_startup_timer{ "app startup" };

    glfwSetErrorCallback(glfw_error_callback);
    FATAL_ASSERT(glfwInit(), "glfw failed to initialize.")
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
    glfwWindowHint(GLFW_SAMPLES, renderer.msaa_samples);
#ifdef DEBUG
    glfwWindowHint(GLFW_OPENGL_DEBUG_CONTEXT, true);
#endif
    glfwWindowHint(GLFW_MAXIMIZED, GLFW_TRUE);

    GLFWmonitor* primary_monitor = glfwGetPrimaryMonitor();

    int work_xpos, work_ypos, work_width, work_height;
    glfwGetMonitorWorkarea(primary_monitor, &work_xpos, &work_ypos, &work_width, &work_height);

    GLFWwindow* window = glfwCreateWindow(work_width, work_height, "LearnOpenGL", NULL, NULL);
    if (window == NULL) {
        std::cerr << "Failed to create GLFW window" << std::endl;
        glfwTerminate();
        return -1;
    }

    glfwMakeContextCurrent(window);

    // the actual window content size will be a little smaller than the monitor's work size, so we need to query it.
    int window_width, window_height;
    glfwGetWindowSize(window, &window_width, &window_height);

    renderer.width = window_width;
    renderer.height = window_height;
    ImGuizmo::SetRect(0, 0, renderer.width, renderer.height);

    if (!gladLoadGLLoader((GLADloadproc) glfwGetProcAddress)) {
        std::cerr << "Failed to initialize GLAD" << std::endl;
        return -1;
    }

#ifdef DEBUG
    int context_flags;
    glGetIntegerv(GL_CONTEXT_FLAGS, &context_flags);
    if (context_flags & GL_CONTEXT_FLAG_DEBUG_BIT) {
        glEnable(GL_DEBUG_OUTPUT);
        glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS);
        glDebugMessageControl(GL_DONT_CARE, GL_DONT_CARE, GL_DEBUG_SEVERITY_NOTIFICATION, 0, nullptr, GL_FALSE);
        glDebugMessageCallback(gl_debug_message, nullptr);
    }
    else {
        ASSERT(false, "debugging not available.")
    }
#endif

    GLint max_vertex_attributes;
    glGetIntegerv(GL_MAX_VERTEX_ATTRIBS, &max_vertex_attributes);
    if (max_vertex_attributes < 16) {
        std::cerr << "max_vertex_attributes must be >= 16." << std::endl;
        return -1;
    }

    set_input_mode(window);

    // Setup Dear ImGui context
    IMGUI_CHECKVERSION();
    ImGui::CreateContext();
    io = &ImGui::GetIO();
    //io->ConfigFlags |= ImGuiConfigFlags_NavEnableKeyboard;     // Enable Keyboard Controls
    //io->ConfigFlags |= ImGuiConfigFlags_NavEnableGamepad;      // Enable Gamepad Controls

    float window_xscale, window_yscale;
    glfwGetWindowContentScale(window, &window_xscale, &window_yscale);
    content_scale = window_xscale;
    add_content_scaled_fonts();

    // Setup Dear ImGui style
    ImGui::StyleColorsDark();
    //ImGui::StyleColorsClassic();

    // Setup Platform/Renderer backends
    ImGui_ImplGlfw_InitForOpenGL(window, true);
    ImGui_ImplOpenGL3_Init("#version 330 core");

    renderer.init();

    simple_log_timer model_loading_timer{ "model loading" };
    load_models();
    model_loading_timer.end();

    init_assets();

    active_scenes.push_back(create_model_display_scene().first);
    //active_scenes.push_back(create_hdr_test_scene().first);

    renderer.post_scene_setup();

    simple_log_timer physics_startup_timer{ "physics startup" };
    init_physics();
    physics_startup_timer.end();

    glfwSwapInterval((int) renderer.vsync_enabled);
    glfwSetFramebufferSizeCallback(window, framebuffer_size_callback);
    glfwSetCursorPosCallback(window, mouse_callback);
    glfwSetWindowContentScaleCallback(window, window_content_scale_callback);

    app_startup_timer.end();
    
    while (!glfwWindowShouldClose(window)) {
        // if window is minimized, skip process_input + rendering.
        if (renderer.width == 0 || renderer.height == 0) {
            glfwPollEvents();
            continue;
        }

        process_input(window);

        // imgui rendering:
        ImGui_ImplOpenGL3_NewFrame();
        ImGui_ImplGlfw_NewFrame();
        ImGui::NewFrame();

        // ImGuizmo::SetOrthographic(false);
        ImGuizmo::BeginFrame();

        update_physics();

        renderer.render_frame();

        bool mesh_buffer_requires_update, light_shadow_info_requires_update;
        editor.display(mesh_buffer_requires_update, light_shadow_info_requires_update);

        renderer.update(mesh_buffer_requires_update, light_shadow_info_requires_update);

        set_input_mode(window);

        if (renderer.use_game_view) {
            glBindFramebuffer(GL_FRAMEBUFFER, 0);
            glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

            const ImGuiWindowFlags window_flags = ImGuiWindowFlags_None;
            if (ImGui::Begin("Game View", &renderer.use_game_view, window_flags)) {
                glm::vec2 content_min = ImGui::GetWindowContentRegionMin();
                glm::vec2 content_max = ImGui::GetWindowContentRegionMax();

                glm::vec2 size = content_max - content_min;

                ImGui::Image((ImTextureID) (intptr_t) renderer.game_view_color_texture(), ImVec2(size), ImVec2(0, 1), ImVec2(1, 0));

                ImGui::End();
            }
        }

        ImGui::Render();

        ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());

        // glfw: swap buffers and poll IO events (keys pressed/released, mouse moved etc.)
        // -------------------------------------------------------------------------------
        glfwSwapBuffers(window);

        // Poll and handle events (inputs, window resize, etc.)
        // You can read the io->WantCaptureMouse, io->WantCaptureKeyboard flags to tell if dear imgui wants to use your inputs.
        // - When io->WantCaptureMouse is true, do not dispatch mouse input data to your main application.
        // - When io->WantCaptureKeyboard is true, do not dispatch keyboard input data to your main application.
        // Generally you may always pass all inputs to dear imgui, and hide them from your application based on those two flags.
        glfwPollEvents();

        update_textures_if_pending();
    }

    terminate_physics();

    terminate_assets();

    renderer.terminate();

    ImGui_ImplOpenGL3_Shutdown();
    ImGui_ImplGlfw_Shutdown();
    ImGui::DestroyContext();

    glfwTerminate();

	return 0;
}
