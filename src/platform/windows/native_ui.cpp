#include "native_ui.h"

#include <final_action.h>

#include <windows.h>      // For common windows data types and function headers
#define STRICT_TYPED_ITEMIDS
#include <shlobj.h>
#include <objbase.h>      // For COM headers
#include <shobjidl.h>     // for IFileDialogEvents and IFileDialogControlEvents
#include <shlwapi.h>
#include <knownfolders.h> // for KnownFolder APIs/datatypes/function headers
#include <propvarutil.h>  // for PROPVAR-related functions
#include <propkey.h>      // for the Property key APIs/datatypes
#include <propidl.h>      // for the Property System APIs
#include <strsafe.h>      // for StringCchPrintfW
#include <shtypes.h>      // for COMDLG_FILTERSPEC
#include <new>
#include <locale>
#include <codecvt>
#include <iostream>

#define REQUIRE_SUCCESS(expr) if (!SUCCEEDED(expr)) return false

#pragma comment(linker, "\"/manifestdependency:type='Win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='*' publicKeyToken='6595b64144ccf1df' language='*'\"")

bool native_ui_startup() {
    HRESULT hr = CoInitializeEx(NULL, COINIT_APARTMENTTHREADED | COINIT_DISABLE_OLE1DDE);
    return SUCCEEDED(hr);
}

std::string wstring_to_string(const std::wstring& wstr) {
    return std::wstring_convert<std::codecvt_utf8<wchar_t>, wchar_t>{}.to_bytes(wstr);
}

bool file_open_dialog(std::vector<std::string>& paths, bool& user_selected_file, const FileFilterSpec* filter_spec, size_t filter_spec_size, bool multi_select) {
    user_selected_file = false;

    IFileOpenDialog* pfd = NULL;
    REQUIRE_SUCCESS(CoCreateInstance(CLSID_FileOpenDialog, NULL, CLSCTX_INPROC_SERVER, IID_PPV_ARGS(&pfd)));

    auto pfd_final = xtd::finally([pfd]() { pfd->Release(); });

    DWORD dwOptions;
    REQUIRE_SUCCESS(pfd->GetOptions(&dwOptions));
    REQUIRE_SUCCESS(pfd->SetOptions(dwOptions | (multi_select * FOS_ALLOWMULTISELECT)));

    REQUIRE_SUCCESS(pfd->SetFileTypes(filter_spec_size, reinterpret_cast<const COMDLG_FILTERSPEC*>(filter_spec)));
    REQUIRE_SUCCESS(pfd->SetFileTypeIndex(1));

    if (!SUCCEEDED(pfd->Show(NULL)))
        return true;

    IShellItemArray* psiResults;
    REQUIRE_SUCCESS(pfd->GetResults(&psiResults));

    user_selected_file = true;

    auto psiResult_final = xtd::finally([psiResults]() { psiResults->Release(); });

    DWORD count;
    REQUIRE_SUCCESS(psiResults->GetCount(&count));

    IShellItem* psi;
    for (DWORD i = 0; i < count; ++i) {
        REQUIRE_SUCCESS(psiResults->GetItemAt(i, &psi));

        auto psi_final = xtd::finally([psi]() { psi->Release(); });

        PWSTR pszFilePath = NULL;
        REQUIRE_SUCCESS(psi->GetDisplayName(SIGDN_FILESYSPATH, &pszFilePath));

        auto converted_path = wstring_to_string(pszFilePath);
        paths.push_back(std::move(converted_path));
        CoTaskMemFree(pszFilePath);
    }

    return true;
}

bool file_save_dialog(std::string& path, bool& user_saved_file, const FileFilterSpec* filter_spec, size_t filter_spec_size, const wchar_t* default_extension) {
    user_saved_file = false;

    IFileSaveDialog* pfd = NULL;
    REQUIRE_SUCCESS(CoCreateInstance(CLSID_FileSaveDialog, NULL, CLSCTX_INPROC_SERVER, IID_PPV_ARGS(&pfd)));

    auto pfd_final = xtd::finally([pfd]() { pfd->Release(); });

    DWORD dwOptions;
    REQUIRE_SUCCESS(pfd->GetOptions(&dwOptions));
    REQUIRE_SUCCESS(pfd->SetOptions(dwOptions));

    REQUIRE_SUCCESS(pfd->SetFileTypes(filter_spec_size, reinterpret_cast<const COMDLG_FILTERSPEC*>(filter_spec)));
    REQUIRE_SUCCESS(pfd->SetFileTypeIndex(1));

    REQUIRE_SUCCESS(pfd->SetDefaultExtension(default_extension));

    if (!SUCCEEDED(pfd->Show(NULL)))
        return true;

    user_saved_file = true;

    IShellItem* psiResult;
    REQUIRE_SUCCESS(pfd->GetResult(&psiResult));
    auto psiResult_final = xtd::finally([psiResult]() { psiResult->Release(); });

    PWSTR pszFilePath;
    REQUIRE_SUCCESS(psiResult->GetDisplayName(SIGDN_FILESYSPATH, &pszFilePath));

    path = wstring_to_string(pszFilePath);
    CoTaskMemFree(pszFilePath);

    return true;
}

std::string monospace_font_filename() {
    std::string filename(64, (char) 0);
    DWORD size = GetEnvironmentVariable("WINDIR", filename.data(), filename.size());
    filename.resize(size);
    filename += "\\Fonts\\consola.ttf";
    return filename;
}

#undef REQUIRE_SUCCESS
