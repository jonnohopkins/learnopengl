#pragma once

#include <vector>
#include <string>

bool native_ui_startup();

// consider using one of instead:
//   https://github.com/btzy/nativefiledialog-extended
//   https://github.com/samhocevar/portable-file-dialogs
//   https://sourceforge.net/projects/tinyfiledialogs/

struct FileFilterSpec {
    const wchar_t* name;
    const wchar_t* spec;
};

// paths are the user selected files
// user_selected_file indicates whether the user selected a file. false implies user cancelled the dialog.
// returns true on succesfully displaying the dialog. false implies a windows api error.
bool file_open_dialog(std::vector<std::string>& paths, bool& user_selected_file, const FileFilterSpec* filter_spec, size_t filter_spec_size, bool multi_select);

bool file_save_dialog(std::string& path, bool& user_saved_file, const FileFilterSpec* filter_spec, size_t filter_spec_size, const wchar_t* default_extension);

std::string monospace_font_filename();
