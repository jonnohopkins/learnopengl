#version 330 core

struct Material {
    sampler2D texture_diffuse1;
    sampler2D texture_specular1;
    float shininess;
};

struct DirectionalLight {
    vec3 direction;
  
    vec3 ambient;
    vec3 diffuse;
    vec3 specular;
};

struct PointLight {
    vec3 position;
  
    vec3 ambient;
    vec3 diffuse;
    vec3 specular;

    float constant;
    float linear;
    float quadratic;
};

struct SpotLight {
    vec3 position;
    vec3 direction;
    float cutOff; // cosine of the angle in radians
    float outerCutOff; // cosine of the angle in radians
  
    vec3 ambient;
    vec3 diffuse;
    vec3 specular;
};

in vec3 FragPos;
in vec3 Normal;
in vec2 TexCoords;

#define MAX_NUM_DIRECTION_LIGHTS 2u
#define MAX_NUM_POINT_LIGHTS 8u
#define MAX_NUM_SPOT_LIGHTS 2u

uniform vec3 viewPos;
uniform uint num_direction_lights;
uniform uint num_point_lights;
uniform uint num_spot_lights;
uniform DirectionalLight directionLights[MAX_NUM_DIRECTION_LIGHTS];
uniform PointLight pointLights[MAX_NUM_POINT_LIGHTS];
uniform SpotLight spotLights[MAX_NUM_SPOT_LIGHTS];
uniform Material material;

out vec4 FragColor;

vec3 ambientLighting(vec3 lightAmbient, vec3 diffuseSample) {
    return lightAmbient * diffuseSample;
}

vec3 diffuseLighting(vec3 lightDiffuse, vec3 diffuseSample, vec3 lightDir, vec3 norm) {
    return lightDiffuse * diffuseSample * max(dot(norm, lightDir), 0.0);
}

vec3 specularLighting(vec3 lightSpecular, vec3 specularSample, vec3 lightDir, vec3 norm, vec3 viewDir) {
    vec3 halfwayDir = normalize(lightDir + viewDir);
    return lightSpecular * specularSample * pow(max(dot(norm, halfwayDir), 0.0), material.shininess);
}

vec3 pointLighting(PointLight light, vec3 diffuseSample, vec3 specularSample, vec3 norm, vec3 viewDir) {
    vec3 lightDir = normalize(light.position - FragPos);

    float distance = length(light.position - FragPos);
    float attenuation = 1.0 / (light.constant + light.linear * distance + light.quadratic * (distance * distance));

    return diffuseLighting(light.diffuse, diffuseSample, lightDir, norm) * attenuation +
        specularLighting(light.specular, specularSample, lightDir, norm, viewDir) * attenuation +
        ambientLighting(light.ambient, diffuseSample) * attenuation;
}

vec3 directionalLighting(DirectionalLight light, vec3 diffuseSample, vec3 specularSample, vec3 norm, vec3 viewDir) {
    vec3 lightDir = normalize(-light.direction);

    return diffuseLighting(light.diffuse, diffuseSample, lightDir, norm) +
        specularLighting(light.specular, specularSample, lightDir, norm, viewDir) +
        ambientLighting(light.ambient, diffuseSample);
}

vec3 spotLighting(SpotLight light, vec3 diffuseSample, vec3 specularSample, vec3 norm, vec3 viewDir) {
    vec3 lightDir = normalize(light.position - FragPos);

    // the angle between this fragment to the light position and the spotlight direction.
    float theta = dot(lightDir, normalize(-light.direction));
    float epsilon = light.cutOff - light.outerCutOff;
    float intensity = clamp((theta - light.outerCutOff) / epsilon, 0.0, 1.0);
    
    // we'll leave ambient unaffected so we always have a little light.
    return diffuseLighting(light.diffuse, diffuseSample, lightDir, norm) * intensity +
        specularLighting(light.specular, specularSample, lightDir, norm, viewDir) * intensity +
        ambientLighting(light.ambient, diffuseSample);
}

void main() {
    vec3 diffuseSample = texture(material.texture_diffuse1, TexCoords).rgb;
    vec3 specularSample = texture(material.texture_specular1, TexCoords).rgb;
    vec3 norm = normalize(Normal);
    vec3 viewDir = normalize(viewPos - FragPos);

    vec3 color = vec3(0.0, 0.0, 0.0);

    for(uint i = 0u; i < min(num_direction_lights, MAX_NUM_DIRECTION_LIGHTS); ++i) {
        color += directionalLighting(directionLights[i], diffuseSample, specularSample, norm, viewDir);
    }

    for(uint i = 0u; i < min(num_point_lights, MAX_NUM_POINT_LIGHTS); ++i) {
        color += pointLighting(pointLights[i], diffuseSample, specularSample, norm, viewDir);
    }

    for(uint i = 0u; i < min(num_spot_lights, MAX_NUM_SPOT_LIGHTS); ++i) {
        color += spotLighting(spotLights[i], diffuseSample, specularSample, norm, viewDir);
    }

    FragColor = vec4(color, 1.0);
}