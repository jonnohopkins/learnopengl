#version 330 core

in vec2 TexCoords;

uniform sampler2D image1Texture;
uniform sampler2D image2Texture;
uniform float firstTextureFactor;
uniform float secondTextureFactor;

out vec4 FragColor;

void main() {
    vec3 combine = texture(image1Texture, TexCoords).rgb * firstTextureFactor + texture(image2Texture, TexCoords).rgb * secondTextureFactor;
    FragColor = vec4(combine, 1.0);
}

