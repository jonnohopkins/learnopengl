#version 330 core

in vec2 TexCoords;

uniform sampler2D imageTexture;

out vec4 FragColor;

void main() {
    vec2 tex_offset = 1.0 / textureSize(imageTexture, 0); // gets size of single texel

    float offset = 1.0;
    vec3 colorAvg = texture(imageTexture, TexCoords + tex_offset * vec2(offset, 0.0)).rgb * 0.25 +
            texture(imageTexture, TexCoords + tex_offset * vec2(-offset, 0.0)).rgb * 0.25 +
            texture(imageTexture, TexCoords + tex_offset * vec2(0.0, offset)).rgb * 0.25 +
            texture(imageTexture, TexCoords + tex_offset * vec2(0.0, -offset)).rgb * 0.25;

    FragColor = vec4(colorAvg, 1.0);
}
