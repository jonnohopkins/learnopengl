#version 330 core

struct Material {
    sampler2D texture_diffuse1;
    sampler2D texture_specular1;
    sampler2D texture_normal1;
    float shininess;
};

in vec3 Normal;
in vec2 TexCoords;
in mat3 TBN;

uniform Material material;

out vec4 FragColor;

void main() {
    vec3 normal = texture(material.texture_normal1, TexCoords).rgb;
    FragColor = vec4(normal, 1.0);
}
