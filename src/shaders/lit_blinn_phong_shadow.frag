#version 330 core

struct Material {
    sampler2D texture_diffuse1;
    sampler2D texture_specular1;
    sampler2D texture_normal1;
    float shininess;
};

struct DirectionalLight {
    vec3 direction;
  
    vec3 ambient;
    vec3 diffuse;
    vec3 specular;
};

struct PointLight {
    vec3 position;
  
    vec3 ambient;
    vec3 diffuse;
    vec3 specular;

    float constant;
    float linear;
    float quadratic;
};

struct SpotLight {
    vec3 position;
    vec3 direction;
    float cutOff; // cosine of the angle in radians
    float outerCutOff; // cosine of the angle in radians
  
    vec3 ambient;
    vec3 diffuse;
    vec3 specular;
};

in vec3 FragPos;
in vec3 Normal;
in vec2 TexCoords;
in vec4 FragPosLightSpace;
in mat3 TBN;

#define MAX_NUM_DIRECTION_LIGHTS 2u
#define MAX_NUM_POINT_LIGHTS 8u
#define MAX_NUM_SPOT_LIGHTS 2u

uniform vec3 viewPos;
uniform uint num_direction_lights;
uniform uint num_point_lights;
uniform uint num_spot_lights;
uniform DirectionalLight directionLights[MAX_NUM_DIRECTION_LIGHTS];
uniform PointLight pointLights[MAX_NUM_POINT_LIGHTS];
uniform SpotLight spotLights[MAX_NUM_SPOT_LIGHTS];
uniform Material material;
uniform sampler2D directionalShadowDepth;
uniform uint shadowPointLightIdx; // the pointShadowDepth is for the light with this index
uniform samplerCube pointShadowDepth;
uniform float shadow_far_plane;

out vec4 FragColor;

vec3 ambientLighting(vec3 lightAmbient, vec3 diffuseSample) {
    return lightAmbient * diffuseSample;
}

vec3 diffuseLighting(vec3 lightDiffuse, vec3 diffuseSample, vec3 lightDir, vec3 norm) {
    return lightDiffuse * diffuseSample * max(dot(norm, lightDir), 0.0);
}

vec3 specularLighting(vec3 lightSpecular, vec3 specularSample, vec3 lightDir, vec3 norm, vec3 viewDir) {
    vec3 halfwayDir = normalize(lightDir + viewDir);
    return lightSpecular * specularSample * pow(max(dot(norm, halfwayDir), 0.0), material.shininess);
}

float calculateDirectionalShadow(vec3 lightDir, vec3 norm) {
    vec3 projCoords = FragPosLightSpace.xyz / FragPosLightSpace.w;
    projCoords = projCoords * 0.5 + 0.5; // map NDC coordinates to [0, 1] range
    float currentDepth = projCoords.z > 1.0 ? 1.0 : projCoords.z;

    float bias = 0.002;
    vec2 texelSize = 1.0 / textureSize(directionalShadowDepth, 0);

    float shadow = 0.0;
    for(int x = 0; x < 4; ++x) {
        for(int y = 0; y < 4; ++y) {
            float pcfDepth = texture(directionalShadowDepth, projCoords.xy + vec2(x - 1.5, y - 1.5) * texelSize).r;
            shadow += currentDepth - bias > pcfDepth ? 1.0 : 0.0;
        }
    }
    shadow /= 16.0;

    return shadow;
}

float calculatePointShadow(PointLight light, vec3 lightDir, vec3 norm) {
    vec3 lightToFrag = FragPos - light.position;

    float currentDepth = length(lightToFrag) / shadow_far_plane;

    float bias = 0.005;
    
    vec3 sampleOffsetDirections[20] = vec3[] (
       vec3( 1,  1,  1), vec3( 1, -1,  1), vec3(-1, -1,  1), vec3(-1,  1,  1), 
       vec3( 1,  1, -1), vec3( 1, -1, -1), vec3(-1, -1, -1), vec3(-1,  1, -1),
       vec3( 1,  1,  0), vec3( 1, -1,  0), vec3(-1, -1,  0), vec3(-1,  1,  0),
       vec3( 1,  0,  1), vec3(-1,  0,  1), vec3( 1,  0, -1), vec3(-1,  0, -1),
       vec3( 0,  1,  1), vec3( 0, -1,  1), vec3( 0, -1, -1), vec3( 0,  1, -1)
    );

    float shadow = 0.0;
    float diskRadius = 0.01;
    //float viewDistance = length(viewPos - FragPos);
    //float diskRadius = (1.0 + (viewDistance / shadow_far_plane)) / 100.0;
    for(int i = 0; i < 20; ++i) {
        float pcfDepth = texture(pointShadowDepth, lightToFrag + sampleOffsetDirections[i] * diskRadius).r;
        shadow += currentDepth - bias > pcfDepth ? 1.0 : 0.0;
    }
    shadow /= 20;

    return shadow;
}

vec3 pointLighting(PointLight light, bool has_shadow, vec3 diffuseSample, vec3 specularSample, vec3 norm, vec3 viewDir) {
    vec3 lightDir = normalize(light.position - FragPos);

    float distance = length(light.position - FragPos);
    float attenuation = 1.0 / (light.constant + light.linear * distance + light.quadratic * (distance * distance));
    
    vec3 diffuse = diffuseLighting(light.diffuse, diffuseSample, lightDir, norm);
    vec3 specular = specularLighting(light.specular, specularSample, lightDir, norm, viewDir);
    vec3 ambient = ambientLighting(light.ambient, diffuseSample);

    float shadow = has_shadow ? calculatePointShadow(light, lightDir, norm) : 0.0;

    return ((1.0 - shadow) * (diffuse + specular) + ambient) * attenuation;
}

vec3 directionalLighting(DirectionalLight light, vec3 diffuseSample, vec3 specularSample, vec3 norm, vec3 viewDir) {
    vec3 lightDir = normalize(-light.direction);

    vec3 diffuse = diffuseLighting(light.diffuse, diffuseSample, lightDir, norm);
    vec3 specular = specularLighting(light.specular, specularSample, lightDir, norm, viewDir);
    vec3 ambient = ambientLighting(light.ambient, diffuseSample);

    return (1.0 - calculateDirectionalShadow(lightDir, norm)) * (diffuse + specular) + ambient;
}

vec3 spotLighting(SpotLight light, vec3 diffuseSample, vec3 specularSample, vec3 norm, vec3 viewDir) {
    vec3 lightDir = normalize(light.position - FragPos);

    // the angle between this fragment to the light position and the spotlight direction.
    float theta = dot(lightDir, normalize(-light.direction));
    float epsilon = light.cutOff - light.outerCutOff;
    float intensity = clamp((theta - light.outerCutOff) / epsilon, 0.0, 1.0);
    
    vec3 diffuse = diffuseLighting(light.diffuse, diffuseSample, lightDir, norm);
    vec3 specular = specularLighting(light.specular, specularSample, lightDir, norm, viewDir);
    vec3 ambient = ambientLighting(light.ambient, diffuseSample);

    // we'll leave ambient unaffected so we always have a little light.
    return (diffuse + specular) * intensity + ambient;
}

vec3 calculateNormal() {
    vec3 normal = texture(material.texture_normal1, TexCoords).rgb;
    //vec3 normal = vec3(0.5, 0.5, 1.0);
    normal = normal * 2.0 - 1.0; // map [0, 1] range to [-1, 1]
    normal = normalize(TBN * normal); // change normal to world space
    return normal;
}

void main() {
    vec3 diffuseSample = texture(material.texture_diffuse1, TexCoords).rgb;
    vec3 specularSample = texture(material.texture_specular1, TexCoords).rgb;
    vec3 normalSample = calculateNormal();
    vec3 viewDir = normalize(viewPos - FragPos);

    vec3 color = vec3(0.0, 0.0, 0.0);

    for(uint i = 0u; i < min(num_direction_lights, MAX_NUM_DIRECTION_LIGHTS); ++i) {
        color += directionalLighting(directionLights[i], diffuseSample, specularSample, normalSample, viewDir);
    }

    for(uint i = 0u; i < min(num_point_lights, MAX_NUM_POINT_LIGHTS); ++i) {
        bool has_shadow = (i == shadowPointLightIdx);
        color += pointLighting(pointLights[i], has_shadow, diffuseSample, specularSample, normalSample, viewDir);
    }

    for(uint i = 0u; i < min(num_spot_lights, MAX_NUM_SPOT_LIGHTS); ++i) {
        color += spotLighting(spotLights[i], diffuseSample, specularSample, normalSample, viewDir);
    }

    FragColor = vec4(color, 1.0);
}
