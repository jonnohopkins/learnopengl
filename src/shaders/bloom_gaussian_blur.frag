#version 330 core

in vec2 TexCoords;

uniform sampler2D imageTexture;
uniform bool isHorizontal;
uniform int kernelSize;

out vec4 FragColor;

#define MAX_WEIGHT_SIZE 21u

//const float weight[5] = float[] (0.2270270270, 0.1945945946, 0.1216216216, 0.0540540541, 0.0162162162);
uniform float weight[MAX_WEIGHT_SIZE];

void main() {
     vec2 tex_offset = 1.0 / textureSize(imageTexture, 0); // gets size of single texel
     vec3 result = vec3(0.0);
     if(isHorizontal) {
         for(int i = 0; i < kernelSize; ++i) {
            result += texture(imageTexture, TexCoords + vec2(tex_offset.x * (i - (kernelSize - 1) / 2.0), 0.0)).rgb * weight[i];
         }
     }
     else {
         for(int i = 0; i < kernelSize; ++i) {
             result += texture(imageTexture, TexCoords + vec2(0.0, tex_offset.y * (i - (kernelSize - 1) / 2.0))).rgb * weight[i];
         }
     }
     FragColor = vec4(result, 1.0);
}