#version 330 core

layout (location = 0) in vec3 aPos;
layout (location = 1) in vec3 aNormal;
layout (location = 2) in vec2 aTexCoords;
layout (location = 3) in vec3 aTangent;

uniform mat4 model;
uniform mat4 view;

out VS_OUT {
    vec3 normal;
    vec3 tangent;
} vs_out;

void main() {
    gl_Position = view * model * vec4(aPos, 1.0);
    vs_out.normal = normalize(mat3(transpose(inverse(view * model))) * aNormal);
    vs_out.tangent = normalize(mat3(transpose(inverse(view * model))) * aTangent);
}
