#version 330 core

in vec2 TexCoords;

uniform sampler2D imageTexture;
uniform float brightnessCutoff;
uniform float brightnessFull; // assumes brightnessCutoff < brightnessFull
uniform float texCoordsScale;

out vec4 FragColor;

const vec3 luminance_average = vec3(0.2126, 0.7152, 0.0722);

// aka. inverse lerp
float linearstep(float min, float max, float value) {
    return (value - min) / (max - min);
}

void main() {
    vec3 color = texture(imageTexture, TexCoords * texCoordsScale).rgb;

    float brightness = dot(color, luminance_average);
    if(brightness < brightnessCutoff)
        color = vec3(0.0);
    color *= clamp(smoothstep(brightnessCutoff, brightnessFull, brightness), 0.0, 1.0);

    FragColor = vec4(color, 1.0);
}
