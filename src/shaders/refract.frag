#version 330 core

in vec3 Normal;
in vec3 FragPos;

uniform vec3 viewPos;
uniform samplerCube skybox;

out vec4 FragColor;

void main() {
    float source_refractive_index = 1.0; // ie. air.
    float dest_refractive_index = 1.52; // ie. glass.

    vec3 viewDir = normalize(viewPos - FragPos);
    vec3 refract = refract(-viewDir, normalize(Normal), source_refractive_index / dest_refractive_index);

    FragColor = vec4(texture(skybox, refract).rgb, 1.0);
}