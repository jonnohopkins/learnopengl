#version 330 core

in vec3 Normal;
in vec3 FragPos;

uniform vec3 viewPos;
uniform samplerCube skybox;

out vec4 FragColor;

void main() {
    vec3 viewDir = normalize(viewPos - FragPos);
    vec3 reflect = reflect(-viewDir, normalize(Normal));

    FragColor = vec4(texture(skybox, reflect).rgb, 1.0);
}