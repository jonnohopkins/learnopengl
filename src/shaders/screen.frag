#version 330 core

in vec2 TexCoords;

uniform sampler2D imageTexture;
uniform float exposure;

out vec4 FragColor;

void main() {
    vec3 hdrColor = texture(imageTexture, TexCoords).rgb;

    //vec3 ldrColor = hdrColor; // no HDR
    //vec3 ldrColor = hdrColor / (hdrColor + vec3(1.0)); // reinhard tone mapping
    vec3 ldrColor = vec3(1.0) - exp(-hdrColor * exposure); // exposure tone mapping

    // gamma correction
    const float gamma = 2.2;
    ldrColor = pow(ldrColor, vec3(1.0 / gamma));

    FragColor = vec4(ldrColor, 1.0);
}
