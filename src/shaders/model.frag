#version 330 core

struct Material {
    sampler2D texture_diffuse1;
};

in vec2 TexCoords;

uniform Material material;

out vec4 FragColor;

void main() {
    FragColor = texture(material.texture_diffuse1, TexCoords);
}
