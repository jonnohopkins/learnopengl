#version 330 core

layout (triangles) in;
layout (line_strip, max_vertices = 6) out;

uniform mat4 projection;

in VS_OUT {
    vec3 normal;
    vec3 tangent;
} gs_in[];

const float magnitude = 0.1;

void GenerateLine(int index) {
    //vec3 bitangent = cross(gs_in[index].normal, gs_in[index].tangent);
    gl_Position = projection * gl_in[index].gl_Position;
    EmitVertex();
    gl_Position = projection * (gl_in[index].gl_Position + vec4(gs_in[index].normal, 0.0) * magnitude);
    EmitVertex();
    EndPrimitive();
}

void main() {
    GenerateLine(0);
    GenerateLine(1);
    GenerateLine(2);
}
