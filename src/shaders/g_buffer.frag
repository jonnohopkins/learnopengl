#version 330 core

struct Material {
    sampler2D texture_diffuse1;
    sampler2D texture_specular1;
    sampler2D texture_normal1;
};

in vec3 FragPos;
in vec2 TexCoords;
in mat3 TBN;

uniform Material material;

layout (location = 0) out vec3 gPosition;
layout (location = 1) out vec3 gNormal;
layout (location = 2) out vec4 gAlbedoSpec;

vec3 calculateNormal() {
    vec3 normal = texture(material.texture_normal1, TexCoords).rgb;
    //vec3 normal = vec3(0.5, 0.5, 1.0);
    normal = normal * 2.0 - 1.0; // map [0, 1] range to [-1, 1]
    normal = normalize(TBN * normal); // change normal to world space
    return normal;
}

void main() {
    gPosition = FragPos;
    gNormal = calculateNormal();
    gAlbedoSpec.rgb = texture(material.texture_diffuse1, TexCoords).rgb;
    gAlbedoSpec.a = texture(material.texture_specular1, TexCoords).r;
}