#version 330 core

layout (location = 0) in vec2 aPos;
layout (location = 1) in vec2 aTexCoords;

uniform mat4 lightSpaceMatrix;
uniform sampler2D gPosition;

out vec2 TexCoords;
out vec4 FragPosLightSpace;

void main() {
    gl_Position = vec4(aPos.x, aPos.y, 0.0, 1.0);
    TexCoords = aTexCoords;
    vec3 fragPos = texture(gPosition, TexCoords).rgb;
    FragPosLightSpace = lightSpaceMatrix * vec4(fragPos, 1.0);
}