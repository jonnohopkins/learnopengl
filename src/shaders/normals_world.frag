#version 330 core

struct Material {
    sampler2D texture_diffuse1;
    sampler2D texture_specular1;
    sampler2D texture_normal1;
    float shininess;
};

in vec3 Normal;
in vec2 TexCoords;
in mat3 TBN;

uniform Material material;

out vec4 FragColor;

vec3 calculateNormal() {
    vec3 normal = texture(material.texture_normal1, TexCoords).rgb;
    //vec3 normal = vec3(0.5, 0.5, 1.0);
    normal = normal * 2.0 - 1.0; // map [0, 1] range to [-1, 1]
    normal = normalize(TBN * normal); // change normal to world space
    return normal;
}

void main() {
    vec3 normal = calculateNormal();
    FragColor = vec4(normal * 0.5 + 0.5, 1.0);
}
