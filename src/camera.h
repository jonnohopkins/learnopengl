#pragma once

#include <glm/glm.hpp>

#include <vector>

#include "transform.h"

class Camera {
public:
    Transform transform;

    const float MovementSpeed = 2.5f;
    const float MouseSensitivity = 0.1f;

    explicit Camera(glm::vec3 position = glm::vec3(0.0f, 0.0f, 0.0f), float yaw = 180.0f, float pitch = 0.0f);

    void ProcessKeyboard(glm::vec3 direction, float deltaTime);

    void ProcessMouseMovement(float xoffset, float yoffset, bool constrainPitch = true);
};
