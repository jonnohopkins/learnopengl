#pragma once

#include "scene.h"

#include <vector>

#include <imgui.h>
#include <ImGuizmo.h>

class Editor {
public:
	bool interacting_with_ui{ true };

	Editor();
	Editor(const Editor&) = delete;
	Editor(Editor&&) = delete;
	Editor& operator =(const Editor&) = delete;
	Editor& operator =(Editor&&) = delete;
	~Editor() = default;

	void display(bool& mesh_buffer_requires_update, bool& light_shadow_info_requires_update);

private:
	bool show_scene_window{ false };
	bool show_entity_window{ false };
	bool show_prefab_window{ false };
	bool show_model_window{ false };
	bool show_demo_window{ false };
	bool show_bloom_window{ false };
	bool show_physics_2d_window{ false };
	std::vector<std::pair<scene_id_t, entity_id_t>> entities_selected; // sorted
	std::vector<model_id_t> models_selected; // sorted
	ImGuizmo::OPERATION manipulator_operation{ ImGuizmo::OPERATION::TRANSLATE };
	ImGuizmo::MODE manipulator_mode{ ImGuizmo::MODE::LOCAL };

	void show_main_menu();
	bool scene_window();
	void entity_window(bool& entity_update, bool& transform_update, bool& mesh_updated, bool& light_updated);
	void prefabs_window();
	bool models_window();
	void bloom_window();
	void physics_2d_window();
	void manipulators();
};

extern Editor editor;
