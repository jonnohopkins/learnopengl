#pragma once

#include <vector>
#include <string>

typedef size_t entity_id_t;

typedef size_t scene_id_t;

class Entity {
public:
    bool is_active{ true };
    std::string name; // todo: should this be stored in the entity or separately?
    entity_id_t parent{ 0 };
    std::vector<entity_id_t> children; // NOT sorted
};
