#include "camera.h"

#include <glm/gtc/matrix_transform.hpp>

Camera::Camera(glm::vec3 position, float yaw, float pitch) {
    transform.Position = position;
    transform.EulerAngles.y = yaw;
    transform.EulerAngles.x = pitch;
    transform.UpdateRotation();
}

void Camera::ProcessKeyboard(glm::vec3 direction, float deltaTime) {
    float velocity = MovementSpeed * deltaTime;
    transform.Position += transform.Forward() * velocity * direction.z;
    transform.Position += transform.Right() * velocity * direction.x;
    transform.Position += transform.Up() * velocity * direction.y;
}

void Camera::ProcessMouseMovement(float xoffset, float yoffset, bool constrainPitch) {
    xoffset *= MouseSensitivity;
    yoffset *= MouseSensitivity;

    transform.EulerAngles.y -= xoffset;
    transform.EulerAngles.x -= yoffset;

    // make sure that when pitch is out of bounds, screen doesn't get flipped
    if (constrainPitch) {
        if (transform.EulerAngles.x > 89.0f)
            transform.EulerAngles.x = 89.0f;
        if (transform.EulerAngles.x < -89.0f)
            transform.EulerAngles.x = -89.0f;
    }

    transform.UpdateRotation();
}
