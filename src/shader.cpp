#include "shader.h"

#include <string>
#include <fstream>
#include <sstream>
#include <iostream>
#include <final_action.h>

std::string load_from_file(const char* path) {
    std::string code;
    std::ifstream shaderFile;
    shaderFile.exceptions(std::ifstream::failbit | std::ifstream::badbit);
    try {
        shaderFile.open(path);
        std::stringstream shaderStream;
        shaderStream << shaderFile.rdbuf();
        shaderFile.close();
        code = shaderStream.str();
    }
    catch (std::ifstream::failure& e) {
        throw std::runtime_error{ std::string("unable to read shader file:") + path };
    }

    return code;
}

GLuint create_shader(GLenum shader_type, const char* code, const char* shader_name) {
    GLuint shader = glCreateShader(shader_type);
    glShaderSource(shader, 1, &code, NULL);
    glCompileShader(shader);

    // check that the shader compiled correctly
    GLint success;
    static constexpr size_t infoLogSize = 1024;
    GLchar infoLog[infoLogSize];
    glGetShaderiv(shader, GL_COMPILE_STATUS, &success);
    if (!success) {
        glGetShaderInfoLog(shader, infoLogSize, NULL, infoLog);
        std::cerr << "shader compilation failed for '" << shader_name << "':\n" << infoLog << "\n---------------------------------------------------" << std::endl;
        throw std::runtime_error{ "shader compilation failed." };
    }

    return shader;
}

void check_program_linked(GLuint program) {
    GLint success;
    static constexpr size_t infoLogSize = 1024;
    GLchar infoLog[infoLogSize];
    glGetProgramiv(program, GL_LINK_STATUS, &success);
    if (!success) {
        glGetProgramInfoLog(program, infoLogSize, NULL, infoLog);
        std::cerr << "program linking failed:\n" << infoLog << "\n---------------------------------------------------" << std::endl;
        throw std::runtime_error{ "shader linking failed." };
    }
}

void Shader::create_regular(const char* vertexPath, const char* fragmentPath, const char* geometryPath) {
    std::string vertexCode = load_from_file(vertexPath);
    std::string fragmentCode = load_from_file(fragmentPath);

    GLuint vertex = create_shader(GL_VERTEX_SHADER, vertexCode.c_str(), vertexPath);
    auto vertex_final = xtd::finally([vertex]() { glDeleteShader(vertex); });
    GLuint fragment = create_shader(GL_FRAGMENT_SHADER, fragmentCode.c_str(), fragmentPath);
    auto fragment_final = xtd::finally([fragment]() { glDeleteShader(fragment); });
    GLuint geometry = 0;
    if (geometryPath != nullptr) {
        std::string geometryCode = load_from_file(geometryPath);
        geometry = create_shader(GL_GEOMETRY_SHADER, geometryCode.c_str(), geometryPath);
    }
    auto geometry_final = xtd::finally([geometry]() { glDeleteShader(geometry); });

    // create the shader program
    auto program = glCreateProgram();
    glAttachShader(program, vertex);
    glAttachShader(program, fragment);
    if (geometryPath != nullptr)
        glAttachShader(program, geometry);
    glLinkProgram(program);

    check_program_linked(program);

    glDeleteProgram(ID); // free the previous program, if available
    ID = program;
}

void Shader::create_compute(const char* computePath) {
    std::string computeCode = load_from_file(computePath);

    GLuint compute = create_shader(GL_COMPUTE_SHADER, computeCode.c_str(), computePath);
    auto compute_final = xtd::finally([compute]() { glDeleteShader(compute); });

    // create the shader program
    auto program = glCreateProgram();
    glAttachShader(program, compute);
    glLinkProgram(program);

    check_program_linked(program);

    glDeleteProgram(ID); // free the previous program, if available
    ID = program;
}
