#pragma once

#include <glm/glm.hpp>

enum class LightType {
    Directional,
    Point,
    Spot,
};

class Light {
public:
    LightType type{ LightType::Point };

    // point light:
    float constant = 1.0f;
    float linear = 0.09f;
    float quadratic = 0.032f;

    // spot light:
    float cutoff = 12.5f;
    float outer_cutoff = 17.5f;

    // common to all light types:
    glm::vec3 ambient{ 1.0f, 1.0f, 1.0f };
    float ambient_intensity = 0.2f;
    glm::vec3 diffuse{ 1.0f, 1.0f, 1.0f };
    float diffuse_intensity = 0.5f;
    glm::vec3 specular{ 1.0f, 1.0f, 1.0f };
    float specular_intensity = 0.5f;
};
