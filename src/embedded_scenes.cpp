#include "embedded_scenes.h"

#include "globals.h"
#include "model.h"
#include "scene.h"
#include "assets.h"
#include "renderer.h"

#include <glad/glad.h>

#include <assimp/DefaultLogger.hpp>

void create_default_textures() {
    auto create_texture = [](unsigned char* value, bool gamma) -> GLuint {
        const GLenum internal_format = gamma ? GL_SRGB : GL_RGB;
        const GLenum data_format = GL_RGB;

        GLuint texture;
        glGenTextures(1, &texture);
        glBindTexture(GL_TEXTURE_2D, texture);
        glTexImage2D(GL_TEXTURE_2D, 0, internal_format, 1, 1, 0, data_format, GL_UNSIGNED_BYTE, value);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

        return texture;
    };

    unsigned char black_color[3] = { 0, 0, 0 };
    unsigned char white_color[3] = { 255, 255, 255 };
    unsigned char normal_color[3] = { 127, 127, 255 };

    textures.reserve(4);
    textures.emplace_back(new_texture_id(), true, create_texture(white_color, false), TextureType::Diffuse, "embedded/diffuse");
    textures.emplace_back(new_texture_id(), true, create_texture(black_color, false), TextureType::Specular, "embedded/specular");
    textures.emplace_back(new_texture_id(), true, create_texture(normal_color, false), TextureType::Normal, "embedded/normal");
    textures.emplace_back(new_texture_id(), true, create_texture(black_color, false), TextureType::Height, "embedded/height");
}

void get_tangent_and_bitangent(const Vertex& v1, const Vertex& v2, const Vertex& v3, glm::vec3& tangent, glm::vec3& bitangent) {
    glm::vec3 edge1 = v2.Position - v1.Position;
    glm::vec3 edge2 = v3.Position - v1.Position;
    glm::vec2 deltaUV1 = v2.TexCoords - v1.TexCoords;
    glm::vec2 deltaUV2 = v3.TexCoords - v1.TexCoords;

    float f = 1.0f / (deltaUV1.x * deltaUV2.y - deltaUV2.x * deltaUV1.y);

    tangent.x = f * (deltaUV2.y * edge1.x - deltaUV1.y * edge2.x);
    tangent.y = f * (deltaUV2.y * edge1.y - deltaUV1.y * edge2.y);
    tangent.z = f * (deltaUV2.y * edge1.z - deltaUV1.y * edge2.z);

    bitangent.x = f * (-deltaUV2.x * edge1.x + deltaUV1.x * edge2.x);
    bitangent.y = f * (-deltaUV2.x * edge1.y + deltaUV1.x * edge2.y);
    bitangent.z = f * (-deltaUV2.x * edge1.z + deltaUV1.x * edge2.z);
}

void set_tangents_for_face(std::vector<Vertex>& vertices, size_t idx) {
    glm::vec3 tangent, bitangent;
    get_tangent_and_bitangent(vertices[idx], vertices[idx + 1], vertices[idx + 2], tangent, bitangent);
    vertices[idx].Tangent = tangent;
    vertices[idx + 1].Tangent = tangent;
    vertices[idx + 2].Tangent = tangent;
    vertices[idx + 3].Tangent = tangent;
    //vertices[idx].Bitangent = glm::cross(vertices[idx].Normal, tangent);
    //vertices[idx + 1].Bitangent = glm::cross(vertices[idx + 1].Normal, tangent);
    //vertices[idx + 2].Bitangent = glm::cross(vertices[idx + 2].Normal, tangent);
    //vertices[idx + 3].Bitangent = glm::cross(vertices[idx + 3].Normal, tangent);
};

void create_wood_plane_model(size_t diffuse_tex_idx, size_t specular_tex_idx, size_t normal_tex_idx, size_t height_tex_id) {
    Model model;
    model.path = "embedded/plane";

    auto& [ mesh_id, mesh ] = *emplace(meshes, new_mesh_id(), Mesh{});
    model.owned_meshes.push_back(mesh_id);

    Texture& wood_tex = textures.emplace_back(new_texture_id(), false, textures[diffuse_tex_idx].gl_id, TextureType::Diffuse, "wood.png");
    model.owned_textures.push_back(wood_tex.tex_id);
    mesh.textures.emplace_back(wood_tex);

    mesh.vertices = {
        // positions                     // normals                   // texcoords
        Vertex{ glm::vec3(-10.0f, 0.0f, -10.0f), glm::vec3(0.0f, 1.0f, 0.0f), glm::vec2(0.0f, 10.0f) },
        Vertex{ glm::vec3(-10.0f, 0.0f,  10.0f), glm::vec3(0.0f, 1.0f, 0.0f), glm::vec2(0.0f,  0.0f) },
        Vertex{ glm::vec3(10.0f, 0.0f, -10.0f), glm::vec3(0.0f, 1.0f, 0.0f), glm::vec2(10.0f, 10.0f) },
        Vertex{ glm::vec3(10.0f, 0.0f,  10.0f), glm::vec3(0.0f, 1.0f, 0.0f), glm::vec2(10.0f,  0.0f) },
    };

    set_tangents_for_face(mesh.vertices, 0);

    mesh.indices = {
        0, 1, 2,
        3, 2, 1
    };

    mesh.textures.emplace_back(textures[specular_tex_idx]);
    mesh.textures.emplace_back(textures[normal_tex_idx]);
    mesh.textures.emplace_back(textures[height_tex_id]);

    mesh.setup();

    renderer.add_instance_for_mesh(mesh_id, mesh);

    auto& [ scene_id, scene ] = *emplace(scenes, new_scene_id(), Scene{});
    prefabs.insert(std::lower_bound(prefabs.begin(), prefabs.end(), scene_id), scene_id);

    auto& [entity_id, entity] = scene.create_entity();
    scene.root_entities.push_back(entity_id);
    entity.name = "plane";
    emplace(scene.transforms, entity_id, Transform{});
    emplace(scene.entity_meshes, entity_id, mesh_id);

    model.scene_id = scene_id;
    emplace(models, new_model_id(), std::move(model));
}

void create_container_cube_model(size_t diffuse_tex_idx, size_t specular_tex_idx, size_t normal_tex_idx, size_t height_tex_id) {
    Model model;
    model.path = "embedded/container";

    auto& [mesh_id, mesh] = *emplace(meshes, new_mesh_id(), Mesh{});
    model.owned_meshes.push_back(mesh_id);

    Texture& container_tex = textures.emplace_back(new_texture_id(), false, textures[diffuse_tex_idx].gl_id, TextureType::Diffuse, "container2.png");
    model.owned_textures.push_back(container_tex.tex_id);
    mesh.textures.emplace_back(container_tex);

    Texture& container_spec_tex = textures.emplace_back(new_texture_id(), false, textures[specular_tex_idx].gl_id, TextureType::Specular, "container2_specular.png");
    model.owned_textures.push_back(container_spec_tex.tex_id);
    mesh.textures.emplace_back(container_spec_tex);

    mesh.vertices = {
        // positions                    // normals                     // texture coords
        // back face
        Vertex{ glm::vec3(-0.5f, -0.5f, -0.5f), glm::vec3(0.0f,  0.0f, -1.0f), glm::vec2(1.0f,  0.0f) },
        Vertex{ glm::vec3(-0.5f,  0.5f, -0.5f), glm::vec3(0.0f,  0.0f, -1.0f), glm::vec2(1.0f,  1.0f) },
        Vertex{ glm::vec3(0.5f, -0.5f, -0.5f), glm::vec3(0.0f,  0.0f, -1.0f), glm::vec2(0.0f,  0.0f) },
        Vertex{ glm::vec3(0.5f,  0.5f, -0.5f), glm::vec3(0.0f,  0.0f, -1.0f), glm::vec2(0.0f,  1.0f) },

        // front face
        Vertex{ glm::vec3(-0.5f, -0.5f,  0.5f), glm::vec3(0.0f,  0.0f,  1.0f), glm::vec2(0.0f,  0.0f) },
        Vertex{ glm::vec3(0.5f, -0.5f,  0.5f), glm::vec3(0.0f,  0.0f,  1.0f), glm::vec2(1.0f,  0.0f) },
        Vertex{ glm::vec3(-0.5f,  0.5f,  0.5f), glm::vec3(0.0f,  0.0f,  1.0f), glm::vec2(0.0f,  1.0f) },
        Vertex{ glm::vec3(0.5f,  0.5f,  0.5f), glm::vec3(0.0f,  0.0f,  1.0f), glm::vec2(1.0f,  1.0f) },

        // left face
        Vertex{ glm::vec3(0.5f,  0.5f,  0.5f), glm::vec3(1.0f,  0.0f,  0.0f), glm::vec2(0.0f,  1.0f) },
        Vertex{ glm::vec3(0.5f, -0.5f,  0.5f), glm::vec3(1.0f,  0.0f,  0.0f), glm::vec2(0.0f,  0.0f) },
        Vertex{ glm::vec3(0.5f,  0.5f, -0.5f), glm::vec3(1.0f,  0.0f,  0.0f), glm::vec2(1.0f,  1.0f) },
        Vertex{ glm::vec3(0.5f, -0.5f, -0.5f), glm::vec3(1.0f,  0.0f,  0.0f), glm::vec2(1.0f,  0.0f) },

        // right face
        Vertex{ glm::vec3(-0.5f,  0.5f,  0.5f), glm::vec3(-1.0f,  0.0f,  0.0f), glm::vec2(1.0f,  1.0f) },
        Vertex{ glm::vec3(-0.5f,  0.5f, -0.5f), glm::vec3(-1.0f,  0.0f,  0.0f), glm::vec2(0.0f,  1.0f) },
        Vertex{ glm::vec3(-0.5f, -0.5f,  0.5f), glm::vec3(-1.0f,  0.0f,  0.0f), glm::vec2(1.0f,  0.0f) },
        Vertex{ glm::vec3(-0.5f, -0.5f, -0.5f), glm::vec3(-1.0f,  0.0f,  0.0f), glm::vec2(0.0f,  0.0f) },

        // bottom face
        Vertex{ glm::vec3(-0.5f, -0.5f, -0.5f), glm::vec3(0.0f, -1.0f,  0.0f), glm::vec2(1.0f,  1.0f) },
        Vertex{ glm::vec3(0.5f, -0.5f, -0.5f), glm::vec3(0.0f, -1.0f,  0.0f), glm::vec2(0.0f,  1.0f) },
        Vertex{ glm::vec3(-0.5f, -0.5f,  0.5f), glm::vec3(0.0f, -1.0f,  0.0f), glm::vec2(1.0f,  0.0f) },
        Vertex{ glm::vec3(0.5f, -0.5f,  0.5f), glm::vec3(0.0f, -1.0f,  0.0f), glm::vec2(0.0f,  0.0f) },

        // top face
        Vertex{ glm::vec3(-0.5f,  0.5f, -0.5f), glm::vec3(0.0f,  1.0f,  0.0f), glm::vec2(1.0f,  0.0f) },
        Vertex{ glm::vec3(-0.5f,  0.5f,  0.5f), glm::vec3(0.0f,  1.0f,  0.0f), glm::vec2(1.0f,  1.0f) },
        Vertex{ glm::vec3(0.5f,  0.5f, -0.5f), glm::vec3(0.0f,  1.0f,  0.0f), glm::vec2(0.0f,  0.0f) },
        Vertex{ glm::vec3(0.5f,  0.5f,  0.5f), glm::vec3(0.0f,  1.0f,  0.0f), glm::vec2(0.0f,  1.0f) },
    };

    set_tangents_for_face(mesh.vertices, 0);
    set_tangents_for_face(mesh.vertices, 4);
    set_tangents_for_face(mesh.vertices, 8);
    set_tangents_for_face(mesh.vertices, 12);
    set_tangents_for_face(mesh.vertices, 16);
    set_tangents_for_face(mesh.vertices, 20);

    mesh.indices = {
        // back face
        0, 1, 2,
        3, 2, 1,
        // front face
        4, 5, 6,
        7, 6, 5,
        // left face
        8, 9, 10,
        11, 10, 9,
        // right face
        12, 13, 14,
        15, 14, 13,
        // bottom face
        16, 17, 18,
        19, 18, 17,
        // top face
        20, 21, 22,
        23, 22, 21,
    };

    mesh.textures.emplace_back(textures[specular_tex_idx]);
    mesh.textures.emplace_back(textures[normal_tex_idx]);
    mesh.textures.emplace_back(textures[height_tex_id]);

    mesh.setup();

    renderer.add_instance_for_mesh(mesh_id, mesh);

    auto& [scene_id, scene] = *emplace(scenes, new_scene_id(), Scene{});
    prefabs.insert(std::lower_bound(prefabs.begin(), prefabs.end(), scene_id), scene_id);

    auto& [entity_id, entity] = scene.create_entity();
    scene.root_entities.push_back(entity_id);
    entity.name = "wooden cube";
    emplace(scene.transforms, entity_id, Transform{});
    emplace(scene.entity_meshes, entity_id, mesh_id);

    model.scene_id = scene_id;
    emplace(models, new_model_id(), std::move(model));
}

void create_brickwall_model(size_t diffuse_tex_idx, size_t specular_tex_idx, size_t normal_tex_idx, size_t height_tex_id) {
    Model model;
    model.path = "embedded/brickwall";

    auto& [mesh_id, mesh] = *emplace(meshes, new_mesh_id(), Mesh{});
    model.owned_meshes.push_back(mesh_id);

    Texture& container_tex = textures.emplace_back(new_texture_id(), false, textures[diffuse_tex_idx].gl_id, TextureType::Diffuse, "brickwall.jpg");
    model.owned_textures.push_back(container_tex.tex_id);
    mesh.textures.emplace_back(container_tex);

    Texture& container_spec_tex = textures.emplace_back(new_texture_id(), false, textures[normal_tex_idx].gl_id, TextureType::Normal, "brickwall_normal.jpg");
    model.owned_textures.push_back(container_spec_tex.tex_id);
    mesh.textures.emplace_back(container_spec_tex);

    mesh.vertices = {
        // positions                   // normals                   // texture coords
        Vertex{ glm::vec3(-1.0f, 1.0f, 0.0f),  glm::vec3(0.0f, 0.0f, 1.0f), glm::vec2(0.0f, 1.0f) },
        Vertex{ glm::vec3(-1.0f, -1.0f, 0.0f), glm::vec3(0.0f, 0.0f, 1.0f), glm::vec2(0.0f, 0.0f) },
        Vertex{ glm::vec3(1.0f, 1.0f, 0.0f),   glm::vec3(0.0f, 0.0f, 1.0f), glm::vec2(1.0f, 1.0f) },
        Vertex{ glm::vec3(1.0f, -1.0f, 0.0f),  glm::vec3(0.0f, 0.0f, 1.0f), glm::vec2(1.0f, 0.0f) },
    };

    set_tangents_for_face(mesh.vertices, 0);

    mesh.indices = {
        0, 1, 2,
        3, 2, 1,
    };

    mesh.textures.emplace_back(textures[specular_tex_idx]);
    mesh.textures.emplace_back(textures[normal_tex_idx]);
    mesh.textures.emplace_back(textures[height_tex_id]);

    mesh.setup();

    renderer.add_instance_for_mesh(mesh_id, mesh);

    auto& [scene_id, scene] = *emplace(scenes, new_scene_id(), Scene{});
    prefabs.insert(std::lower_bound(prefabs.begin(), prefabs.end(), scene_id), scene_id);

    auto& [entity_id, entity] = scene.create_entity();
    scene.root_entities.push_back(entity_id);
    entity.name = "brick wall";
    emplace(scene.transforms, entity_id, Transform{});
    emplace(scene.entity_meshes, entity_id, mesh_id);

    model.scene_id = scene_id;
    emplace(models, new_model_id(), std::move(model));
}

void load_models() {
    create_default_textures();

    auto diffuse_model_tex_idx = 0;
    auto specular_model_tex_idx = 1;
    auto normal_model_tex_idx = 2;
    auto height_model_tex_idx = 3;

    //Assimp::DefaultLogger::create("", Assimp::Logger::VERBOSE);
    //Assimp::DefaultLogger::get()->attachStream(Assimp::LogStream::createDefaultStream(aiDefaultLogStream_STDERR), Assimp::Logger::NORMAL | Assimp::Logger::DEBUGGING | Assimp::Logger::VERBOSE);
    
    create_wood_plane_model(diffuse_model_tex_idx, specular_model_tex_idx, normal_model_tex_idx, height_model_tex_idx);
    create_container_cube_model(diffuse_model_tex_idx, specular_model_tex_idx, normal_model_tex_idx, height_model_tex_idx);
    create_brickwall_model(diffuse_model_tex_idx, specular_model_tex_idx, normal_model_tex_idx, height_model_tex_idx);
    add_textures(textures.begin(), textures.end());

    std::vector<std::string> paths{
        "backpack/backpack.obj",
        // "planet/planet.obj",
        // "rock/rock.obj",
    };
    add_models(paths);

    //Assimp::DefaultLogger::kill();
}

std::pair<scene_id_t, Scene>& create_model_display_scene() {
    auto& pair = *emplace(scenes, new_scene_id(), Scene{});
    auto& [scene_id, scene] = pair;

    auto instantiate_prefab = [&scene](size_t model_idx, Transform transform) {
        auto [it_scene_lower, it_scene_upper] = equal_range(scenes, models[model_idx].second.scene_id);
        ASSERT(std::distance(it_scene_lower, it_scene_upper) == 1, "scene expected.");
        const Scene& prefab_scene = it_scene_lower->second;

        auto& [entity_id, entity] = copy_entity(prefab_scene, prefab_scene.root_entities[0], scene);
        auto [it_transform_lower, it_transform_upper] = equal_range(scene.transforms, entity_id);
        ASSERT(std::distance(it_transform_lower, it_transform_upper) == 1, "transform expected.");
        it_transform_lower->second = std::move(transform);
    };

    const size_t plane_model_idx = 0;

    // add a plane
    instantiate_prefab(plane_model_idx, Transform{ glm::vec3(0.0f, -2.0f, 0.0f) });

    // add the other models above the plane
    for (size_t i = 1; i < models.size(); ++i) {
        instantiate_prefab(i, Transform{ glm::vec3{ ((float) i - models.size() / 2.0f) * 4.0f, 0.0f, 0.0f } });
    }

    // add a light above the entities shining down onto the plane
    {
        auto& [entity_id, entity] = scene.create_entity();
        scene.root_entities.push_back(entity_id);
        entity.name = "light";
        emplace(scene.transforms, entity_id, Transform{ glm::vec3(0.0f, 2.0f, 0.0f), glm::vec3{ 90.0f, 0.0f, 0.0f }, glm::vec3(0.2f) });
        emplace(scene.lights, entity_id, Light{});
    }

    return pair;
}

std::pair<scene_id_t, Scene>& create_hdr_test_scene() {
    auto& pair = *emplace(scenes, new_scene_id(), Scene{});
    auto& [scene_id, scene] = pair;

    const size_t plane_model_idx = 0;
    auto [it_plane_prefab_lower, it_plane_prefab_upper] = equal_range(scenes, models[plane_model_idx].second.scene_id);
    ASSERT(std::distance(it_plane_prefab_lower, it_plane_prefab_upper) == 1, "scene expected.");
    const Scene& plane_prefab_scene = it_plane_prefab_lower->second;

    auto add_plane = [&plane_prefab_scene, &scene](Transform&& transform) {
        auto& [entity_id, entity] = copy_entity(plane_prefab_scene, plane_prefab_scene.root_entities[0], scene);
        auto [it_transform_lower, it_transform_upper] = equal_range(scene.transforms, entity_id);
        ASSERT(std::distance(it_transform_lower, it_transform_upper) == 1, "transform expected.");
        it_transform_lower->second = transform;
    };

    float scale = 10.0f;
    add_plane(Transform{ glm::vec3(0.0f, 0.0f, -10.0f * scale), glm::vec3(90.0f, 0.0f, 0.0f) });
    add_plane(Transform{ glm::vec3(0.0f, 0.0f, 10.0f * scale), glm::vec3(-90.0f, 0.0f, 0.0f) });
    add_plane(Transform{ glm::vec3(-10.0f, 0.0f, 0.0f), glm::vec3(0.0f, 0.0f, -90.0f), glm::vec3(1.0f, 1.0f, scale) });
    add_plane(Transform{ glm::vec3(10.0f, 0.0f, 0.0f), glm::vec3(0.0f, 0.0f, 90.0f), glm::vec3(1.0f, 1.0f, scale) });
    add_plane(Transform{ glm::vec3(0.0f, -10.0f, 0.0f), glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(1.0f, 1.0f, scale) });
    add_plane(Transform{ glm::vec3(0.0f, 10.0f, 0.0f), glm::vec3(0.0f, 0.0f, 180.0f), glm::vec3(1.0f, 1.0f, scale) });

    auto add_light = [&scene](Transform&& transform) -> Light& {
        auto& [entity_id, entity] = scene.create_entity();
        scene.root_entities.push_back(entity_id);
        entity.name = "light";
        emplace(scene.transforms, entity_id, std::move(transform));
        auto& light = emplace(scene.lights, entity_id, Light{})->second;
        light.ambient_intensity = 0.0f;
        return light;
    };

    auto& white_light = add_light(Transform{ glm::vec3(0.0f, 0.0f, -95.0f), glm::vec3{ 0.0f, 0.0f, 0.0f }, glm::vec3(0.2f) });
    white_light.specular_intensity = white_light.diffuse_intensity = 200.0f;

    auto& green_light = add_light(Transform{ glm::vec3(3.0f, -9.0f, 60.0f), glm::vec3{ 0.0f, 0.0f, 0.0f }, glm::vec3(0.2f) });
    green_light.diffuse = green_light.ambient = green_light.specular = glm::vec3(0.1f, 1.0f, 0.1f);
    green_light.specular_intensity = green_light.diffuse_intensity = 0.1f;

    auto& red_light = add_light(Transform{ glm::vec3(-6.0f, -8.0f, 70.0f), glm::vec3{ 0.0f, 0.0f, 0.0f }, glm::vec3(0.2f) });
    red_light.diffuse = red_light.ambient = red_light.specular = glm::vec3(1.0f, 0.1f, 0.1f);
    red_light.specular_intensity = red_light.diffuse_intensity = 0.1f;

    auto& blue_light = add_light(Transform{ glm::vec3(4.0f, -9.0f, 80.0f), glm::vec3{ 0.0f, 0.0f, 0.0f }, glm::vec3(0.2f) });
    blue_light.diffuse = blue_light.ambient = blue_light.specular = glm::vec3(0.1f, 0.1f, 1.0f);
    blue_light.specular_intensity = blue_light.diffuse_intensity = 0.1f;

    return pair;
}
