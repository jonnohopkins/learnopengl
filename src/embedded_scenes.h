#pragma once

#include <utility>

#include "scene.h"

void load_models();
std::pair<scene_id_t, Scene>& create_model_display_scene();
std::pair<scene_id_t, Scene>& create_hdr_test_scene();
