#include "scene.h"
#include "globals.h"
#include "assets.h"
#include "physics_2d.h"

#include <functional>

scene_id_t last_scene_id{ 0 };

scene_id_t new_scene_id() {
    ASSERT(last_scene_id != std::numeric_limits<scene_id_t>::max(), "exhausted available ids.");
    ++last_scene_id;
    return last_scene_id;
}

entity_id_t Scene::new_entity_id() {
    ASSERT(last_entity_id != std::numeric_limits<entity_id_t>::max(), "exhausted available ids.");
    ++last_entity_id;
    return last_entity_id;
}

std::pair<entity_id_t, Entity>& Scene::create_entity() {
    return entities.emplace_back(new_entity_id(), Entity{});
}

std::pair<entity_id_t, Entity>& Scene::create_entity_with_parent(entity_id_t parent_id) {
    auto& pair = entities.emplace_back(new_entity_id(), Entity{});

    if (parent_id != 0) {
        pair.second.parent = parent_id;
        auto [it_parent_lower, it_parent_upper] = equal_range(entities, parent_id);
        ASSERT(std::distance(it_parent_lower, it_parent_upper) == 1, "entity expected.");
        it_parent_lower->second.children.push_back(pair.first);
    }
    else {
        root_entities.push_back(pair.first);
    }

    return pair;
}

void Scene::delete_entity(entity_id_t id) {
    std::function<void(std::vector<std::pair<entity_id_t, Entity>>::iterator)> recursive_remove = [this, &recursive_remove](std::vector<std::pair<entity_id_t, Entity>>::iterator it_entity) {
        entity_id_t entity_id = it_entity->first;
        Entity entity = std::move(it_entity->second);

        entities.erase(it_entity);

        auto remove_components = [entity_id](auto& components) {
            auto [it_lower, it_upper] = equal_range(components, entity_id);
            components.erase(it_lower, it_upper);
        };

        remove_components(transforms);
        remove_components(entity_meshes);
        remove_components(lights);
        remove_components(rigidbody_2ds);

        for (auto child_id : entity.children) {
            auto [it_lower, it_upper] = equal_range(entities, child_id);
            ASSERT(std::distance(it_lower, it_upper) == 1, "entity expected.");
            recursive_remove(it_lower);
        }
    };

    auto [it_lower, it_upper] = equal_range(entities, id);
    ASSERT(std::distance(it_lower, it_upper) <= 1, "entity id was not unique.");

    if (it_lower != it_upper) {
        // if the entity was a child, remove it from its parent.
        if (it_lower->second.parent != 0) {
            auto [it_parent_lower, it_parent_upper] = equal_range(entities, it_lower->second.parent);
            ASSERT(std::distance(it_parent_lower, it_parent_upper) == 1, "entity expected.");
            auto it_child = std::find(it_parent_lower->second.children.begin(), it_parent_lower->second.children.end(), id);
            ASSERT(it_child != it_parent_lower->second.children.end(), "entity must be in parent's list of children.");
            it_parent_lower->second.children.erase(it_child);
        }
        else {
            auto it_root = std::find(root_entities.begin(), root_entities.end(), id);
            ASSERT(it_root != root_entities.end(), "entity must be in root_entities.");
            root_entities.erase(it_root);
        }

        recursive_remove(it_lower);
    }
}

Transform to_local_space(const Scene& source_scene, entity_id_t source_id, Transform source_transform, const Scene& dest_scene, entity_id_t dest_id) {
    std::vector<std::pair<entity_id_t, size_t>> ancestors_id_and_idx; // back() is the root ancestor
    entity_id_t ancestor_id = source_id;

    while (ancestor_id != 0) {
        auto [it_ancestor_lower, it_ancestor_upper] = equal_range(source_scene.entities, ancestor_id);
        ASSERT(std::distance(it_ancestor_lower, it_ancestor_upper) == 1, "entity expected.");

        ancestors_id_and_idx.emplace_back(ancestor_id, std::distance(source_scene.entities.begin(), it_ancestor_lower));
        ancestor_id = it_ancestor_lower->second.parent;
    }

    std::vector<std::pair<entity_id_t, size_t>> new_ancestors_id_and_idx; // front() is the new parent
    std::vector<std::pair<entity_id_t, size_t>>::const_iterator it_common_ancestor = ancestors_id_and_idx.end();

    ancestor_id = dest_id;

    // will only find a common ancestor if the entities exist in the same scene
    if (&source_scene == &dest_scene) {
        while (ancestor_id != 0) {
            auto [it_ancestor_lower, it_ancestor_upper] = equal_range(dest_scene.entities, ancestor_id);
            ASSERT(std::distance(it_ancestor_lower, it_ancestor_upper) == 1, "entity expected.");

            it_common_ancestor = std::find_if(ancestors_id_and_idx.begin(), ancestors_id_and_idx.end(), [&ancestor_id](auto pair) { return pair.first == ancestor_id; });
            if (it_common_ancestor == ancestors_id_and_idx.end()) {
                new_ancestors_id_and_idx.emplace_back(ancestor_id, std::distance(dest_scene.entities.begin(), it_ancestor_lower));
                ancestor_id = it_ancestor_lower->second.parent;
            }
            else {
                // found a common ancestor between the new and old ancestors
                break;
            }
        }
    }
    else {
        while (ancestor_id != 0) {
            auto [it_ancestor_lower, it_ancestor_upper] = equal_range(dest_scene.entities, ancestor_id);
            ASSERT(std::distance(it_ancestor_lower, it_ancestor_upper) == 1, "entity expected.");

            new_ancestors_id_and_idx.emplace_back(ancestor_id, std::distance(dest_scene.entities.begin(), it_ancestor_lower));
            ancestor_id = it_ancestor_lower->second.parent;
        }
    }

    glm::mat4 model_mat = source_transform.GetMatrix();

    // up to the common ancestor:
    for (auto it_ancestor = ancestors_id_and_idx.begin(); it_ancestor != it_common_ancestor; ++it_ancestor) {
        auto [it_transform_lower, it_transform_upper] = equal_range(source_scene.transforms, it_ancestor->first);
        if (it_transform_lower != it_transform_upper) {
            glm::mat4 parent_model = it_transform_lower->second.GetMatrix();
            model_mat = parent_model * model_mat;
        }
    }

    // iterate backwards to descend the hierarchy:
    for (auto it_ancestor = new_ancestors_id_and_idx.rbegin(); it_ancestor != new_ancestors_id_and_idx.rend(); ++it_ancestor) {
        auto [it_transform_lower, it_transform_upper] = equal_range(dest_scene.transforms, it_ancestor->first);
        if (it_transform_lower != it_transform_upper) {
            glm::mat4 parent_model = glm::inverse(it_transform_lower->second.GetMatrix());
            model_mat = parent_model * model_mat;
        }
    }

    return Transform{ model_mat };
}

void reparent_entity(Scene& source_scene, entity_id_t entity_id, Scene& dest_scene, entity_id_t new_parent_id) {
    if (&source_scene == &dest_scene) {
        reparent_entity(source_scene, entity_id, new_parent_id);
    }
    else {
        // moving across scenes: need to delete entity + components and recreate them in the other scene.
        copy_entity(source_scene, entity_id, dest_scene, new_parent_id);
        source_scene.delete_entity(entity_id);
    }
}

void reparent_entity(Scene& scene, entity_id_t entity_id, entity_id_t new_parent_id) {
    auto [it_entity_lower, it_entity_upper] = equal_range(scene.entities, entity_id);
    ASSERT(std::distance(it_entity_lower, it_entity_upper) == 1, "entity expected.");

    // todo: do we want to support not requiring a transform for each entity?
    auto [it_transform_lower, it_transform_upper] = equal_range(scene.transforms, entity_id);
    ASSERT(std::distance(it_transform_lower, it_transform_upper) == 1, "transform expected.");
    Transform& transform = it_transform_lower->second;

    // convert from the old local space to the new local space:
    transform = to_local_space(scene, it_entity_lower->second.parent, transform, scene, new_parent_id);

    // remove entity from old parent's children
    if (it_entity_lower->second.parent != 0) {
        auto [it_old_parent_lower, it_old_parent_upper] = equal_range(scene.entities, it_entity_lower->second.parent);
        ASSERT(std::distance(it_old_parent_lower, it_old_parent_upper) == 1, "entity expected.");
        auto it_child = std::find(it_old_parent_lower->second.children.begin(), it_old_parent_lower->second.children.end(), entity_id);
        ASSERT(it_child != it_old_parent_lower->second.children.end(), "entity must be in parent's list of children.");
        it_old_parent_lower->second.children.erase(it_child);
    }
    else {
        auto it_root = std::find(scene.root_entities.begin(), scene.root_entities.end(), entity_id);
        ASSERT(it_root != scene.root_entities.end(), "entity must be in root_entities.");
        scene.root_entities.erase(it_root);
    }

    // add entity to new parent's children
    if (new_parent_id != 0) {
        auto [it_new_parent_lower, it_new_parent_upper] = equal_range(scene.entities, new_parent_id);
        ASSERT(std::distance(it_new_parent_lower, it_new_parent_upper) == 1, "entity expected.");
        it_new_parent_lower->second.children.push_back(entity_id);
    }
    else {
        scene.root_entities.push_back(entity_id);
    }

    it_entity_lower->second.parent = new_parent_id;
}

std::pair<entity_id_t, Entity>& copy_entity(const Scene& source_scene, entity_id_t source_id, Scene& dest_scene, entity_id_t dest_parent_id) {
    size_t num_source_transforms = 0;
    size_t num_source_entity_meshes = 0;
    size_t num_source_lights = 0;
    size_t num_source_rigidbody2ds = 0;
    size_t num_source_entities = 1;

    // counts components and child entities recursively.
    std::function<void(entity_id_t)> count_recursive = [&](entity_id_t source_id) {
        auto [it_source_lower, it_source_upper] = equal_range(source_scene.entities, source_id);
        ASSERT(std::distance(it_source_lower, it_source_upper) == 1, "entity expected.");

        auto count_components_for_source_entity = [source_id](const auto& components) -> size_t {
            auto [it_lower, it_upper] = equal_range(components, source_id);
            return std::distance(it_lower, it_upper);
        };

        num_source_transforms += count_components_for_source_entity(source_scene.transforms);
        num_source_entity_meshes += count_components_for_source_entity(source_scene.entity_meshes);
        num_source_lights += count_components_for_source_entity(source_scene.lights);
        num_source_rigidbody2ds += count_components_for_source_entity(source_scene.rigidbody_2ds);

        num_source_entities += it_source_lower->second.children.size();

        for (auto child_id : it_source_lower->second.children) {
            count_recursive(child_id);
        }
    };

    count_recursive(source_id);

    // copying entities and components won't invalidate iterators / references because we have reserved all that's necessary.
    dest_scene.transforms.reserve(dest_scene.transforms.size() + num_source_transforms);
    dest_scene.entity_meshes.reserve(dest_scene.entity_meshes.size() + num_source_entity_meshes);
    dest_scene.lights.reserve(dest_scene.lights.size() + num_source_lights);
    dest_scene.rigidbody_2ds.reserve(dest_scene.rigidbody_2ds.size() + num_source_rigidbody2ds);
    dest_scene.entities.reserve(dest_scene.entities.size() + num_source_entities);

    // copies the entity, its components and children recursively.
    std::function<std::pair<entity_id_t, Entity>& (entity_id_t)> recursive_copy = [&recursive_copy, &source_scene, &dest_scene](entity_id_t source_id) -> std::pair<entity_id_t, Entity>& {
        auto [it_source_lower, it_source_upper] = equal_range(source_scene.entities, source_id);
        ASSERT(std::distance(it_source_lower, it_source_upper) == 1, "entity expected.");
        const Entity& source_entity = it_source_lower->second;

        auto& pair = dest_scene.create_entity();
        auto& [dest_id, dest_entity] = pair;
        dest_entity.name = source_entity.name;

        auto copy_components = [source_id, dest_id](auto& source_components, auto& dest_components) {
            auto [it_lower, it_upper] = equal_range(source_components, source_id);

            for (auto it_component = it_lower; it_component != it_upper; ++it_component) {
                emplace(dest_components, dest_id, it_component->second);
            }
        };

        copy_components(source_scene.transforms, dest_scene.transforms);
        copy_components(source_scene.entity_meshes, dest_scene.entity_meshes);
        copy_components(source_scene.lights, dest_scene.lights);
        copy_components(source_scene.rigidbody_2ds, dest_scene.rigidbody_2ds);

        dest_entity.children.reserve(source_entity.children.size());

        for (auto source_child_id : source_entity.children) {
            auto& [dest_child_id, dest_child_entity] = recursive_copy(source_child_id);
            dest_child_entity.parent = dest_id;
            dest_entity.children.push_back(dest_child_id);
        }

        return pair;
    };

    auto& pair = recursive_copy(source_id);

    if (dest_parent_id == 0) {
        dest_scene.root_entities.push_back(pair.first);
    }
    else {
        pair.second.parent = dest_parent_id;

        auto [it_parent_lower, it_parent_upper] = equal_range(dest_scene.entities, dest_parent_id);
        ASSERT(std::distance(it_parent_lower, it_parent_upper) == 1, "entity expected.");
        it_parent_lower->second.children.push_back(pair.first);
    }
    
    if (&source_scene != &dest_scene) {
        auto [it_source_entity_lower, it_source_entity_upper] = equal_range(source_scene.entities, source_id);
        ASSERT(std::distance(it_source_entity_lower, it_source_entity_upper) == 1, "entity expected.");

        // todo: do we want to support not requiring a transform for each entity?
        auto [it_source_transform_lower, it_source_transform_upper] = equal_range(source_scene.transforms, source_id);
        ASSERT(std::distance(it_source_transform_lower, it_source_transform_upper) == 1, "transform expected.");
        Transform source_transform = it_source_transform_lower->second;

        auto [it_dest_transform_lower, it_dest_transform_upper] = equal_range(dest_scene.transforms, pair.first);
        ASSERT(std::distance(it_dest_transform_lower, it_dest_transform_upper) == 1, "transform expected.");
        Transform& dest_transform = it_dest_transform_lower->second;

        // convert from the old local space to the new local space:
        dest_transform = to_local_space(source_scene, it_source_entity_lower->second.parent, source_transform, dest_scene, dest_parent_id);
    }

    return pair;
}

bool is_active(const Scene& scene, entity_id_t entity_id) {
    const Scene* current_scene = &scene;

    while (entity_id != 0) {
        auto [it_entity_lower, it_entity_upper] = equal_range(current_scene->entities, entity_id);
        ASSERT(std::distance(it_entity_lower, it_entity_upper) == 1, "entity expected.");
        if (!it_entity_lower->second.is_active) {
            return false;
        }

        entity_id = it_entity_lower->second.parent;
    }

    return true;
}

bool world_space_matrix(const Scene& scene, entity_id_t entity_id, glm::mat4& model_mat, bool only_calc_matrix_when_active) {
    const Scene* current_scene = &scene;
    model_mat = glm::mat4(1.0f);

    bool active = true;
    while (entity_id != 0) {
        auto [it_entity_lower, it_entity_upper] = equal_range(current_scene->entities, entity_id);
        ASSERT(std::distance(it_entity_lower, it_entity_upper) == 1, "entity expected.");
        if (!it_entity_lower->second.is_active) {
            if (only_calc_matrix_when_active)
                return false;
            active = false;
        }

        auto [it_transform_lower, it_transform_upper] = equal_range(current_scene->transforms, entity_id);
        if (it_transform_lower != it_transform_upper) {
            glm::mat4 parent_model = it_transform_lower->second.GetMatrix();
            model_mat = parent_model * model_mat;
        }

        entity_id = it_entity_lower->second.parent;
    }

    return active;
}

void set_active(scene_id_t scene_id, entity_id_t entity_id, Entity& entity, bool is_active) {
    entity.is_active = is_active;
    physics_entity_set_activation(scene_id, entity_id, is_active);
}
