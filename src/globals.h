#pragma once

#ifdef WIN32
#define BREAKPOINT __debugbreak()
#else
#define BREAKPOINT
#endif

#define ASSERT(condition, explanation) if(!(condition)) { BREAKPOINT; }
#define FATAL_ASSERT(condition, explanation) if(!(condition)) { BREAKPOINT; abort(); }
