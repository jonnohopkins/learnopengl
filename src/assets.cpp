#include "assets.h"
#include "globals.h"
#include "model_loading.h"
#include "renderer.h"

#include <mutex>
#include <iostream>
#include <algorithm>

#include <glad/glad.h>
#define GLEW_STATIC
#include <GLFW/glfw3.h>

#include <stb_image.h>

std::vector<std::pair<scene_id_t, Scene>> scenes;
std::vector<scene_id_t> active_scenes;
std::vector<scene_id_t> prefabs;

struct TexturePreLoadInfo {
    tex_id_t tex_id;
    std::string path;
};

struct TexturePostLoadInfo {
    tex_id_t tex_id;
    std::string path;
    unsigned char* data; // has ownership
    int width;
    int height;
    int num_components;
};

bool texture_streaming_exiting = false;

std::mutex texture_load_mutex;
std::vector<TexturePreLoadInfo> textures_to_load;
std::vector<TexturePostLoadInfo> textures_loaded_pending;
std::vector<Texture> textures;
std::vector<Cubemap> cubemaps;
std::thread texture_load_thread;

std::vector<std::pair<model_id_t, Model>> models; // { model id, model }, sorted by model id

std::vector<std::pair<mesh_id_t, Mesh>> meshes;

void get_texture_formats(GLenum& internal_format, GLenum& data_format, int num_components, bool gamma) {
    if (num_components == 1) {
        internal_format = GL_RED;
        data_format = GL_RED;
    }
    else if (num_components == 3) {
        internal_format = gamma ? GL_SRGB : GL_RGB;
        data_format = GL_RGB;
    }
    else if (num_components == 4) {
        internal_format = gamma ? GL_SRGB_ALPHA : GL_RGBA;
        data_format = GL_RGBA;
    }
    else {
        throw std::runtime_error{ "unsupported number of components." };
    }
}

GLuint texture_from_load_info(const TexturePostLoadInfo& texture_info, bool gamma) {
    GLuint texture_id;
    glGenTextures(1, &texture_id);

    if (texture_info.data) {
        GLenum internal_format;
        GLenum data_format;
        get_texture_formats(internal_format, data_format, texture_info.num_components, gamma);

        glBindTexture(GL_TEXTURE_2D, texture_id);
        glTexImage2D(GL_TEXTURE_2D, 0, internal_format, texture_info.width, texture_info.height, 0, data_format, GL_UNSIGNED_BYTE, texture_info.data);
        glGenerateMipmap(GL_TEXTURE_2D);

        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

        stbi_image_free(texture_info.data);
    }
    else {
        std::cerr << "Texture failed to load: " << texture_info.path << std::endl;
        stbi_image_free(texture_info.data);
        throw;
    }

    return texture_id;
}

// executed on the texture streaming thread
void load_textures() {
    while (!texture_streaming_exiting) {
        if (texture_load_mutex.try_lock()) {
            if (!textures_to_load.empty()) {
                TexturePreLoadInfo pre_load_info = std::move(textures_to_load.front());
                textures_to_load.erase(textures_to_load.begin());

                TexturePostLoadInfo post_load_info;
                post_load_info.tex_id = pre_load_info.tex_id;
                post_load_info.path = std::move(pre_load_info.path);
                texture_load_mutex.unlock();

                std::cout << "[texture streaming] loading texture from file: " << post_load_info.path << "\n";
                post_load_info.data = stbi_load(post_load_info.path.c_str(), &post_load_info.width, &post_load_info.height, &post_load_info.num_components, 0);

                texture_load_mutex.lock();
                textures_loaded_pending.push_back(std::move(post_load_info));
            }

            texture_load_mutex.unlock();
        }
        else {
            using namespace std::chrono_literals;
            std::this_thread::sleep_for(1ms);
        }
    }
}

void cubemap_texture_from_load_info(GLuint texture_id, GLenum target, const TexturePostLoadInfo& texture_info, bool gamma) {
    if (texture_info.data) {
        GLenum internal_format;
        GLenum data_format;
        get_texture_formats(internal_format, data_format, texture_info.num_components, gamma);

        glBindTexture(GL_TEXTURE_CUBE_MAP, texture_id);
        glTexImage2D(target, 0, internal_format, texture_info.width, texture_info.height, 0, data_format, GL_UNSIGNED_BYTE, texture_info.data);

        stbi_image_free(texture_info.data);
    }
    else {
        std::cerr << "Texture failed to load: " << texture_info.path << std::endl;
        stbi_image_free(texture_info.data);
        throw;
    }
}

void update_textures_if_pending() {
    std::lock_guard lock{ texture_load_mutex };
    if (!textures_loaded_pending.empty()) {
        for (auto& texture_info : textures_loaded_pending) {
            auto it_texture = std::find_if(textures.begin(), textures.end(), [&texture_info](const Texture& texture) { return texture_info.tex_id == texture.tex_id; });
            if (it_texture != textures.end()) {
                std::cout << "[main] switching texture: " << texture_info.path << "\n";
                GLuint gl_id = texture_from_load_info(texture_info, gamma_enabled && it_texture->type == TextureType::Diffuse);
                it_texture->gl_id = gl_id;
                it_texture->is_loaded = true;

                for (auto& mesh_pair : meshes) {
                    for (auto& mesh_texture : mesh_pair.second.textures) {
                        if (mesh_texture.tex_id == texture_info.tex_id) {
                            mesh_texture.gl_id = gl_id;
                        }
                    }
                }

                continue;
            }

            auto it_cubemap = std::find_if(cubemaps.begin(), cubemaps.end(), [&texture_info](const Cubemap& cubemap) { return texture_info.tex_id == cubemap.tex_id; });
            if (it_cubemap != cubemaps.end()) {
                auto it_face = std::find(it_cubemap->face_paths.begin(), it_cubemap->face_paths.end(), texture_info.path);

                if (it_face != it_cubemap->face_paths.end()) {
                    static constexpr std::array<GLenum, 6> targets{
                        GL_TEXTURE_CUBE_MAP_POSITIVE_X,
                        GL_TEXTURE_CUBE_MAP_NEGATIVE_X,
                        GL_TEXTURE_CUBE_MAP_NEGATIVE_Y,
                        GL_TEXTURE_CUBE_MAP_POSITIVE_Y,
                        GL_TEXTURE_CUBE_MAP_POSITIVE_Z,
                        GL_TEXTURE_CUBE_MAP_NEGATIVE_Z,
                    };

                    auto face_idx = std::distance(it_cubemap->face_paths.begin(), it_face);
                    std::cout << "[main] switching (cubemap) texture: " << texture_info.path << "\n";
                    cubemap_texture_from_load_info(it_cubemap->gl_id, targets[face_idx], texture_info, gamma_enabled);
                    it_cubemap->is_loaded[face_idx] = true;
                }
                else {
                    std::cerr << "no matching cubemap face for: " << texture_info.tex_id << " - " << texture_info.path << std::endl;
                    stbi_image_free(texture_info.data);
                }

                continue;
            }

            std::cout << "no matching texture found to switch to. texture must have been deleted: " << texture_info.tex_id << " - " << texture_info.path << "\n";
        }

        textures_loaded_pending.clear();
    }
}

void add_textures(std::vector<Texture>::const_iterator begin, std::vector<Texture>::const_iterator end) {
    std::lock_guard lock{ texture_load_mutex };
    for (auto it = begin; it != end; ++it) {
        const Texture& texture = *it;
        if (!texture.is_loaded) {
            TexturePreLoadInfo& pre_load_info = textures_to_load.emplace_back();
            pre_load_info.path = texture.path;
            pre_load_info.tex_id = texture.tex_id;
        }
    }
}

template<typename T>
struct TextureIdComparator {
    inline bool operator ()(const T& lhs, tex_id_t rhs) { return lhs.tex_id < rhs; }
    inline bool operator ()(tex_id_t lhs, const T& rhs) { return lhs < rhs.tex_id; }
};

void remove_texture(tex_id_t tex_id) {

    auto [it_tex_lower, it_tex_upper] = std::equal_range(textures.begin(), textures.end(), tex_id, TextureIdComparator<Texture>{});

    for (auto it = it_tex_lower; it != it_tex_upper; ++it) {
        if (it->is_loaded)
            glDeleteTextures(1, &it->gl_id);
    }

    textures.erase(it_tex_lower, it_tex_upper);
}

void add_cubemap(const Cubemap& cubemap) {
    std::lock_guard lock{ texture_load_mutex };
    for (size_t i = 0; i < 6; ++i) {
        if (!cubemap.is_loaded[i]) {
            TexturePreLoadInfo& pre_load_info = textures_to_load.emplace_back();
            pre_load_info.path = cubemap.face_paths[i];
            pre_load_info.tex_id = cubemap.tex_id;
        }
    }
}

void remove_cubemap(tex_id_t tex_id) {
    auto [it_cubemap_lower, it_cubemap_upper] = std::equal_range(cubemaps.begin(), cubemaps.end(), tex_id, TextureIdComparator<Cubemap>{});

    for (auto it = it_cubemap_lower; it != it_cubemap_upper; ++it) {
        glDeleteTextures(1, &it->gl_id);
    }

    cubemaps.erase(it_cubemap_lower, it_cubemap_upper);
}

void add_models(const std::vector<std::string>& paths) {
    size_t textures_size = textures.size();
    size_t meshes_count = meshes.size();

    for (const std::string& path : paths) {
        if (std::find_if(models.begin(), models.end(), [&path](const std::pair<model_id_t, Model>& candidate) { return candidate.second.path == path; }) == models.end()) {
            try {
                create_model_from_file(path.c_str());
            }
            catch (const std::exception& exc) {
                std::cerr << "Unable to read model: " << path << '\n' << exc.what() << std::endl;
            }
        }
        else {
            std::cout << "Model already loaded: " << path << std::endl;
        }
    }

    add_textures(textures.begin() + textures_size, textures.end());

    for (auto it = meshes.begin() + meshes_count; it != meshes.end(); ++it) {
        renderer.add_instance_for_mesh(it->first, it->second);
    }
}

// todo: what does it mean to delete a model? what about all the things referencing it + it's assets?
void remove_models(const std::vector<model_id_t>& models_to_remove) {
    auto it_models = models.begin();

    for (auto model_id : models_to_remove) {
        auto [it_model_lower, it_model_upper] = std::equal_range(it_models, models.end(), model_id, IdComparator<model_id_t>{});

        if (it_model_lower != it_model_upper) {
            for (tex_id_t tex_id : it_model_lower->second.owned_textures) {
                remove_texture(tex_id);
            }

            for (mesh_id_t mesh_id : it_model_lower->second.owned_meshes) {
                remove_mesh(mesh_id);
            }

            renderer.remove_instances_for_mesh(it_model_lower->second.owned_meshes);

            auto [it_scene_lower, it_scene_upper] = equal_range(scenes, it_model_lower->second.scene_id);
            ASSERT(std::distance(it_scene_lower, it_scene_upper) == 1, "scene expected.");
            scenes.erase(it_scene_lower);

            // delete all entities referencing the model's prefab?

            it_models = models.erase(it_model_lower);
        }
    }
}

void remove_mesh(mesh_id_t mesh_id) {
    auto [it_lower, it_upper] = equal_range(meshes, mesh_id);
    ASSERT(std::distance(it_lower, it_upper) <= 1, "mesh id was not unique.");
    meshes.erase(it_lower, it_upper);
}

void init_assets() {
    texture_load_thread = std::thread{ []() { load_textures(); } };
}

void terminate_assets() {
    texture_streaming_exiting = true;
    std::cout << "exiting ..." << std::endl;

    texture_load_thread.join();

    auto num_textures_loaded = std::count_if(textures.begin(), textures.end(), [](const Texture& texture) { return texture.is_loaded; });
    size_t num_cubemap_textures_loaded = 0;
    for (const Cubemap& cubemap : cubemaps) {
        num_cubemap_textures_loaded += std::count(cubemap.is_loaded.begin(), cubemap.is_loaded.end(), true);
    }
    auto num_textures_in_load_states = textures_to_load.size() + textures_loaded_pending.size() + num_textures_loaded + num_cubemap_textures_loaded;
    ASSERT(textures.size() + cubemaps.size() * 6 == num_textures_in_load_states, "textures.size() != textures to load + textures loaded, pending + textures loaded, complete");

    for (const auto& texture_info : textures_loaded_pending) {
        stbi_image_free(texture_info.data);
    }

    for (const Texture& texture : textures) {
        if (texture.is_loaded)
            glDeleteTextures(1, &texture.gl_id);
    }

    textures.clear();

    for (const Cubemap& cubemap : cubemaps) {
        glDeleteTextures(1, &cubemap.gl_id);
    }

    cubemaps.clear();
}