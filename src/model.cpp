#include "model.h"
#include "globals.h"
#include "shader.h"

mesh_id_t last_mesh_id = 0;

mesh_id_t new_mesh_id() {
    ASSERT(last_mesh_id != std::numeric_limits<mesh_id_t>::max(), "exhausted available ids.");
    ++last_mesh_id;
    return last_mesh_id;
}

model_id_t last_model_id = 0;

model_id_t new_model_id() {
    ASSERT(last_model_id != std::numeric_limits<model_id_t>::max(), "exhausted available ids.");
    ++last_model_id;
    return last_model_id;
}

void Mesh::setup() {
    glGenVertexArrays(1, &VAO);
    glGenBuffers(1, &VBO);
    glGenBuffers(1, &EBO);

    glBindVertexArray(VAO);

    glBindBuffer(GL_ARRAY_BUFFER, VBO);
    glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(Vertex), &vertices[0], GL_STATIC_DRAW);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(unsigned int), &indices[0], GL_STATIC_DRAW);

    // vertex positions
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*) 0);
    // vertex normals
    glEnableVertexAttribArray(1);
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*) offsetof(Vertex, Normal));
    // vertex texture coords
    glEnableVertexAttribArray(2);
    glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*) offsetof(Vertex, TexCoords));
    // vertex tangent
    glEnableVertexAttribArray(3);
    glVertexAttribPointer(3, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*) offsetof(Vertex, Tangent));
    //// vertex bitangent
    //glEnableVertexAttribArray(4);
    //glVertexAttribPointer(4, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*) offsetof(Vertex, Bitangent));
    //// ids
    //glEnableVertexAttribArray(5);
    //glVertexAttribPointer(5, 4, GL_INT, sizeof(Vertex), (void*) offsetof(Vertex, BoneIds));
    //glEnableVertexAttribArray(6);
    //glVertexAttribPointer(6, 4, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*) offsetof(Vertex, Weights));

    //glBindVertexArray(0);
}

Mesh::Mesh(Mesh&& rhs) noexcept
    : vertices(std::move(rhs.vertices)),
    indices(std::move(rhs.indices)),
    textures(std::move(rhs.textures)),
    VAO(rhs.VAO),
    VBO(rhs.VBO),
    EBO(rhs.EBO) {
    rhs.VAO = 0;
    rhs.VBO = 0;
    rhs.EBO = 0;
}

Mesh& Mesh::operator =(Mesh&& rhs) noexcept {
    glDeleteVertexArrays(1, &VAO);
    glDeleteBuffers(1, &VBO);
    glDeleteBuffers(1, &EBO);

    vertices = std::move(rhs.vertices);
    indices = std::move(rhs.indices);
    textures = std::move(rhs.textures);
    VAO = rhs.VAO;
    rhs.VAO = 0;
    VBO = rhs.VBO;
    rhs.VBO = 0;
    EBO = rhs.EBO;
    rhs.EBO = 0;

    return *this;
}

Mesh::~Mesh() {
    glDeleteVertexArrays(1, &VAO);
    glDeleteBuffers(1, &VBO);
    glDeleteBuffers(1, &EBO);
}

void gl_draw(const Mesh& mesh, Shader& shader, size_t instance_count) {
    if (instance_count == 0)
        return;

    bool has_diffuse = false;
    bool has_specular = false;
    bool has_normal = false;
    bool has_height = false;

    size_t tex_idx = 0;
    for (; tex_idx < mesh.textures.size(); ++tex_idx) {
        auto texture_type = mesh.textures[tex_idx].type;
        if (texture_type == TextureType::Diffuse) {
            static constexpr int shader_diffuse_idx = 0;
            glActiveTexture(GL_TEXTURE0 + shader_diffuse_idx);
            shader.setInt("material.texture_diffuse1", shader_diffuse_idx);
            glBindTexture(GL_TEXTURE_2D, mesh.textures[tex_idx].gl_id);
            has_diffuse = true;
        }
        else if (texture_type == TextureType::Specular) {
            static constexpr int shader_specular_idx = 1;
            glActiveTexture(GL_TEXTURE0 + shader_specular_idx);
            shader.setInt("material.texture_specular1", shader_specular_idx);
            glBindTexture(GL_TEXTURE_2D, mesh.textures[tex_idx].gl_id);
            has_specular = true;
        }
        else if (texture_type == TextureType::Normal) {
            static constexpr int shader_normal_idx = 2;
            glActiveTexture(GL_TEXTURE0 + shader_normal_idx);
            shader.setInt("material.texture_normal1", shader_normal_idx);
            glBindTexture(GL_TEXTURE_2D, mesh.textures[tex_idx].gl_id);
            has_normal = true;
        }
        //else if (texture_type == TextureType::Height) {
        //    static constexpr int shader_height_idx = 3;
        //    glActiveTexture(GL_TEXTURE0 + shader_height_idx);
        //    shader.setInt("material.texture_height1", shader_height_idx);
        //    glBindTexture(GL_TEXTURE_2D, mesh.textures[tex_idx].gl_id);
        //    has_height = true;
        //}
    }

    glActiveTexture(GL_TEXTURE0);

    // draw mesh
    glBindVertexArray(mesh.VAO);
    if (instance_count > 1)
        glDrawElementsInstanced(GL_TRIANGLES, mesh.indices.size(), GL_UNSIGNED_INT, 0, instance_count);
    else
        glDrawElements(GL_TRIANGLES, mesh.indices.size(), GL_UNSIGNED_INT, 0);
}

void setup_uniform_attributes(GLuint instance_buffer, const Mesh& mesh) {
    glBindBuffer(GL_ARRAY_BUFFER, instance_buffer);

    glBindVertexArray(mesh.VAO);

    glEnableVertexAttribArray(12);
    glVertexAttribPointer(12, 4, GL_FLOAT, GL_FALSE, 4 * sizeof(glm::vec4), (void*) 0);
    glEnableVertexAttribArray(13);
    glVertexAttribPointer(13, 4, GL_FLOAT, GL_FALSE, 4 * sizeof(glm::vec4), (void*) (1 * sizeof(glm::vec4)));
    glEnableVertexAttribArray(14);
    glVertexAttribPointer(14, 4, GL_FLOAT, GL_FALSE, 4 * sizeof(glm::vec4), (void*) (2 * sizeof(glm::vec4)));
    glEnableVertexAttribArray(15);
    glVertexAttribPointer(15, 4, GL_FLOAT, GL_FALSE, 4 * sizeof(glm::vec4), (void*) (3 * sizeof(glm::vec4)));

    glVertexAttribDivisor(12, 1);
    glVertexAttribDivisor(13, 1);
    glVertexAttribDivisor(14, 1);
    glVertexAttribDivisor(15, 1);
}
