#pragma once

#include <box2d/b2_types.h>
#include <box2d/b2_api.h>

#include <stdarg.h>
#include <stdint.h>

typedef size_t entity_id_t;
typedef size_t scene_id_t;

// Tunable Constants

/// You can use this to change the length scale used by your game.
/// For example for inches you could use 39.4.
#define b2_lengthUnitsPerMeter 1.0f

/// The maximum number of vertices on a convex polygon. You cannot increase
/// this too much because b2BlockAllocator has a maximum object size.
#define b2_maxPolygonVertices	8

struct B2_API b2BodyUserData {
	scene_id_t scene_id;
	entity_id_t entity_id;
};

struct B2_API b2FixtureUserData {
	scene_id_t scene_id;
	entity_id_t entity_id;
};

struct B2_API b2JointUserData {
};

// Memory Allocation

/// Default allocation functions
B2_API void* b2Alloc_Default(int32 size);
B2_API void b2Free_Default(void* mem);

inline void* b2Alloc(int32 size) {
	return b2Alloc_Default(size);
}

inline void b2Free(void* mem) {
	b2Free_Default(mem);
}

/// Default logging function
B2_API void b2Log_Default(const char* string, va_list args);

inline void b2Log(const char* string, ...) {
	va_list args;
	va_start(args, string);
	b2Log_Default(string, args);
	va_end(args);
}