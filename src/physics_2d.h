#pragma once

#include <array>
#include <string>

#include <glm/glm.hpp>

#include "entity.h"

class b2Body;
class b2Fixture;
class b2Joint;
class b2World;

// there are also macros in b2_common.h that control global settings, but we have no reason to change those values.
class Physics2dGlobal {
public:
	glm::vec2 gravity{ 0.0f, -10.0f };
	int velocity_iterations{ 6 };
	int position_iterations{ 2 };
	int steps_per_second{ 60 };
	bool allow_sleep{ true };

	std::array<std::string, 16> collision_layer_names{
		"1", "2", "3", "4",
		"5", "6", "7", "8",
		"9", "10", "11", "12",
		"13", "14", "15", "16"
	};
	std::array<uint16_t, 16> collision_masks{
		0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF,
		0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF,
		0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF,
		0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF
	};

	bool playing{ false };
	bool reset{ false };
	bool advance{ false };

	bool debug_draw{ false };
	bool debug_draw_shapes{ true };
	bool debug_draw_joints{ true };
	bool debug_draw_aabb{ false };
	bool debug_draw_center_of_mass{ false };
};

extern Physics2dGlobal physics_2d_settings;

enum class Rigidbody2dType {
	Static,
	Kinematic,
	Dynamic,
};

class Rigidbody2d {
public:
	Rigidbody2dType type{ Rigidbody2dType::Static };
	bool is_continuous{ false };
	bool auto_calculate_mass{ true };
	float mass{ 0.0f };
	float linear_damping{ 0.0f };
	float angular_damping{ 0.0f };
	bool allow_sleep{ true };
	bool start_awake{ true };
	bool fixed_rotation{ false };
	float gravity_scale{ 1.0f };
};

class PhysicsShape2d {
public:
	uint8_t collision_idx{ 0 };
	bool is_sensor{ false };
	float friction{ 0.2f };
	float restitution{ 0.0f };
	float density{ 1.0f };
};

class PhysicsShape2dBox : public PhysicsShape2d {
public:
	glm::vec2 center{};
	glm::vec2 size{ 1.0f, 1.0f };
	float rotation_degrees{ 0.0f };
};

class PhysicsShape2dCircle : public PhysicsShape2d {
public:
	glm::vec2 center{};
	float radius{ 1.0f };
};

void init_physics();
void terminate_physics();
void update_physics();

void physics_entity_set_activation(scene_id_t scene_id, entity_id_t entity_id, bool is_active);

bool raycast_closest(const b2World& world, glm::vec2 from, glm::vec2 to, b2Fixture** fixture, glm::vec2* point, glm::vec2* normal, uint16_t maskBits = 0xFFFF);
