#include "renderer.h"
#include "globals.h"
#include "entity.h"
#include "model.h"
#include "assets.h"
#include "simple_log_timer.h"

#include <cmath>
#include <functional>
#include <iostream>
#include <filesystem>
#include <numeric>

#include <glad/glad.h>
#define GLEW_STATIC
#include <GLFW/glfw3.h>

#include <stb_image.h>

Renderer renderer;

// the Gaussian function is simply the normal distribution:
// https://en.wikipedia.org/wiki/Normal_distribution
// in the context of applying it as a blur the weights will be normalized anyway, so dividing by (stddev * sqrt(2 * PI)) is redundant.
double gaussian(double x, double mean, double stddev) {
    const double a = (x - mean) / stddev;
    return std::exp(-0.5 * a * a);
}

void Renderer::create_gaussian_blur_weights() {
    gaussian_blur_weights.resize(gaussian_blur_kernel_size, 0.0f);
    for (size_t i = 0; i < (gaussian_blur_kernel_size + 1) / 2; ++i)
        gaussian_blur_weights[i] = gaussian_blur_weights[gaussian_blur_kernel_size - 1 - i] = gaussian(i, (gaussian_blur_kernel_size - 1) / 2.0, gaussian_blur_std_dev);

    double weight_sum = 0.0;
    for (auto weight : gaussian_blur_weights)
        weight_sum += weight;

    for (size_t i = 0; i < (gaussian_blur_kernel_size + 1) / 2; ++i)
        gaussian_blur_weights[i] = gaussian_blur_weights[gaussian_blur_kernel_size - 1 - i] = gaussian_blur_weights[i] / weight_sum;
}

void Renderer::add_instance_for_mesh(mesh_id_t id, const Mesh& mesh) {
    MeshInstanceInfo instance_info;
    setup_uniform_attributes(instance_info.gl_buffer, mesh);
    emplace(instance_infos, id, std::move(instance_info));
}

void Renderer::remove_instances_for_mesh(const std::vector<mesh_id_t>& meshes_to_remove) {
    auto it_instances = renderer.instance_infos.begin();

    for (auto mesh_id : meshes_to_remove) {
        auto [it_instance_lower, it_instance_upper] = std::equal_range(it_instances, renderer.instance_infos.end(), mesh_id, IdComparator<mesh_id_t>{});

        if (it_instance_lower != it_instance_upper) {
            it_instances = renderer.instance_infos.erase(it_instance_lower);
        }
    }
}

void Renderer::update_instance_buffer() {
    size_t num_meshes_in_active_scenes = 0;

    std::vector<std::pair<scene_id_t, const Scene*>> active_scene_ptrs;
    active_scene_ptrs.reserve(active_scenes.size());

    for (scene_id_t scene_id : active_scenes) {
        auto [it_scene_lower, it_scene_upper] = equal_range(scenes, scene_id);
        ASSERT(std::distance(it_scene_lower, it_scene_upper) == 1, "scene expected.");
        const Scene& active_scene = it_scene_lower->second;
        num_meshes_in_active_scenes += active_scene.entity_meshes.size();
        active_scene_ptrs.emplace_back(scene_id, &active_scene);
    }

    std::vector<glm::mat4> mats;
    mats.resize(num_meshes_in_active_scenes);

    size_t next_mat_idx = 0;

    for (auto& [ mesh_id, instance_info ] : instance_infos) {
        size_t count = 0;
        for (auto [ scene_id, scene ] : active_scene_ptrs) {
            for (auto [entity_id, entity_mesh_id] : scene->entity_meshes) {
                glm::mat4 model_mat;
                if (entity_mesh_id == mesh_id && world_space_matrix(*scene, entity_id, model_mat)) {
                    mats[next_mat_idx] = model_mat;
                    ++next_mat_idx;
                    ++count;
                }
            }
        }

        instance_info.count = count;

        glBindBuffer(GL_ARRAY_BUFFER, instance_info.gl_buffer);
        glBufferData(GL_ARRAY_BUFFER, instance_info.count * sizeof(glm::mat4), &mats[next_mat_idx - count], GL_STATIC_DRAW);
    }

    ASSERT(next_mat_idx <= num_meshes_in_active_scenes, "wrote past end of array.");
}

void Renderer::create_skybox_cubemap() {
    skybox_cubemap_idx = cubemaps.size();
    Cubemap& skybox_cubemap = cubemaps.emplace_back();
    skybox_cubemap.tex_id = new_texture_id();
    skybox_cubemap.face_paths = {
        "skybox/right.jpg",
        "skybox/left.jpg",
        "skybox/top.jpg",
        "skybox/bottom.jpg",
        "skybox/front.jpg",
        "skybox/back.jpg"
    };

    glGenTextures(1, &skybox_cubemap.gl_id);
    glBindTexture(GL_TEXTURE_CUBE_MAP, skybox_cubemap.gl_id);

    unsigned char black_color[3] = { 255, 255, 255 };

    for (unsigned int i = 0; i < 6; i++) {
        glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, GL_RGB, 1, 1, 0, GL_RGB, GL_UNSIGNED_BYTE, black_color);
    }

    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);

    add_cubemap(cubemaps[0]);
}

void Renderer::render_lights(const std::vector<std::pair<scene_id_t, const Scene*>>& active_scene_ptrs, const glm::mat4& view, const glm::mat4& projection) {
    shader_unlit.use();
    shader_unlit.setMat4("view", view);
    shader_unlit.setMat4("projection", projection);

    glBindVertexArray(light_vao);

    for (auto [scene_id, scene] : active_scene_ptrs) {
        for (auto& [entity_id, light] : scene->lights) {
            glm::mat4 model_mat;
            if (!world_space_matrix(*scene, entity_id, model_mat))
                continue;

            shader_unlit.setVec3("objectColor", light.diffuse);
            shader_unlit.setMat4("model", model_mat);

            glDrawElements(GL_TRIANGLES, 36, GL_UNSIGNED_INT, nullptr);
        }
    }
}

glm::mat4 Renderer::camera_projection() {
    return glm::perspective(glm::radians(camera_fovy), (float) width / (float) height, camera_near, camera_far);
}

glm::mat4 Renderer::shadow_directional_projection() {
    return glm::ortho(-10.0f, 10.0f, -10.0f, 10.0f, shadow_near_plane, shadow_far_plane);
}

glm::mat4 Renderer::shadow_point_projection() {
    return glm::perspective(glm::radians(90.0f), 1.0f, shadow_near_plane, shadow_far_plane);
}

// only renders a mesh if the entity has a transform + mesh.
void Renderer::render_shadows_non_instanced(const Scene& scene, const glm::mat4& parent_model, entity_id_t entity_id, glm::mat4 direction_light_space_matrix, glm::vec3 light_world_pos) {
    auto [it_entity_lower, it_entity_upper] = equal_range(scene.entities, entity_id);
    ASSERT(std::distance(it_entity_lower, it_entity_upper) == 1, "entity expected.");
    const Entity& entity = it_entity_lower->second;
    if (entity.is_active) {
        auto [it_transform_lower, it_transform_upper] = equal_range(scene.transforms, entity_id);
        auto [it_entity_mesh_lower, it_entity_mesh_upper] = equal_range(scene.entity_meshes, entity_id);

        glm::mat4 model_mat = parent_model;
        if (it_transform_lower != it_transform_upper) {
            const Transform& transform = it_transform_lower->second;
            model_mat *= transform.GetMatrix();
        }

        for (auto it_entity_mesh = it_entity_mesh_lower; it_entity_mesh != it_entity_mesh_upper; ++it_entity_mesh) {
            auto [it_mesh_lower, it_mesh_upper] = equal_range(meshes, it_entity_mesh->second);
            ASSERT(std::distance(it_mesh_lower, it_mesh_upper) <= 1, "mesh id was not unique.");
            if (it_mesh_lower != it_mesh_upper) {
                const Mesh& mesh = it_mesh_lower->second;

                if (direction_light.first != 0) {
                    glBindFramebuffer(GL_FRAMEBUFFER, shadow_direction_fb);

                    shader_shadow_direction.use();
                    shader_shadow_direction.setMat4("model", model_mat);
                    shader_shadow_direction.setMat4("lightSpaceMatrix", direction_light_space_matrix);

                    gl_draw(mesh, shader_shadow_direction, 1);
                }

                if (point_light.first != 0) {
                    glBindFramebuffer(GL_FRAMEBUFFER, shadow_point_fb);

                    auto light_projection = shadow_point_projection();

                    shader_shadow_point.use();
                    shader_shadow_point.setMat4("model", model_mat);
                    shader_shadow_point.setVec3("lightPos", light_world_pos);
                    shader_shadow_point.setFloat("far_plane", shadow_far_plane);
                    shader_shadow_point.setMat4("lightSpaceMatrices[0]", light_projection * glm::lookAt(light_world_pos, light_world_pos + -world_right, -world_up));
                    shader_shadow_point.setMat4("lightSpaceMatrices[1]", light_projection * glm::lookAt(light_world_pos, light_world_pos + world_right, -world_up));
                    shader_shadow_point.setMat4("lightSpaceMatrices[2]", light_projection * glm::lookAt(light_world_pos, light_world_pos + world_up, world_forward));
                    shader_shadow_point.setMat4("lightSpaceMatrices[3]", light_projection * glm::lookAt(light_world_pos, light_world_pos + -world_up, -world_forward));
                    shader_shadow_point.setMat4("lightSpaceMatrices[4]", light_projection * glm::lookAt(light_world_pos, light_world_pos + world_forward, -world_up));
                    shader_shadow_point.setMat4("lightSpaceMatrices[5]", light_projection * glm::lookAt(light_world_pos, light_world_pos + -world_forward, -world_up));

                    gl_draw(mesh, shader_shadow_point, 1);
                }
            }
        }

        for (auto child_id : entity.children)
            render_shadows_non_instanced(scene, model_mat, child_id, direction_light_space_matrix, light_world_pos);
    }
};

void Renderer::bind_lighting_info_to_shader(const std::vector<std::pair<scene_id_t, const Scene*>>& active_scene_ptrs, Shader& shader) {
    size_t point_light_idx = 0;
    size_t direct_light_idx = 0;
    size_t spot_light_idx = 0;

    for (auto [scene_id, scene] : active_scene_ptrs) {
        for (const auto& [entity_id, light] : scene->lights) {
            glm::mat4 model_mat;
            if (!world_space_matrix(*scene, entity_id, model_mat))
                continue;

            glm::vec3 light_forward = model_mat * glm::vec4(world_forward, 0.0f);
            glm::vec3 light_position = model_mat * glm::vec4(glm::vec3(0.0f), 1.0f);

            char str_buffer[64];

            // directional lighting:
            if (light.type == LightType::Directional) {
                sprintf(str_buffer, "directionLights[%i].direction", (int) direct_light_idx);
                shader.setVec3(str_buffer, light_forward);

                // common lighting:
                sprintf(str_buffer, "directionLights[%i].ambient", (int) direct_light_idx);
                shader.setVec3(str_buffer, light.ambient * light.ambient_intensity);
                sprintf(str_buffer, "directionLights[%i].diffuse", (int) direct_light_idx);
                shader.setVec3(str_buffer, light.diffuse * light.diffuse_intensity);
                sprintf(str_buffer, "directionLights[%i].specular", (int) direct_light_idx);
                shader.setVec3(str_buffer, light.specular * light.specular_intensity);

                ++direct_light_idx;
            }
            else if (light.type == LightType::Spot) {
                sprintf(str_buffer, "spotLights[%i].position", (int) spot_light_idx);
                shader.setVec3(str_buffer, light_position);
                sprintf(str_buffer, "spotLights[%i].direction", (int) spot_light_idx);
                shader.setVec3(str_buffer, light_forward);
                sprintf(str_buffer, "spotLights[%i].cutOff", (int) spot_light_idx);
                shader.setFloat(str_buffer, glm::cos(glm::radians(light.cutoff)));
                sprintf(str_buffer, "spotLights[%i].outerCutOff", (int) spot_light_idx);
                shader.setFloat(str_buffer, glm::cos(glm::radians(light.outer_cutoff)));

                // common lighting:
                sprintf(str_buffer, "spotLights[%i].ambient", (int) spot_light_idx);
                shader.setVec3(str_buffer, light.ambient * light.ambient_intensity);
                sprintf(str_buffer, "spotLights[%i].diffuse", (int) spot_light_idx);
                shader.setVec3(str_buffer, light.diffuse * light.diffuse_intensity);
                sprintf(str_buffer, "spotLights[%i].specular", (int) spot_light_idx);
                shader.setVec3(str_buffer, light.specular * light.specular_intensity);

                ++spot_light_idx;
            }
            else {
                // point lighting:
                sprintf(str_buffer, "pointLights[%i].position", (int) point_light_idx);
                shader.setVec3(str_buffer, light_position);
                sprintf(str_buffer, "pointLights[%i].constant", (int) point_light_idx);
                shader.setFloat(str_buffer, light.constant);
                sprintf(str_buffer, "pointLights[%i].linear", (int) point_light_idx);
                shader.setFloat(str_buffer, light.linear);
                sprintf(str_buffer, "pointLights[%i].quadratic", (int) point_light_idx);
                shader.setFloat(str_buffer, light.quadratic);

                if (point_light.first == scene_id && point_light.second == entity_id) {
                    shader.setUint("shadowPointLightIdx", point_light_idx);
                }

                // common lighting:
                sprintf(str_buffer, "pointLights[%i].ambient", (int) point_light_idx);
                shader.setVec3(str_buffer, light.ambient * light.ambient_intensity);
                sprintf(str_buffer, "pointLights[%i].diffuse", (int) point_light_idx);
                shader.setVec3(str_buffer, light.diffuse * light.diffuse_intensity);
                sprintf(str_buffer, "pointLights[%i].specular", (int) point_light_idx);
                shader.setVec3(str_buffer, light.specular * light.specular_intensity);

                ++point_light_idx;
            }
        }
    }

    shader.setUint("num_direction_lights", direct_light_idx);
    shader.setUint("num_point_lights", point_light_idx);
    shader.setUint("num_spot_lights", spot_light_idx);
}

void Renderer::bind_shadow_info_to_shader(Shader& shader, glm::mat4 direction_light_space_matrix) {
    const int directional_shadow_texture_unit = 3;
    shader.setInt("directionalShadowDepth", directional_shadow_texture_unit);

    glActiveTexture(GL_TEXTURE0 + directional_shadow_texture_unit);
    if (direction_light.first != 0) {
        glBindTexture(GL_TEXTURE_2D, shadow_depth_texture);

        shader.setMat4("lightSpaceMatrix", direction_light_space_matrix);
    }
    else {
        glBindTexture(GL_TEXTURE_2D, 0);
    }

    const int point_shadow_texture_unit = 4;
    shader.setInt("pointShadowDepth", point_shadow_texture_unit);
    shader.setFloat("shadow_far_plane", shadow_far_plane);

    glActiveTexture(GL_TEXTURE0 + point_shadow_texture_unit);
    if (point_light.first != 0) {
        glBindTexture(GL_TEXTURE_CUBE_MAP, shadow_depth_cubemap);
    }
    else {
        glBindTexture(GL_TEXTURE_CUBE_MAP, 0);
    }
}

// only renders a mesh if the entity has a transform + mesh.
void Renderer::render_entity_non_instanced(const std::vector<std::pair<scene_id_t, const Scene*>>& active_scene_ptrs, const Scene& scene, const glm::mat4& parent_model, const glm::mat4& view, const glm::mat4& projection, entity_id_t entity_id, ShaderView shader_view, glm::mat4 direction_light_space_matrix) {
    auto [it_entity_lower, it_entity_upper] = equal_range(scene.entities, entity_id);
    ASSERT(std::distance(it_entity_lower, it_entity_upper) == 1, "entity expected.");
    const Entity& entity = it_entity_lower->second;
    if (entity.is_active) {
        auto [it_transform_lower, it_transform_upper] = equal_range(scene.transforms, entity_id);
        auto [it_entity_mesh_lower, it_entity_mesh_upper] = equal_range(scene.entity_meshes, entity_id);

        glm::mat4 model_mat = parent_model;
        if (it_transform_lower != it_transform_upper) {
            const Transform& transform = it_transform_lower->second;
            model_mat *= transform.GetMatrix();
        }

        for (auto it_entity_mesh = it_entity_mesh_lower; it_entity_mesh != it_entity_mesh_upper; ++it_entity_mesh) {
            auto [it_mesh_lower, it_mesh_upper] = equal_range(meshes, it_entity_mesh->second);
            ASSERT(std::distance(it_mesh_lower, it_mesh_upper) <= 1, "mesh id was not unique.");
            if (it_mesh_lower != it_mesh_upper) {
                const Mesh& mesh = it_mesh_lower->second;

                if (shader_view == ShaderView::Unlit || shader_view == ShaderView::Debug_Normal_Local || shader_view == ShaderView::Debug_Normal_World || shader_view == ShaderView::Deferred_Shading || shader_view == ShaderView::Debug_Wireframe) {
                    auto& shader = (shader_view == ShaderView::Debug_Normal_Local) ? shader_debug_normal_local
                        : (shader_view == ShaderView::Debug_Normal_World) ? shader_debug_normal_world
                        : (shader_view == ShaderView::Debug_Wireframe) ? shader_debug_wireframe
                        : (shader_view == ShaderView::Deferred_Shading) ? shader_deferred_g_buffer
                        : shader_model;
                    shader.use();
                    shader.setMat4("model", model_mat);
                    shader.setMat4("view", view);
                    shader.setMat4("projection", projection);

                    gl_draw(mesh, shader, 1);
                }
                else if (shader_view == ShaderView::Debug_Depth) {
                    shader_depth.use();
                    shader_depth.setMat4("model", model_mat);
                    shader_depth.setMat4("view", view);
                    shader_depth.setMat4("projection", projection);
                    shader_depth.setFloat("near", camera_near);
                    shader_depth.setFloat("far", camera_far);

                    gl_draw(mesh, shader_depth, 1);
                }
                else if (shader_view == ShaderView::Lit_Phong || shader_view == ShaderView::Lit_Blinn_Phong || shader_view == ShaderView::Lit_Blinn_Phong_Shadow) {
                    auto& lit_shader = (shader_view == ShaderView::Lit_Phong) ? shader_phong
                        : (shader_view == ShaderView::Lit_Blinn_Phong) ? shader_blinn_phong
                        : shader_blinn_phong_shadow;

                    lit_shader.use();
                    lit_shader.setMat4("model", model_mat);
                    lit_shader.setMat4("view", view);
                    lit_shader.setMat4("projection", projection);
                    lit_shader.setVec3("viewPos", camera.transform.Position);

                    lit_shader.setFloat("material.shininess", 32.0f);

                    if (does_view_have_shadows()) {
                        bind_shadow_info_to_shader(lit_shader, direction_light_space_matrix);
                    }

                    bind_lighting_info_to_shader(active_scene_ptrs, lit_shader);

                    gl_draw(mesh, lit_shader, 1);
                }
                else if (shader_view == ShaderView::Reflect || shader_view == ShaderView::Refract) {
                    auto& shader = (shader_view == ShaderView::Reflect) ? shader_reflect : shader_refract;
                    shader.use();
                    shader.setMat4("model", model_mat);
                    shader.setMat4("view", view);
                    shader.setMat4("projection", projection);
                    shader.setVec3("viewPos", camera.transform.Position);
                    shader.setInt("skybox", 0);

                    glActiveTexture(GL_TEXTURE0);
                    glBindTexture(GL_TEXTURE_CUBE_MAP, cubemaps[skybox_cubemap_idx].gl_id);

                    gl_draw(mesh, shader, 1);
                }
                else if (shader_view == ShaderView::Override_Vertex_Normals) {
                    shader_normals.use();
                    shader_normals.setMat4("model", model_mat);
                    shader_normals.setMat4("view", view);
                    shader_normals.setMat4("projection", projection);

                    gl_draw(mesh, shader_normals, 1);
                }
            }
        }

        for (auto child_id : entity.children)
            render_entity_non_instanced(active_scene_ptrs, scene, model_mat, view, projection, child_id, shader_view, direction_light_space_matrix);
    }
};

void Renderer::create_framebuffers(bool is_update) {
    auto assert_framebuffer_complete = []() {
        if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE) {
            ASSERT(false, "framebuffer is not complete!")
                throw;
        }
    };

    if (is_update)
        glDeleteTextures(1, &ms_color_texture);
    glGenTextures(1, &ms_color_texture);

    glBindFramebuffer(GL_FRAMEBUFFER, ms_fb);

    {
        glBindTexture(GL_TEXTURE_2D_MULTISAMPLE, ms_color_texture);
        glTexImage2DMultisample(GL_TEXTURE_2D_MULTISAMPLE, msaa_samples, GL_RGB16F, width, height, GL_TRUE);
        //glBindTexture(GL_TEXTURE_2D_MULTISAMPLE, 0);
    }

    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D_MULTISAMPLE, ms_color_texture, 0);

    {
        glBindRenderbuffer(GL_RENDERBUFFER, ms_rbo);
        glRenderbufferStorageMultisample(GL_RENDERBUFFER, msaa_samples, GL_DEPTH24_STENCIL8, width, height);
        //glBindRenderbuffer(GL_RENDERBUFFER, 0);
    }

    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT, GL_RENDERBUFFER, ms_rbo);

    assert_framebuffer_complete();

    auto setup_fb = [this, &assert_framebuffer_complete](GLuint fb, GLuint color_texture) {
        glBindFramebuffer(GL_FRAMEBUFFER, fb);

        glBindTexture(GL_TEXTURE_2D, color_texture);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB16F, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, NULL);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

        glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, color_texture, 0);

        assert_framebuffer_complete();
    };

    if (is_update)
        glDeleteTextures(1, &post_process_color_texture);
    glGenTextures(1, &post_process_color_texture);

    setup_fb(post_process_fb, post_process_color_texture);

    if (is_update)
        glDeleteTextures(1, &tonemapped_color_texture);
    glGenTextures(1, &tonemapped_color_texture);

    setup_fb(tonemapped_fb, tonemapped_color_texture);

    if (is_update)
        glDeleteTextures(2 * bloom_max_num_blurs, bloom_color_textures);
    glGenTextures(2 * bloom_max_num_blurs, bloom_color_textures);
    for (size_t j = 0; j < bloom_max_num_blurs; ++j) {
        // half the texture size each iteration, starting at full resolution.
        for (size_t i = 0; i < 2; ++i) {
            glBindFramebuffer(GL_FRAMEBUFFER, bloom_fbs[j * 2 + i]);
            glBindTexture(GL_TEXTURE_2D, bloom_color_textures[j * 2 + i]);
            glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB16F, width / (1 << j), height / (1 << j), 0, GL_RGB, GL_UNSIGNED_BYTE, NULL);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR); // not GL_NEAREST
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
            glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, bloom_color_textures[j * 2 + i], 0);

            assert_framebuffer_complete();
        }
    }
};

void Renderer::create_gbuffer(bool is_update) {
    auto assert_framebuffer_complete = []() {
        if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE) {
            ASSERT(false, "framebuffer is not complete!")
                throw;
        }
    };

    if (is_update)
        glDeleteFramebuffers(1, &g_buffer);
    glGenFramebuffers(1, &g_buffer);
    glBindFramebuffer(GL_FRAMEBUFFER, g_buffer);

    if (is_update)
        glDeleteTextures(1, &g_position);
    glGenTextures(1, &g_position);

    glBindTexture(GL_TEXTURE_2D, g_position);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA16F, width, height, 0, GL_RGBA, GL_FLOAT, NULL);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, g_position, 0);

    if (is_update)
        glDeleteTextures(1, &g_normal);
    glGenTextures(1, &g_normal);

    glBindTexture(GL_TEXTURE_2D, g_normal);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA16F, width, height, 0, GL_RGBA, GL_FLOAT, NULL);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT1, GL_TEXTURE_2D, g_normal, 0);

    if (is_update)
        glDeleteTextures(1, &g_color_spec);
    glGenTextures(1, &g_color_spec);

    glBindTexture(GL_TEXTURE_2D, g_color_spec);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA16F, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, NULL);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT2, GL_TEXTURE_2D, g_color_spec, 0);

    GLenum attachments[3] = { GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1, GL_COLOR_ATTACHMENT2 };
    glDrawBuffers(3, attachments);

    if (is_update)
        glDeleteRenderbuffers(1, &gbuffer_depth_rbo);
    glGenRenderbuffers(1, &gbuffer_depth_rbo);

    glBindRenderbuffer(GL_RENDERBUFFER, gbuffer_depth_rbo);
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT, width, height);

    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, gbuffer_depth_rbo);

    assert_framebuffer_complete();

    if (is_update)
        glDeleteTextures(1, &deferred_color_texture);
    glGenTextures(1, &deferred_color_texture);

    glBindFramebuffer(GL_FRAMEBUFFER, deferred_fb);

    {
        glBindTexture(GL_TEXTURE_2D, deferred_color_texture);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB16F, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, NULL);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    }

    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, deferred_color_texture, 0);

    {
        glBindRenderbuffer(GL_RENDERBUFFER, deferred_rbo);
        glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH24_STENCIL8, width, height);
    }

    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT, GL_RENDERBUFFER, deferred_rbo);

    assert_framebuffer_complete();

    glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

const float cube_vertices[] = {
    -0.5f, -0.5f, -0.5f,
     0.5f, -0.5f, -0.5f,
     0.5f,  0.5f, -0.5f,
    -0.5f,  0.5f, -0.5f,
    -0.5f, -0.5f,  0.5f,
     0.5f, -0.5f,  0.5f,
     0.5f,  0.5f,  0.5f,
    -0.5f,  0.5f,  0.5f
};

void Renderer::create_gl_cube_and_light() {
    unsigned int cube_indices[] = {
        // back face
        1, 0, 2,
        3, 2, 0,
        // front face
        4, 5, 6,
        6, 7, 4,
        // left face
        7, 3, 0,
        0, 4, 7,
        // right face
        2, 6, 1,
        5, 1, 6,
        // bottom face
        0, 1, 5,
        5, 4, 0,
        // top face
        2, 3, 6,
        7, 6, 3,
    };

    glBindVertexArray(light_vao);
    glBindBuffer(GL_ARRAY_BUFFER, cube_vbo);
    glBufferData(GL_ARRAY_BUFFER, sizeof(cube_vertices), cube_vertices, GL_STATIC_DRAW);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, cube_ebo);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(cube_indices), cube_indices, GL_STATIC_DRAW);

    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void*) 0);
    glEnableVertexAttribArray(0);

    //glBindBuffer(GL_ARRAY_BUFFER, 0);
}

void Renderer::create_gl_skybox() {
    // cubemap faces face inwards.
    const unsigned int cubemap_indices[] = {
        // back face
        3, 0, 1,
        1, 2, 3,
        // front face
        4, 0, 3,
        3, 7, 4,
        // left face
        1, 5, 6,
        6, 2, 1,
        // right face
        4, 7, 6,
        6, 5, 4,
        // bottom face
        3, 2, 6,
        6, 7, 3,
        // top face
        0, 4, 1,
        1, 4, 5,
    };

    glBindVertexArray(skybox_vao);
    glBindBuffer(GL_ARRAY_BUFFER, cube_vbo);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, skybox_ebo);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(cubemap_indices), cubemap_indices, GL_STATIC_DRAW);

    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void*) 0);
    glEnableVertexAttribArray(0);

    //glBindBuffer(GL_ARRAY_BUFFER, 0);
}

void Renderer::render_skybox(const glm::mat4& view, const glm::mat4& projection) {
    glDepthFunc(GL_LEQUAL);

    glm::mat4 translation_independent_view = glm::mat4(glm::mat3(view));

    shader_skybox.use();
    shader_skybox.setMat4("view", translation_independent_view);
    shader_skybox.setMat4("projection", projection);
    shader_skybox.setInt("skybox", 0);

    glBindVertexArray(skybox_vao);
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_CUBE_MAP, cubemaps[skybox_cubemap_idx].gl_id);
    glDrawElements(GL_TRIANGLES, 36, GL_UNSIGNED_INT, nullptr);
    //glBindVertexArray(0);

    glDepthFunc(GL_LESS);
}

// in NDC
void gl_screen(GLuint vao, GLuint vbo, float x_min, float x_max, float y_min, float y_max) {
    float screen_vertices[] = {
        // positions  // texcoords
        x_min, y_min, 0.0f, 0.0f, // bottom-left
        x_max, y_min, 1.0f, 0.0f, // bottom-right
        x_max, y_max, 1.0f, 1.0f, // top-right
        x_min, y_max, 0.0f, 1.0f, // top-left
        x_min, y_min, 0.0f, 0.0f, // bottom-left
        x_max, y_max, 1.0f, 1.0f  // top-right
    };

    glBindVertexArray(vao);
    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    glBufferData(GL_ARRAY_BUFFER, sizeof(screen_vertices), screen_vertices, GL_STATIC_DRAW);

    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 4 * sizeof(float), (void*) 0);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 4 * sizeof(float), (void*) (2 * sizeof(float)));
    glEnableVertexAttribArray(1);

    //glBindBuffer(GL_ARRAY_BUFFER, 0);
}

void Renderer::create_gl_screen() {
    gl_screen(screen_vao, screen_vbo, -1.0f, 1.0f, -1.0f, 1.0f);
}

void Renderer::create_shadow_framebuffer_with_tex2d() {
    glBindFramebuffer(GL_FRAMEBUFFER, shadow_direction_fb);

    {
        glBindTexture(GL_TEXTURE_2D, shadow_depth_texture);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT, shadow_size, shadow_size, 0, GL_DEPTH_COMPONENT, GL_FLOAT, NULL);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);
        float border_color[] = { 1.0f, 1.0f, 1.0f, 1.0f };
        glTexParameterfv(GL_TEXTURE_2D, GL_TEXTURE_BORDER_COLOR, border_color);
    }

    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, shadow_depth_texture, 0);
    glDrawBuffer(GL_NONE);
    glReadBuffer(GL_NONE);

    if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE) {
        ASSERT(false, "framebuffer is not complete!")
            throw;
    }
}

void Renderer::create_shadow_framebuffer_with_cubemap() {
    glBindFramebuffer(GL_FRAMEBUFFER, shadow_point_fb);

    {
        glBindTexture(GL_TEXTURE_CUBE_MAP, shadow_depth_cubemap);

        for (unsigned int i = 0; i < 6; i++)
            glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, GL_DEPTH_COMPONENT, shadow_size, shadow_size, 0, GL_DEPTH_COMPONENT, GL_FLOAT, NULL);

        glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
    }

    glFramebufferTexture(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, shadow_depth_cubemap, 0);
    glDrawBuffer(GL_NONE);
    glReadBuffer(GL_NONE);

    if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE) {
        ASSERT(false, "framebuffer is not complete!")
            throw;
    }
}

struct ShaderFileInfo {
    std::filesystem::file_time_type vert_time;
    const char* vert_path;
    std::filesystem::file_time_type frag_time;
    const char* frag_path;
    std::filesystem::file_time_type geom_time;
    const char* geom_path;
    Shader* shader;
};

ShaderFileInfo create_shader_file_info(Shader& shader, const char* vert_path, const char* frag_path, const char* geom_path = nullptr) {
    auto shader_files_exists = std::filesystem::exists(vert_path) && std::filesystem::exists(frag_path) && (!geom_path || std::filesystem::exists(geom_path));
    if (!shader_files_exists)
        throw std::runtime_error{ "a shader file doesn't exist." };

    return ShaderFileInfo{
        std::filesystem::last_write_time(vert_path),
        vert_path,
        std::filesystem::last_write_time(frag_path),
        frag_path,
        geom_path ? std::filesystem::last_write_time(geom_path) : std::filesystem::file_time_type{},
        geom_path,
        &shader
    };
}

#ifdef DEBUG
const float shader_file_check_wait = 0.5f;
float last_shader_file_check = 0.0f;
std::vector<ShaderFileInfo> shader_last_write_times;

void update_shaders_if_modified(float time) {
    last_shader_file_check = time;

    for (auto& shader_file_info : shader_last_write_times) {
        if (std::filesystem::last_write_time(shader_file_info.vert_path) > shader_file_info.vert_time ||
            std::filesystem::last_write_time(shader_file_info.frag_path) > shader_file_info.frag_time ||
            (shader_file_info.geom_path && std::filesystem::last_write_time(shader_file_info.geom_path) > shader_file_info.geom_time)) {
            shader_file_info.vert_time = std::filesystem::last_write_time(shader_file_info.vert_path);
            shader_file_info.frag_time = std::filesystem::last_write_time(shader_file_info.frag_path);
            if (shader_file_info.geom_path)
                shader_file_info.geom_time = std::filesystem::last_write_time(shader_file_info.geom_path);

            try {
                shader_file_info.shader->create_regular(shader_file_info.vert_path, shader_file_info.frag_path, shader_file_info.geom_path);

                std::cout << "Reloading renderer: " << shader_file_info.vert_path << ", " << shader_file_info.frag_path;
                if (shader_file_info.geom_path)
                    std::cout << ", " << shader_file_info.geom_path << std::endl;
                else
                    std::cout << std::endl;
            }
            catch (const std::exception& exc) {
                // timestamp already set, don't attempt to reload the same shader until another modification.

                std::cerr << "Unable to reload renderer: " << shader_file_info.vert_path << ", " << shader_file_info.frag_path;
                if (shader_file_info.geom_path)
                    std::cerr << ", " << shader_file_info.geom_path << '\n';
                else
                    std::cerr << '\n';
                std::cerr << exc.what() << std::endl;
            }
        }
    }
}
#endif

void Renderer::setup_shaders(float time) {
    std::vector<ShaderFileInfo> shaders_to_load;
    shaders_to_load.push_back(create_shader_file_info(shader_model, "shaders/model.vert", "shaders/model.frag"));
    shaders_to_load.push_back(create_shader_file_info(shader_model_instanced, "shaders/model_instanced.vert", "shaders/model.frag"));
    shaders_to_load.push_back(create_shader_file_info(shader_phong, "shaders/lit_phong.vert", "shaders/lit_phong.frag"));
    shaders_to_load.push_back(create_shader_file_info(shader_blinn_phong, "shaders/lit_phong.vert", "shaders/lit_blinn_phong.frag"));
    shaders_to_load.push_back(create_shader_file_info(shader_blinn_phong_shadow, "shaders/lit_phong_shadow.vert", "shaders/lit_blinn_phong_shadow.frag"));
    shaders_to_load.push_back(create_shader_file_info(shader_unlit, "shaders/unlit.vert", "shaders/unlit.frag"));
    shaders_to_load.push_back(create_shader_file_info(shader_reflect, "shaders/reflect.vert", "shaders/reflect.frag"));
    shaders_to_load.push_back(create_shader_file_info(shader_refract, "shaders/refract.vert", "shaders/refract.frag"));
    shaders_to_load.push_back(create_shader_file_info(shader_depth, "shaders/depth.vert", "shaders/depth.frag"));
    shaders_to_load.push_back(create_shader_file_info(shader_normals, "shaders/normals.vert", "shaders/normals.frag", "shaders/normals.geom"));
    shaders_to_load.push_back(create_shader_file_info(shader_skybox, "shaders/skybox.vert", "shaders/skybox.frag"));
    shaders_to_load.push_back(create_shader_file_info(shader_bloom_downsample, "shaders/screen.vert", "shaders/bloom_downsample.frag"));
    shaders_to_load.push_back(create_shader_file_info(shader_bloom_bright_only, "shaders/screen.vert", "shaders/bloom_bright_only.frag"));
    shaders_to_load.push_back(create_shader_file_info(shader_bloom_gaussian_blur, "shaders/screen.vert", "shaders/bloom_gaussian_blur.frag"));
    shaders_to_load.push_back(create_shader_file_info(shader_bloom_combine_textures, "shaders/screen.vert", "shaders/bloom_combine_textures.frag"));
    shaders_to_load.push_back(create_shader_file_info(shader_screen, "shaders/screen.vert", "shaders/screen.frag"));
    shaders_to_load.push_back(create_shader_file_info(shader_shadow_direction, "shaders/shadow_direction.vert", "shaders/shadow_direction.frag"));
    shaders_to_load.push_back(create_shader_file_info(shader_shadow_point, "shaders/shadow_point_cubemap.vert", "shaders/shadow_point_cubemap.frag", "shaders/shadow_point_cubemap.geom"));
    shaders_to_load.push_back(create_shader_file_info(shader_debug_normal_local, "shaders/normals_debug.vert", "shaders/normals_local.frag"));
    shaders_to_load.push_back(create_shader_file_info(shader_debug_normal_world, "shaders/normals_debug.vert", "shaders/normals_world.frag"));
    shaders_to_load.push_back(create_shader_file_info(shader_deferred_g_buffer, "shaders/g_buffer.vert", "shaders/g_buffer.frag"));
    shaders_to_load.push_back(create_shader_file_info(shader_deferred_shading, "shaders/deferred_shading.vert", "shaders/deferred_shading.frag"));
    shaders_to_load.push_back(create_shader_file_info(shader_debug_wireframe, "shaders/debug_wireframe.vert", "shaders/debug_wireframe.frag"));

    simple_log_timer shader_compile_timer{ "shader compilation" };
    for (ShaderFileInfo& shader_info : shaders_to_load)
        shader_info.shader->create_regular(shader_info.vert_path, shader_info.frag_path, shader_info.geom_path);
    shader_compile_timer.end();

#ifdef DEBUG
    shader_last_write_times = std::move(shaders_to_load);
    last_shader_file_check = time;
#endif
}

void Renderer::update_shadow_light_info() {
    direction_light = std::pair<scene_id_t, entity_id_t>{};
    point_light = std::pair<scene_id_t, entity_id_t>{};

    for (auto it_scene = active_scenes.begin(); it_scene != active_scenes.end() && (direction_light.first == 0 || point_light.first == 0); ++it_scene) {
        auto [it_scene_lower, it_scene_upper] = equal_range(scenes, *it_scene);
        ASSERT(std::distance(it_scene_lower, it_scene_upper) == 1, "scene expected.");
        const Scene& active_scene = it_scene_lower->second;

        if (direction_light.first == 0) {
            auto it_direction_light = std::find_if(active_scene.lights.begin(), active_scene.lights.end(), [&active_scene](const std::pair<entity_id_t, Light>& pair) { return pair.second.type == LightType::Directional && is_active(active_scene, pair.first); });

            if (it_direction_light != active_scene.lights.end()) {
                direction_light = std::make_pair(*it_scene, it_direction_light->first);
            }
        }

        if (point_light.first == 0) {
            auto it_point_light = std::find_if(active_scene.lights.begin(), active_scene.lights.end(), [&active_scene](const std::pair<entity_id_t, Light>& pair) { return pair.second.type == LightType::Point && is_active(active_scene, pair.first); });

            if (it_point_light != active_scene.lights.end()) {
                point_light = std::make_pair(*it_scene, it_point_light->first);
            }
        }
    }
}

void Renderer::render_bloom(GLuint source_framebuffer, GLuint dest_framebuffer) {
    glBindFramebuffer(GL_READ_FRAMEBUFFER, source_framebuffer);
    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, bloom_fbs[0]);
    glBlitFramebuffer(0, 0, width, height, 0, 0, width, height, GL_COLOR_BUFFER_BIT, GL_NEAREST);

    shader_bloom_bright_only.use();
    shader_bloom_bright_only.setInt("imageTexture", 0);
    shader_bloom_bright_only.setFloat("brightnessCutoff", bloom_brightness_cutoff / exposure);
    shader_bloom_bright_only.setFloat("brightnessFull", bloom_brightness_full / exposure);

    auto copy_bright = [&](size_t from_pp_buffer, size_t to_pp_buffer, float scale) {
        shader_bloom_bright_only.setFloat("texCoordsScale", scale);

        glBindFramebuffer(GL_FRAMEBUFFER, bloom_fbs[to_pp_buffer]);

        glBindVertexArray(screen_vao);
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, bloom_color_textures[from_pp_buffer]);
        glDrawArrays(GL_TRIANGLES, 0, 6);
    };

    shader_bloom_gaussian_blur.use();
    shader_bloom_gaussian_blur.setInt("imageTexture", 0);
    shader_bloom_gaussian_blur.setInt("kernelSize", gaussian_blur_kernel_size);
    char str_buffer[16];
    for (size_t i = 0; i < gaussian_blur_kernel_size; ++i) {
        sprintf(str_buffer, "weight[%i]", (int) i);
        shader_bloom_gaussian_blur.setFloat(str_buffer, gaussian_blur_weights[i]);
    }

    auto blur = [&](size_t pp_buffer, bool isHorizontal) {
        shader_bloom_gaussian_blur.setBool("isHorizontal", isHorizontal);

        glBindFramebuffer(GL_FRAMEBUFFER, bloom_fbs[pp_buffer + (isHorizontal ? 0 : 1)]);

        glBindVertexArray(screen_vao);
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, bloom_color_textures[pp_buffer + (isHorizontal ? 1 : 0)]);
        glDrawArrays(GL_TRIANGLES, 0, 6);
    };

    auto combine_original_with_bloom = [&](float factor1, float factor2) {
        glBindFramebuffer(GL_FRAMEBUFFER, dest_framebuffer);

        shader_bloom_combine_textures.use();
        shader_bloom_combine_textures.setInt("image1Texture", 0);
        shader_bloom_combine_textures.setInt("image2Texture", 1);
        shader_bloom_combine_textures.setFloat("firstTextureFactor", factor1);
        shader_bloom_combine_textures.setFloat("secondTextureFactor", factor2);

        glBindVertexArray(screen_vao);
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, bloom_color_textures[0]);
        glActiveTexture(GL_TEXTURE1);
        glBindTexture(GL_TEXTURE_2D, bloom_color_textures[1]);
        glDrawArrays(GL_TRIANGLES, 0, 6);
    };

    if (bloom_blur == BloomOption::RepeatedGaussian) {
        shader_bloom_bright_only.use();
        copy_bright(0, 1, 1.0f);

        shader_bloom_gaussian_blur.use();
        for (size_t i = 0; i < bloom_powers.size(); ++i) {
            blur(0, true);
            blur(0, false);
        }

        glBindFramebuffer(GL_READ_FRAMEBUFFER, source_framebuffer);
        glBindFramebuffer(GL_DRAW_FRAMEBUFFER, bloom_fbs[0]);
        glBlitFramebuffer(0, 0, width, height, 0, 0, width, height, GL_COLOR_BUFFER_BIT, GL_NEAREST);

        combine_original_with_bloom(1.0f, bloom_intensity_factor);
    }
    else {
        shader_bloom_downsample.use();
        shader_bloom_downsample.setInt("imageTexture", 0);
        glBindVertexArray(screen_vao);

        auto downsample = [&](size_t from_pp_buffer, size_t to_pp_buffer) {
            glBindFramebuffer(GL_FRAMEBUFFER, bloom_fbs[to_pp_buffer]);

            glBindVertexArray(screen_vao);
            glActiveTexture(GL_TEXTURE0);
            glBindTexture(GL_TEXTURE_2D, bloom_color_textures[from_pp_buffer]);
            glDrawArrays(GL_TRIANGLES, 0, 6);
        };

        shader_bloom_bright_only.use();
        copy_bright(0, 1, 1.0f);

        for (size_t i = 0; i < bloom_powers.size() - 1; ++i) {
            // preferred over the below
            //glBindFramebuffer(GL_READ_FRAMEBUFFER, pp_framebuffers[i * 2]);
            //glBindFramebuffer(GL_DRAW_FRAMEBUFFER, pp_framebuffers[(i + 1) * 2]);
            //glBlitFramebuffer(0, 0, width / (1 << i), height / (1 << i), 0, 0, width / (1 << (i + 1)), height / (1 << (i + 1)), GL_COLOR_BUFFER_BIT, GL_LINEAR);

            //glBindFramebuffer(GL_READ_FRAMEBUFFER, pp_framebuffers[0]);
            //glBindFramebuffer(GL_DRAW_FRAMEBUFFER, pp_framebuffers[(i + 1) * 2]);
            //glBlitFramebuffer(0, 0, width, height, 0, 0, width / (1 << (i + 1)), height / (1 << (i + 1)), GL_COLOR_BUFFER_BIT, GL_LINEAR);

            glViewport(0, 0, width / (1 << (i + 1)), height / (1 << (i + 1)));
            shader_bloom_downsample.use();
            downsample(i * 2, (i + 1) * 2);
            shader_bloom_bright_only.use();
            copy_bright((i + 1) * 2, (i + 1) * 2 + 1, 1.0f);
        }

        shader_bloom_gaussian_blur.use();
        for (size_t i = 0; i < bloom_powers.size(); ++i) {
            glViewport(0, 0, width / (1 << i), height / (1 << i));
            blur(i * 2, true);
            blur(i * 2, false);
        }

        if (bloom_powers.size() > 1) {
            shader_bloom_combine_textures.use();
            shader_bloom_combine_textures.setInt("image1Texture", 0);
            shader_bloom_combine_textures.setInt("image2Texture", 1);

            auto combine_textures = [&](size_t pp_texture1, float power1, size_t pp_texture2, float power2, size_t to_pp_buffer) {
                shader_bloom_combine_textures.setFloat("firstTextureFactor", power1);
                shader_bloom_combine_textures.setFloat("secondTextureFactor", power2);

                glBindFramebuffer(GL_FRAMEBUFFER, bloom_fbs[to_pp_buffer]);

                glBindVertexArray(screen_vao);
                glActiveTexture(GL_TEXTURE0);
                glBindTexture(GL_TEXTURE_2D, bloom_color_textures[pp_texture1]);
                glActiveTexture(GL_TEXTURE1);
                glBindTexture(GL_TEXTURE_2D, bloom_color_textures[pp_texture2]);
                glDrawArrays(GL_TRIANGLES, 0, 6);
            };

            const size_t bloom_num_blurs = bloom_powers.size();
            // using a combined bloom power of 1 across all resolutions ensures that the added intensity of the multi size/resolution bloom is the same as the simple repeated bloom.
            float bloom_powers_sum = std::accumulate(bloom_powers.begin(), bloom_powers.end(), 0.0f);
            if (bloom_powers_sum == 0.0f)
                bloom_powers_sum = 1.0f;

            shader_bloom_combine_textures.use();
            glViewport(0, 0, width / (1 << (bloom_num_blurs - 2)), height / (1 << (bloom_num_blurs - 2)));
            combine_textures(bloom_num_blurs * 2 - 1, *bloom_powers.rbegin() / bloom_powers_sum, bloom_num_blurs * 2 - 3, *(bloom_powers.rbegin() + 1) / bloom_powers_sum, bloom_num_blurs * 2 - 4);

            auto it_bloom_powers = bloom_powers.rbegin() + 2;
            for (size_t i = bloom_num_blurs - 2; i > 0; --i) {
                glViewport(0, 0, width / (1 << (i - 1)), height / (1 << (i - 1)));
                combine_textures(i * 2, 1.0f, (i - 1) * 2 + 1, *it_bloom_powers / bloom_powers_sum, (i - 1) * 2);
                ++it_bloom_powers;
            }

            glBindFramebuffer(GL_READ_FRAMEBUFFER, source_framebuffer);
            glBindFramebuffer(GL_DRAW_FRAMEBUFFER, bloom_fbs[1]);
            glBlitFramebuffer(0, 0, width, height, 0, 0, width, height, GL_COLOR_BUFFER_BIT, GL_NEAREST);

            combine_original_with_bloom(bloom_intensity_factor, 1.0f);

            glViewport(0, 0, width, height);
        }
        else {
            glBindFramebuffer(GL_READ_FRAMEBUFFER, source_framebuffer);
            glBindFramebuffer(GL_DRAW_FRAMEBUFFER, bloom_fbs[0]);
            glBlitFramebuffer(0, 0, width, height, 0, 0, width, height, GL_COLOR_BUFFER_BIT, GL_NEAREST);
        }
    }
}

void Renderer::set_msaa_samples(size_t samples) {
    msaa_samples = samples;
    create_framebuffers(true);

    if (msaa_samples <= 1)
        glDisable(GL_MULTISAMPLE);
    else
        glEnable(GL_MULTISAMPLE);

    glfwWindowHint(GLFW_SAMPLES, msaa_samples);
}

void Renderer::set_vsync(bool enabled) {
    vsync_enabled = enabled;
    glfwSwapInterval((int) vsync_enabled);
}

void Renderer::post_scene_setup() {
    // setup after we have created our scene:
    update_instance_buffer();

    update_shadow_light_info();
}

void Renderer::render_frame() {
    double time = glfwGetTime();
    delta_time = time - last_frame;
    last_frame = time;

    std::vector<std::pair<scene_id_t, const Scene*>> active_scene_ptrs;
    active_scene_ptrs.reserve(active_scenes.size());

    for (scene_id_t scene_id : active_scenes) {
        auto [it_scene_lower, it_scene_upper] = equal_range(scenes, scene_id);
        ASSERT(std::distance(it_scene_lower, it_scene_upper) == 1, "scene expected.");
        active_scene_ptrs.emplace_back(scene_id, &it_scene_lower->second);
    }

#ifdef DEBUG
    if (last_shader_file_check + shader_file_check_wait < time)
        update_shaders_if_modified(time);
#endif DEBUG

    glEnable(GL_DEPTH_TEST);

    // shadow-depth rendering

    glm::mat4 direction_light_space_matrix{};
    if (does_view_have_shadows() &&
        (direction_light.first != 0 || point_light.first != 0)) {
        glViewport(0, 0, shadow_size, shadow_size);
        glBindFramebuffer(GL_FRAMEBUFFER, shadow_direction_fb);
        glClear(GL_DEPTH_BUFFER_BIT);
        glBindFramebuffer(GL_FRAMEBUFFER, shadow_point_fb);
        glClear(GL_DEPTH_BUFFER_BIT);

        if (direction_light.first != 0) {
            auto [it_direction_scene_lower, it_direction_scene_upper] = equal_range(scenes, direction_light.first);
            ASSERT(std::distance(it_direction_scene_lower, it_direction_scene_upper) == 1, "scene expected.");

            glm::mat4 direction_light_mat;
            world_space_matrix(it_direction_scene_lower->second, direction_light.second, direction_light_mat);
            Transform direction_light_transform{ direction_light_mat };
            direction_light_transform.Scale = glm::vec3{ 1.0f };
            direction_light_space_matrix = shadow_directional_projection() * direction_light_transform.GetViewMatrix();
        }

        glm::vec3 light_world_pos{};
        if (point_light.first != 0) {
            auto [it_point_scene_lower, it_point_scene_upper] = equal_range(scenes, point_light.first);
            ASSERT(std::distance(it_point_scene_lower, it_point_scene_upper) == 1, "scene expected.");

            glm::mat4 point_light_mat;
            world_space_matrix(it_point_scene_lower->second, point_light.second, point_light_mat);
            light_world_pos = position_from_affine_transformation(point_light_mat);
        }

        for (auto [ active_scene_id, active_scene ] : active_scene_ptrs) {
            for (entity_id_t root_id : active_scene->root_entities) {
                render_shadows_non_instanced(*active_scene, glm::mat4(1.0f), root_id, direction_light_space_matrix, light_world_pos);
            }
        }

        glViewport(0, 0, width, height);
    }

    // mesh rendering
    glm::mat4 projection = camera_projection();
    glm::mat4 view = camera.transform.GetViewMatrix();

    if (shader_view == ShaderView::Debug_Wireframe)
        glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
    else
        glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

    if (shader_view == ShaderView::Deferred_Shading) {
        // todo: MSAA won't work with g-buffer as is.
        glBindFramebuffer(GL_FRAMEBUFFER, g_buffer);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    }
    else {
        glBindFramebuffer(GL_FRAMEBUFFER, ms_fb);
        glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    }

    if (shader_view == ShaderView::Unlit_Instanced) {
        shader_model_instanced.use();
        shader_model_instanced.setMat4("view", view);
        shader_model_instanced.setMat4("projection", projection);

        for (const auto& [ mesh_id, instance_info ] : instance_infos) {
            auto [it_mesh_lower, it_mesh_upper] = equal_range(meshes, mesh_id);
            ASSERT(std::distance(it_mesh_lower, it_mesh_upper) <= 1, "mesh id was not unique.");
            gl_draw(it_mesh_lower->second, shader_model_instanced, instance_info.count);
        }
    }
    else {
        for (auto [active_scene_id, active_scene] : active_scene_ptrs) {
            for (entity_id_t root_id : active_scene->root_entities) {
                render_entity_non_instanced(active_scene_ptrs, *active_scene, glm::mat4(1.0f), view, projection, root_id, shader_view, direction_light_space_matrix);
            }
        }
    }

    if (shader_view == ShaderView::Debug_Wireframe)
        glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

    GLuint source_framebuffer;

    if (shader_view == ShaderView::Deferred_Shading) {
        glBindFramebuffer(GL_FRAMEBUFFER, deferred_fb);
        glDisable(GL_DEPTH_TEST);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        shader_deferred_shading.use();

        shader_deferred_shading.setInt("gPosition", 0);
        shader_deferred_shading.setInt("gNormal", 1);
        shader_deferred_shading.setInt("gAlbedoSpec", 2);
        shader_deferred_shading.setVec3("viewPos", camera.transform.Position);

        shader_deferred_shading.setFloat("material.shininess", 32.0f);

        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, g_position);
        glActiveTexture(GL_TEXTURE1);
        glBindTexture(GL_TEXTURE_2D, g_normal);
        glActiveTexture(GL_TEXTURE2);
        glBindTexture(GL_TEXTURE_2D, g_color_spec);

        // todo: deferred shading doesn't render directional shadows? why?
        bind_shadow_info_to_shader(shader_deferred_shading, direction_light_space_matrix);

        bind_lighting_info_to_shader(active_scene_ptrs, shader_deferred_shading);

        glBindVertexArray(screen_vao);
        glDrawArrays(GL_TRIANGLES, 0, 6);

        glEnable(GL_DEPTH_TEST);

        // copy depth from g_buffer to deferred_fb
        glBindFramebuffer(GL_READ_FRAMEBUFFER, g_buffer);
        glBindFramebuffer(GL_DRAW_FRAMEBUFFER, deferred_fb);
        glBlitFramebuffer(0, 0, width, height, 0, 0, width, height, GL_DEPTH_BUFFER_BIT, GL_NEAREST);

        source_framebuffer = deferred_fb;
    }
    else {
        source_framebuffer = ms_fb;
    }

    if (is_view_lit())
        render_lights(active_scene_ptrs, view, projection);

    if (use_skybox)
        render_skybox(view, projection);

    if (use_normals_rendering) {
        for (auto [active_scene_id, active_scene] : active_scene_ptrs) {
            for (entity_id_t root_id : active_scene->root_entities) {
                render_entity_non_instanced(active_scene_ptrs, *active_scene, glm::mat4(1.0f), view, projection, root_id, ShaderView::Override_Vertex_Normals, direction_light_space_matrix);
            }
        }
    }

    // screen rendering
    {
        glDisable(GL_DEPTH_TEST);

        if (use_postprocess && !use_normals_rendering && !is_view_debug() && (bloom_blur == BloomOption::RepeatedGaussian || bloom_blur == BloomOption::MultiSizeGaussian)) {
            render_bloom(source_framebuffer, post_process_fb);
        }
        else {
            glBindFramebuffer(GL_READ_FRAMEBUFFER, source_framebuffer);
            glBindFramebuffer(GL_DRAW_FRAMEBUFFER, post_process_fb);
            glBlitFramebuffer(0, 0, width, height, 0, 0, width, height, GL_COLOR_BUFFER_BIT, GL_NEAREST);
        }

        shader_screen.use();
        shader_screen.setInt("imageTexture", 0);
        shader_screen.setFloat("exposure", exposure);
        glBindVertexArray(screen_vao);
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, post_process_color_texture);

        if (use_game_view)
            glBindFramebuffer(GL_FRAMEBUFFER, tonemapped_fb);
        else
            glBindFramebuffer(GL_FRAMEBUFFER, 0);
        glDrawArrays(GL_TRIANGLES, 0, 6);
    }
}

void Renderer::update(bool mesh_buffer_requires_update, bool light_shadow_info_requires_update) {
    if (mesh_buffer_requires_update) {
        renderer.update_instance_buffer();
    }

    if (light_shadow_info_requires_update) {
        renderer.update_shadow_light_info();
    }
}

void Renderer::resize(int w, int h) {
    width = w;
    height = h;

    if (width != 0 && height != 0) {
        glViewport(0, 0, width, height);
        create_framebuffers(true);
        create_gbuffer(true);
    }
}

Renderer::Renderer() {
    create_gaussian_blur_weights();
}

void Renderer::init() {
    stbi_set_flip_vertically_on_load(true);

    glEnable(GL_DEPTH_TEST);

    //glEnable(GL_BLEND);
    //glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    // enable face culling
    glEnable(GL_CULL_FACE);
    //glFrontFace(GL_CCW);
    //glCullFace(GL_BACK);

    glEnable(GL_MULTISAMPLE);

    setup_shaders(0.0f);

    glGenBuffers(1, &cube_vbo);
    glGenBuffers(1, &cube_ebo);
    glGenVertexArrays(1, &light_vao);

    create_gl_cube_and_light();

    create_skybox_cubemap();
    glGenBuffers(1, &skybox_ebo);
    glGenVertexArrays(1, &skybox_vao);

    create_gl_skybox();

    // creation of ms_color_texture, final_color_texture, bloom_color_textures will be handled by create_framebuffers()
    glGenFramebuffers(1, &ms_fb);
    glGenRenderbuffers(1, &ms_rbo);
    glGenFramebuffers(1, &post_process_fb);
    glGenFramebuffers(1, &tonemapped_fb);
    glGenFramebuffers(2 * bloom_max_num_blurs, bloom_fbs);
    glGenFramebuffers(1, &shadow_direction_fb);
    glGenTextures(1, &shadow_depth_texture);
    glGenFramebuffers(1, &shadow_point_fb);
    glGenTextures(1, &shadow_depth_cubemap);
    //glGenFramebuffers(1, &g_buffer);
    glGenFramebuffers(1, &deferred_fb);
    glGenRenderbuffers(1, &deferred_rbo);

    create_framebuffers(false);
    create_gbuffer(false);
    create_shadow_framebuffer_with_tex2d();
    create_shadow_framebuffer_with_cubemap();

    glGenBuffers(1, &screen_vbo);
    glGenVertexArrays(1, &screen_vao);

    create_gl_screen();
}

void Renderer::terminate() {
    glDeleteVertexArrays(1, &screen_vao);
    glDeleteBuffers(1, &screen_vbo);

    glDeleteTextures(1, &g_position);
    glDeleteTextures(1, &g_normal);
    glDeleteTextures(1, &g_color_spec);
    glDeleteRenderbuffers(1, &gbuffer_depth_rbo);
    glDeleteFramebuffers(1, &g_buffer);

    glDeleteTextures(1, &shadow_depth_cubemap);
    glDeleteFramebuffers(1, &shadow_point_fb);
    glDeleteTextures(1, &shadow_depth_texture);
    glDeleteFramebuffers(1, &shadow_direction_fb);
    glDeleteTextures(1, &post_process_color_texture);
    glDeleteTextures(1, &tonemapped_color_texture);
    glDeleteFramebuffers(1, &post_process_fb);
    glDeleteFramebuffers(1, &tonemapped_fb);
    glDeleteTextures(2 * bloom_max_num_blurs, bloom_color_textures);
    glDeleteFramebuffers(2 * bloom_max_num_blurs, bloom_fbs);
    glDeleteFramebuffers(1, &ms_fb);
    glDeleteTextures(1, &ms_color_texture);
    glDeleteRenderbuffers(1, &ms_rbo);
    glDeleteFramebuffers(1, &deferred_fb);
    glDeleteTextures(1, &deferred_color_texture);
    glDeleteRenderbuffers(1, &deferred_rbo);

    glDeleteVertexArrays(1, &light_vao);
    glDeleteBuffers(1, &cube_ebo);
    glDeleteBuffers(1, &cube_vbo);

    glDeleteVertexArrays(1, &skybox_vao);
    glDeleteBuffers(1, &skybox_ebo);
}
