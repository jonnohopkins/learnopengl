#include "texture.h"
#include "globals.h"

tex_id_t last_texture_id = 0;

tex_id_t new_texture_id() {
	ASSERT(last_texture_id != std::numeric_limits<tex_id_t>::max(), "exhausted available ids.");
	++last_texture_id;
	return last_texture_id;
}
