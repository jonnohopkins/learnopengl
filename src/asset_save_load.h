#pragma once

#include "scene.h"

void save_scene_file(const char* path, const Scene& scene);
void load_scene_file(const char* path, bool* value_changed);
